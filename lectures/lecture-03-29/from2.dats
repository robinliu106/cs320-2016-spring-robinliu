(* ****** ****** *)
//
#include
"share/atspre_define.hats"
#include
"share/atspre_staload.hats"
#include
"share/HATS/atspre_staload_libats_ML.hats"
//
(* ****** ****** *)

typedef int2 = (int, int)

(* ****** ****** *)

extern
fun
from2 : (int) -> stream(int2)
implement
from2(n) = let
//
fun aux(i: int): stream(int2) =
  $delay(stream_cons((n, i), aux(i+1)))
//
in
//
$delay
(
stream_cons
(
(n, 0)
,
stream_merge_cloref<int2>
( aux(1)
, from2(n+1)
, lam(p1, p2) => let
    val x1 = p1.0 + p1.1
    val x2 = p2.0 + p2.1
    val sgn = compare(x1, x2)
  in
    if sgn != 0 then sgn else compare(p1.0, p2.0)
  end // end of [lam]
)
) (* stream_cons *)
) (* end of [$delay] *)
//
end // end of [from2]

(* ****** ****** *)

implement
main0 () =
{
//
val xys = from2(0)
val-stream_cons((x, y), xys) = !xys
val () = println! ("(x, y) = (", x, ", ", y, ")")
val-stream_cons((x, y), xys) = !xys
val () = println! ("(x, y) = (", x, ", ", y, ")")
val-stream_cons((x, y), xys) = !xys
val () = println! ("(x, y) = (", x, ", ", y, ")")
val-stream_cons((x, y), xys) = !xys
val () = println! ("(x, y) = (", x, ", ", y, ")")
val-stream_cons((x, y), xys) = !xys
val () = println! ("(x, y) = (", x, ", ", y, ")")
val-stream_cons((x, y), xys) = !xys
val () = println! ("(x, y) = (", x, ", ", y, ")")
val-stream_cons((x, y), xys) = !xys
val () = println! ("(x, y) = (", x, ", ", y, ")")
val-stream_cons((x, y), xys) = !xys
val () = println! ("(x, y) = (", x, ", ", y, ")")
//
} (* end-of-main *)

(* ****** ****** *)

(* end of [from2.dats] *)
