//
#include
"share/atspre_define.hats"
#include
"share/atspre_staload.hats"
#include
"share/HATS/atspre_staload_libats_ML.hats"
//
(* ****** ****** *)

(*
extern
fun
from : (int) -> list0(int)
implement
from(n) = cons0(n, from(n+1))
*)

(* ****** ****** *)

extern
fun
from : (int) -> stream(int)
implement
from(n) = $delay(stream_cons(n, from(n+1)))

(* ****** ****** *)

implement
main0 () =
{
  val xs = from(0)
  val-stream_cons(x, xs) = !xs
  val () = println! ("x = ", x)
  val-stream_cons(x, xs) = !xs
  val () = println! ("x = ", x)
  val-stream_cons(x, xs) = !xs
  val () = println! ("x = ", x)
  val-stream_cons(x, xs) = !xs
  val () = println! ("x = ", x)
  val-stream_cons(x, xs) = !xs
  val () = println! ("x = ", x)
} (* end-of-main *)

(* ****** ****** *)

(* end of [from.dats] *)
