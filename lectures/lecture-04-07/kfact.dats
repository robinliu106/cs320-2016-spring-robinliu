(* ****** ****** *)

#include "share/atspre_staload.hats"

(* ****** ****** *)
//
// Direct style
//
fun fact(n: int): int = 
  if n > 0 then n * fact(n-1) else 1
//
// Continutation-passing style
//
typedef K(a:t@ype) = (a) -<cloref1> void
//
fun
kfact(n: int, k: K(int)): void =
  if n > 0
    then kfact(n-1, lam res => k(n * res))
    else k(1)
//
(* ****** ****** *)

implement
main0 () =
{
//
val N = 10
val k0 =
lam(res: int): void =<cloref1> println!("res = ", res)
//
val () = kfact(3, k0)
//
} (* end of [main0] *)
