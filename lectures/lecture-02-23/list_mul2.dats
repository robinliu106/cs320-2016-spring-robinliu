(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/HATS/atspre_staload_libats_ML.hats"

(* ****** ****** *)

extern
fun
list_mul : (list0(int)) -> int

(* ****** ****** *)

implement
list_mul(xs) = let
//
exception ZeroExn of ()
//
in
//
try
list0_foldleft<int><int>
  (xs, 1, lam(init, x) => if x = 0 then $raise ZeroExn() else init * x)
with
  | ~ZeroExn() => 0
//
end // end of [list_mul]

(* ****** ****** *)

implement
main0() = let
//
val xs0 =
list0_make_intrange(1, 100)
val xs1 =
list0_make_intrange(1, 1000000)
//
val xs2 = list0_append(xs0, cons0(0, xs1))
//
val prod = list_mul(xs2)
//
in
  println! ("prod = ", prod)
end // end of [main0]

(* ****** ****** *)

(* end of [list_mul2.dats] *)
