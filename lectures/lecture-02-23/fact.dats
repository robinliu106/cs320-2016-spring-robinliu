(* ****** ****** *)

(*
** A simple example of
** defensive programming
*)

(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
//
(* ****** ****** *)

extern
fun fact(x: int): int

(* ****** ****** *)

(*
implement fact(x) =
  if x > 0 then x * fact(x-1) else 1
*) 

(* ****** ****** *)

implement
fact(x) = let
//
val () = assertloc(x >= 0)
//
(*
val () =
if not(x >= 0) then (print ...; $raise ....)
*)
//
fun aux(x: int): int =
  if x > 0 then x * aux(x-1) else 1
//
in
  aux(x)
end // end of [fact]

(* ****** ****** *)

implement
main0() =
{
  val () = println! ("fact(10) = ", fact(10))
  val () = println! ("fact(~10) = ", fact(~10))
}

(* ****** ****** *)

(* end of [fact.dats] *)
