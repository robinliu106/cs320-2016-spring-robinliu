(*
** Counting the words in a given file
*)

(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
#include
"share/HATS/atspre_staload_libats_ML.hats"
//
(* ****** ****** *)

typedef word = string

(* ****** ****** *)

abstype wordmap = ptr

(* ****** ****** *)

(*
datatype
Option(a:t@ype) =
Some of (a) | None of ()
*)

(* ****** ****** *)
//
extern
fun char_get(): int
//
extern
fun word_get(): Option(word)
//
(* ****** ****** *)

implement
word_get() = let
//
fun
skip(): int = let
  val c = char_get()
in
//
if
c >= 0
then (if isalpha(c) then c else skip())
else (c)
//
end // end of [skip]
//
fun
loop
(
  cs: list0(char)
) : word = let
//
val c = char_get()
//
in
//
if isalpha(c)
then loop(cons0(int2char0(c), cs))
else string_implode(list0_reverse(cs))
//
end // end of [loop]
//
val c = skip()
//
in
  if c >= 0 then Some(loop(list0_sing(int2char0(c)))) else None()
end // end of [word_get]

(* ****** ****** *)

extern
fun wordmap_make_nil(): wordmap

(* ****** ****** *)

extern
fun
wordmap_search(map: wordmap, w: word): int

(* ****** ****** *)
//
extern
fun
wordmap_insert(map: wordmap, w: word): wordmap
//
extern
fun
wordmap_insert2(map: wordmap, w: word, n: int): wordmap
//
extern
fun
wordmap_insert_all(): wordmap
//
extern
fun
fprint_wordmap(FILEref, wordmap): void
//
overload fprint with fprint_wordmap
//
(* ****** ****** *)

implement
wordmap_insert(map, w) =
  wordmap_insert2(map, w, wordmap_search(map, w)+1)

(* ****** ****** *)

implement
wordmap_insert_all
  () = let
//
fun
loop(map: wordmap): wordmap = let
   val opt = word_get()
in
  case+ opt of
  | None() => map
  | Some(w) =>
      loop(wordmap_insert(map, w))
    // end of [Some]
end
//
in
  loop(wordmap_make_nil())
end // end of [word_insert_all]
//
(* ****** ****** *)

local
//
staload FM =
"libats/ML/SATS/funmap.sats"
//
assume wordmap = $FM.map(word, int)
//
in (* in-of-local *)

implement
wordmap_make_nil
  () = $FM.funmap_make_nil()

implement
wordmap_search(map, w) = let
  val opt = $FM.funmap_search<word,int>(map, w)
in
  case+ opt of
  | ~None_vt() => 0
  | ~Some_vt(n) => n
end // end of [wordmap_search]

(*
fun{
key,itm:t0p
} funmap_insert
  (map: &map(key, INV(itm)) >> _, key, itm): Option_vt(itm)
// end of [funmap_insert]
*)

implement
wordmap_insert2
  (map, w, n) = let
  var map = map
  val opt = $FM.funmap_insert(map, w, n)
  val () = 
  (
    case+ opt of
    | ~None_vt() => ()
    | ~Some_vt(n) => ()
  ) : void // end-of-val
in
  map
end // end of [word_insert2]

implement
fprint_wordmap(out, map) = fprint_funmap<word,int>(out, map)

end // end of [local]

(* ****** ****** *)

implement
char_get() = fileref_getc(stdin_ref)

(* ****** ****** *)

implement
main0() =
{
//
(*
val () =
println!
  ("Hello from [WordCounting]")
*)
//
val map = wordmap_insert_all((*void*))
//
val ((*void*)) = fprintln! (stdout_ref, "map = ", map)
//
} (* end of [main0] *)

(* ****** ****** *)

(* end of [WordCounting.dats] *)
