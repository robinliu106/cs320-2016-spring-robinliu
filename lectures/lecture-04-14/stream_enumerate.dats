(* ****** ****** *)
//
#include
"share/atspre_define.hats"
#include
"share/atspre_staload.hats"
#include
"share/HATS/atspre_staload_libats_ML.hats"
//
(* ****** ****** *)
//
extern
fun
{a:t@ype}
stream_enumerate
  (xss: stream(stream(a))): stream(a)
//
(* ****** ****** *)

implement
{a}(*tmp*)
stream_enumerate
  (xss) = let
//
fun
alter
(
  xs: stream(a)
, ys: stream(a)
) : stream(a) =
$delay
(
let
  val-stream_cons(x, xs) = !xs
in
  stream_cons(x, alter(ys, xs))  
end
) // end of [alter]
//
(*
//
// This is supported in the latest version of ATS2:
//
fun
alter
(
  xs: stream(a)
, ys: stream(a)
) : stream(a) =
  $delay(stream_cons(xs.head(), alter(ys, xs.tail())))
*)
//
in
//
$delay(
//
let
//
val-
stream_cons(xs0, xss_tl) = !xss
//
val-stream_cons(x0, xs0_tl) = !xs0
//
in
  stream_cons(x0, alter(xs0_tl, stream_enumerate(xss_tl)))
end // let
) (* $delay *)
end // end of [stream_enumerate]

(* ****** ****** *)

extern
fun
from : (int) -> stream(int)
implement
from(n) = $delay(stream_cons(n, from(n+1)))

(* ****** ****** *)
//
typedef int2 = (int, int)
//
extern
fun
from2 : (int) -> stream(stream(int2))
implement
from2(n) = $delay(stream_cons(stream_map_cloref(from(0), lam(j) => (n, j)), from2(n+1)))
//
(* ****** ****** *)

implement
main0 () =
{
//
val xys = from2(0)
val xys = stream_enumerate<int2>(xys)
//
val-stream_cons((x, y), xys) = !xys
val () = println! ("(x, y) = (", x, ", ", y, ")")
val-stream_cons((x, y), xys) = !xys
val () = println! ("(x, y) = (", x, ", ", y, ")")
val-stream_cons((x, y), xys) = !xys
val () = println! ("(x, y) = (", x, ", ", y, ")")
val-stream_cons((x, y), xys) = !xys
val () = println! ("(x, y) = (", x, ", ", y, ")")
val-stream_cons((x, y), xys) = !xys
val () = println! ("(x, y) = (", x, ", ", y, ")")
val-stream_cons((x, y), xys) = !xys
val () = println! ("(x, y) = (", x, ", ", y, ")")
val-stream_cons((x, y), xys) = !xys
val () = println! ("(x, y) = (", x, ", ", y, ")")
val-stream_cons((x, y), xys) = !xys
val () = println! ("(x, y) = (", x, ", ", y, ")")
//
} (* end-of-main *)

(* ****** ****** *)

(* end of [stream_enumerate.dats] *)
