//
// Title:
// Concepts of Programming Languages
// Number: CAS CS 320
// Semester: Spring 2016
// Class Time: TR 12:30-2:00pm
// Instructor: Hongwei Xi (hwxiATbuDOTedu)
// Teaching Fellow: Hanwen (Steinway) Wu (hwwuATbuDOTedu)
//
(* ****** ****** *)
//
// HX-2016-04-19:
//
(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
#include
"share/HATS/atspre_staload_libats_ML.hats"
//
(* ****** ****** *)
//
typedef
mymat(a:t@ype) =
  (int, int) -<cloref1> a
//
(* ****** ****** *)
//
extern
fun
{a:t@ype}
mymat_remove_row
  (mymat(a), i0: int): mymat(a)
extern
fun
{a:t@ype}
mymat_remove_col
  (mymat(a), i0: int): mymat(a)
//
(* ****** ****** *)
//
implement
{a}(*tmp*)
mymat_remove_row(M, i0) =
  lam(i, j) => M(if i < i0 then i else i+1, j)
implement
{a}(*tmp*)
mymat_remove_row(M, j0) =
  lam(i, j) => M(i, if j < j0 then j else j+1)
//
(* ****** ****** *)

extern
fun
{a:t@ype}
mymat_remove2_row
  (mymat(a), i0: int, i1: int): mymat(a)
implement
{a}
mymat_remove2_row
  (M, i0, i1) = let
//
val sgn = compare(i0, i1)
//
in
//
case+ sgn of
| _ when sgn > 0 => mymat_remove_row(mymat_remove_row(M, i0), i1)
| _ when sgn < 0 => mymat_remove_row(mymat_remove_row(M, i0), i1-1)
| _ => mymat_remove_row(M, i0)
//
end // end of [mymat_remove2_row]

(* ****** ****** *)

(* end of [mymatrix_cloref.dats] *)
