(* ****** ****** *)
//
#include
"share/atspre_define.hats"
#include
"share/atspre_staload.hats"
#include
"share/HATS/atspre_staload_libats_ML.hats"
//
(* ****** ****** *)
//
extern
fun
{a:t@ype}
mmul : (matrix0(a), matrix0(a)) -> matrix0(a)
//
(* ****** ****** *)

implement
{a}(*tmp*)
mmul (A, B) = let
//
val pa = A.nrow()
val qa = A.ncol()
//
val qb = B.nrow()
val rb = B.ncol()
//
val () = assertloc(qa = qb)
//
macdef + = gadd_val_val<a>
macdef * = gmul_val_val<a>
//
val _0_ = gnumber_int<a>(0)
//
in
//
matrix0_tabulate<a>
(
  pa, rb
, lam(i, j) =>
  $effmask_all
  (
    (sz2i(qa)).foldleft(TYPE{a})(_0_, lam(res, k) => res+A[sz2i(i),k]*B[k,sz2i(j)])
  )
) (* matrix0_tabulate *)
//
end // end of [mmul]

(* ****** ****** *)

implement main0 () = ()

(* ****** ****** *)

(* end of [mymatmul.dats] *)
