(* ****** ****** *)

staload
"libats/ML/SATS/basis.sats"

staload
"./../../mylib/mylist.dats"

(* ****** ****** *)
//
datatype
tree(a:t@ype) =
| tree_nil of ()
| tree_cons of (tree(a), a, tree(a))
//
(* ****** ****** *)

extern
fun{a:t@ype}
tree_size(xs: tree(a)): int // intGte(0)
//
implement
{a}
tree_size(xs) = let
//
fun aux(xs: tree(a)): int =
  case+ xs of
  | tree_nil() => 0
  | tree_cons(xs_l, _, xs_r) => 1+(aux(xs_l)+aux(xs_r))
//
in
  aux(xs)
end // end of [tree_size]

(* ****** ****** *)

extern
fun{a:t@ype}
tree_height(xs: tree(a)): int // intGte(0)
//
implement
{a}
tree_height(xs) = let
//
fun aux(xs: tree(a)): int =
  case+ xs of
  | tree_nil() => 0
  | tree_cons(xs_l, _, xs_r) => 1+max(aux(xs_l), aux(xs_r))
//
in
  aux(xs)
end // end of [tree_height]

(* ****** ****** *)
//
extern
fun
{a,b:t@ype}
tree_fold
(
  xs: tree(a), fopr: (b, a, b) -<cloref1> b, sink: b
) : b // end of [tree_fold]
//
(* ****** ****** *)

implement
{a}
tree_size(xs) =
tree_fold<a,int>(xs, lam(sl, _, sr) => sl + 1 + sr, 0)

(* ****** ****** *)

implement
{a}
tree_height(xs) =
tree_fold<a,int>(xs, lam(sl, _, sr) => 1+max(sl, sr), 0)

(* ****** ****** *)

implement
{a}
tree_height(xs) =
tree_fold<a,int>(xs, lam(hl, _, hr) => 1+max(hl, hr), 0)

(* ****** ****** *)

implement
{a}
tree_height(xs) =
tree_fold<a,int>(xs, lam(sl, _, sr) => 1+max(sl, sr), 0)

(* ****** ****** *)

extern
fun{a:t@ype}
tree_sheight(xs: tree(a)): int

(* ****** ****** *)

implement
{a}
tree_sheight(xs) =
tree_fold<a,int>(xs, lam(sl, _, sr) => 1+min(sl, sr), 0)

(* ****** ****** *)

implement
{a,b}
tree_fold
(
  xs, fopr, sink
) = let
//
fun aux(xs: tree(a)): b =
  case+ xs of
  | tree_nil() => sink
  | tree_cons(xs_l, x, xs_r) => fopr(aux(xs_l), x, aux(xs_r))
//
in
  aux(xs)
end // end of [tree_fold]

(* ****** ****** *)
  
extern
fun{a:t@ype}
tree_flatten(xs: tree(a)): list0(a)

implement
{a}
tree_flatten
  (xs) =
tree_fold<a,list0(a)>(xs, lam(l, x, r) => mylist_append(l, cons0(x, r)), nil0)

(* ****** ****** *)

(* end of [mytree.dats] *)