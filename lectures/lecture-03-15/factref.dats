
(*
** A "bad" implementation of fact
*)

#include
"share/atspre_staload.hats"

extern fun factref : int -> int

implement
factref(n) = let
  val i = ref<int>(1)
  val r = ref<int>(1)
  fun loop(): void =
    // for (i = 1; i < n; i += 1) ...
    if !i <= n then (!r := !i * !r; !i := !i+1; loop())
  // end of [loop]
in
  loop(); !r
end // end of [factref]

implement
main0() = println! ("factref(10) = ", factref(10))