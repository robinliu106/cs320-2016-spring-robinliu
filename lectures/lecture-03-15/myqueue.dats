#include
"share/atspre_staload.hats"
#include
"share/HATS/atspre_staload_libats_ML.hats"

datatype
myqueue(a:t@ype) =
MYQUEUE of (ref(list0(a)), ref(list0(a)))

extern
fun
{a:t@ype}
myqueue_make_nil(): myqueue(a)

extern
fun
{a:t@ype}
myqueue_enqueue(xs: myqueue(a), x0: a): void

exception MYQUEUE_EMPTY_EXN of ()

extern
fun
{a:t@ype}
myqueue_dequeue_exn(xs: myqueue(a)): a
extern
fun
{a:t@ype}
myqueue_dequeue_opt(xs: myqueue(a)): Option(a)

(* ****** ****** *)

implement
{a}
myqueue_enqueue
  (xs, x0) = let
  val MYQUEUE(left, right) = xs
in
  !left := list0_cons(x0, !left)
end

(* ****** ****** *)

implement
{a}
myqueue_dequeue_exn
  (xs) = let
  val MYQUEUE(left, right) = xs
in
  case+ !right of
  | list0_nil() => let
      val ys = !left
      val () = !left := list0_nil()
      val ys = list0_reverse(ys)
    in
      case+ ys of
      | list0_nil() =>
          $raise MYQUEUE_EMPTY_EXN()
      | list0_cons(y, ys) => (!right := ys; y)
    end
  | list0_cons(y, ys) => (!right := ys; y)
end

(* ****** ****** *)

implement
{a}
myqueue_dequeue_opt
  (xs) =
try 
  Some(myqueue_dequeue_exn(xs))
with ~MYQUEUE_EMPTY_EXN() => None()

(* ****** ****** *)

(* end of [myqueue.dats] *)
