(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/HATS/atspre_staload_libats_ML.hats"

(* ****** ****** *)

staload "./../../mylib/mystack.dats"

(* ****** ****** *)

#define N 8

(* ****** ****** *)

typedef node = list0(int)

(* ****** ****** *)

extern
fun
print_node(node): void

(* ****** ****** *)

extern
fun
process_node(node): void

(* ****** ****** *)

implement
process_node(nx) = let
  val n = list0_length(nx)
in
  if n >= N then print_node(nx) else ()
end // end of [process_node]

(* ****** ****** *)

extern
fun
node_get_children(node): list0(node)


local

fun
node_test
(
  nx: node, i: int
) : bool = let
//
fun
aux
(
  nx: node, df: int
) : bool =
(
  case+ nx of
  | nil0() => true
  | cons0(x, nx) =>
    if abs(x-i)=df
      then aux(nx, df+1) else false
    // end of [if]
)
//
in
  aux(nx, 1)
end // end of [node_test]

in (* in-of-local *)

implement
node_get_children(nx) = let
//
fun
aux(i: int): list0(node) =
if
i < N
then let
  val test = node_test(nx, i)
in
  if test then list0_cons(cons0(i, nx), aux(i+1)) else aux(i+1)
end
else list0_nil()
//
in
  if length(nx) < N then aux(0) else nil0()
end // end of [node_get_children]

end // end of [local]

(* ****** ****** *)

(* end of [queens.dats] *)
