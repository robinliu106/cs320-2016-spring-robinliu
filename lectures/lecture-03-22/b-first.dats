#include
"share/atspre_staload.hats"
#include
"share/HATS/atspre_staload_libats_ML.hats"

(* ****** ****** *)

staload "./../../mylib/myqueue.dats"

(* ****** ****** *)

abstype node

(* ****** ****** *)

extern
fun
node_get_children(node): list0(node)

(* ****** ****** *)

extern
fun
bfirst_search(node): void

(* ****** ****** *)

implement
bfirst_search(nx) = let
//
val
stk = myqueue_make_nil()
//
val () = myqueue_enqueue(stk, nx)
//
fun loop() = let
  val opt = myqueue_dequeue_opt(stk)
in
  case+ opt of
  | None() => ()
  | Some(nx) => let
      val nxs = node_get_children(nx)
      val () = myqueue_enqueue_list(stk, nxs)
    in
      loop()
    end
end
//
in
  loop()
end // end of [bfirst_search]

(* ****** ****** *)

(* end of [b-first.dats] *)
