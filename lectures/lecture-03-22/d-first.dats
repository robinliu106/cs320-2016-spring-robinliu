#include
"share/atspre_staload.hats"
#include
"share/HATS/atspre_staload_libats_ML.hats"

(* ****** ****** *)

staload "./../../mylib/mystack.dats"

(* ****** ****** *)

abstype node

(* ****** ****** *)

extern
fun
process_node(node): void

extern
fun
node_get_children(node): list0(node)

(* ****** ****** *)

extern
fun
dfirst_search(node): void

(* ****** ****** *)

implement
dfirst_search(nx) = let
//
val
stk = mystack_make_nil()
//
val () = mystack_push(stk, nx)
//
fun loop(): void = let
  val opt = mystack_pop_opt(stk)
in
  case+ opt of
  | None() => ()
  | Some(nx) => let
      val () = process_node(nx)
      val nxs = node_get_children(nx)
      val () = mystack_push_list(stk, nxs)
    in
      loop()
    end
end
//
in
  loop()
end // end of [dfirst_search]


(* ****** ****** *)

(* end of [d-first.dats] *)
