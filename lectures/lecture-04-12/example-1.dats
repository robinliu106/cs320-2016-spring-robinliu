(* ****** ****** *)
//
#include
"share/atspre_define.hats"
#include
"share/atspre_staload.hats"
#include
"share/HATS/atspre_staload_libats_ML.hats"
//
(* ****** ****** *)

extern
fun
from : (int) -> stream(int)
implement
from(n) = $delay(stream_cons(n, from(n+1)))

(* ****** ****** *)

fun
alterneg
(
  xs0: stream(int)
) : stream(int) = let
//
fun
aux
(
  sgn: int
, xs0: stream(int)
) : stream(int) =
$delay(
  case+ !xs0 of
  | stream_nil() => stream_nil()
  | stream_cons(x, xs) => stream_cons(sgn*x, aux(~sgn, xs))
) (* $delay *)
//
in
  aux(1, xs0)
end // end of [alterneg]

(* ****** ****** *)



(*
fun
partialsum
(
  xs0: stream(double)
) : stream(double) = let
//
fun
aux
(
  sum: double
, xs0: stream(double)
) : stream(double) =
$delay(
  case+ !xs0 of
  | stream_nil() => stream_nil()
  | stream_cons(x, xs) => stream_cons(sum, aux(sum+x, xs))
) 
//
in
  aux(0.0, xs0)
end // end of [partialsum]
*)


(* ****** ****** *)
(*
fun
pi_eval
(
// argless
) : stream(double) = let
//
val xs0 = from(0) // 0, 1, 2, 3, 4, 5,
val xs1 = stream_map_cloref<int><int>(xs0, lam(x) => 2*x+1) // 1, 3, 5, ...
val xs2 = alterneg(xs1) // 1, ~3, 5, ...
val xs3 = stream_map_cloref<int><double>(xs2, lam(x) => 1.0/x) // 1/1, -1/3, 1/5, ...
//
in
  partialsum(xs3)
end // end of [pi_eval]
*)

fun
partialsum(xs0: stream(double)) : stream(double) = let
//
fun
aux
(sum: double, xs0: stream(double)) : stream(double) =
$delay(
  case+ !xs0 of
  | stream_nil() => stream_nil()
  | stream_cons(x, xs) => stream_cons(sum, aux(sum+x, xs))
) (* $delay *)
//
in
  aux(0.0, xs0)
end // end of [partialsum]


fun
pi_eval
(
// argless
) : stream(double) = let
//
val xs0 = from(1) // 1, 2, 3, 4,
//val xs1 = stream_map_cloref<int><int>(xs0, lam(x) => x+1) // 1, 2, 3, 4...
val xs2 = alterneg(xs0) // 1, -2, 3, -4 ...
val xs3 = stream_map_cloref<int><double>(xs2, lam(x) => $effmask_all(1.0/x)) // 1/1, -1/2, 1/3, -1/4 ...
//
in
  partialsum(xs3)
end // end of [pi_eval]

(* ****** ****** *)

implement
main0() = let
  val ps = pi_eval()
in
  println! ("p_10 = ", stream_nth_exn(ps, 10));
  println! ("p_100 = ", stream_nth_exn(ps, 100));
  println! ("p_1000 = ", stream_nth_exn(ps, 1000));
  println! ("p_1000000 = ", stream_nth_exn(ps, 1000000));
  //println! ("p_10000000 = ", stream_nth_exn(ps, 10000000));
end // end of [main0]

(* ****** ****** *)

(* end of [example-1.dats] *)
