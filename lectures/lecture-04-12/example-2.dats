(* ****** ****** *)
//
#include
"share/atspre_define.hats"
#include
"share/atspre_staload.hats"
#include
"share/HATS/atspre_staload_libats_ML.hats"
//
(* ****** ****** *)

extern
fun
from : (int) -> stream(int)
implement
from(n) = $delay(stream_cons(n, from(n+1)))

(* ****** ****** *)
//
extern
fun
{a,b:t@ype}
stream_fold
(
  xs: stream(a), init: b, fopr: (b, a) -<cloref1> b
) : stream(b) // end of [stream_fold]
//
(* ****** ****** *)

implement
{a,b}
stream_fold
  (xs, init, fopr) = let
//
fun
aux
(
  xs: stream(a), y: b
) : stream(b) =
$delay(
  case+ !xs of
  | stream_nil() => stream_nil()
  | stream_cons(x, xs) => stream_cons(y, aux(xs, fopr(y, x)))
) (* $delay *)
//
in
  aux(xs, init)
end // end of [stream_fold]

(* ****** ****** *)

fun
pi_eval
(
// argless
) : stream(double) = let
//
val xs0 = from(0) // 0, 1, 2, 3, 4, 5,
val ys0 = stream_fold<int,int>(xs0, 1, lam(y, _) => ~y)
val xs1 = stream_map2_cloref<int,int><int>(xs0, ys0, lam(x, y) => (2*x+1)*y)
val xs2 = stream_map_cloref<int><double>(xs1, lam(x) => 1.0/x) // 1/1, -1/3, 1/5, ...
//
in
  stream_fold<double,double>(xs2, 0.0, lam(y, x) => y+x)
end // end of [pi_eval]

(* ****** ****** *)

implement
main0() = let
  val ps = pi_eval()
in
  println! ("p_10 = ", 4 * stream_nth_exn(ps, 10));
  println! ("p_100 = ", 4 * stream_nth_exn(ps, 100));
  println! ("p_1000 = ", 4 * stream_nth_exn(ps, 1000));
  println! ("p_1000000 = ", 4 * stream_nth_exn(ps, 1000000));
end // end of [main0]

(* ****** ****** *)

(* end of [example-2.dats] *)
