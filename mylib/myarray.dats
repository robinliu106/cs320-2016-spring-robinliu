(* ****** ****** *)

staload
"libats/ML/SATS/basis.sats"
staload
"libats/ML/SATS/array0.sats"

(* ****** ****** *)
//
staload
UN = "prelude/SATS/unsafe.sats"  
//
(* ****** ****** *)
//
// t@ype: flat, unboxed, native
//
typedef myarray(a:t@ype) = array0(a)

(* ****** ****** *)

extern
fun{a:t@ype}
myarray_size(myarray(a)): int

(* ****** ****** *)
//
extern
fun{a:t@ype}
myarray_interchange
  (myarray(a), i: int, j: int): void
//
(* ****** ****** *)

extern
fun{a:t@ype}
myarray_make_elt
  (n: int, x0: a): myarray(a)

(* ****** ****** *)
//
extern
fun{a:t@ype}
myarray_foreach
  (myarray(a), fopr: (a) -<cloref1> void): void

extern
fun{a:t@ype}
myarray_iforeach
  (myarray(a), fopr: (int, a) -<cloref1> void): void 
//
(* ****** ****** *)
//
extern
fun{a:t@ype}
myarray_rforeach
  (myarray(a), fopr: (a) -<cloref1> void): void
//
(* ****** ****** *)
//
extern
fun
{a,b:t@ype}
myarray_foldleft
(
  A: myarray(a), init: b, fopr: (b, a) -<cloref1> b
) : b // end-of-myarray_foldleft
//
extern
fun
{a,b:t@ype}
myarray_foldright
(
  A: myarray(a), fopr: (a, b) -<cloref1> b, sink: b
) : b // end-of-myarray_foldright
//
(* ****** ****** *)

implement
{a}(*tmp*)
myarray_size(A) = sz2i(A.size())

(* ****** ****** *)

implement
{a}(*tmp*)
myarray_interchange
  (A, i, j) =
{
  val x0 = A[i]
  val () = A[i] := A[j]
  val () = A[j] := x0
}

(* ****** ****** *)

implement
{a}(*tmp*)
myarray_make_elt
  (n, x0) = let
  val () = assertloc(n > 0)
in
  array0_make_elt<a>($UN.cast{size_t}(n), x0)
end // end of [myarray_make_elt]

(* ****** ****** *)

implement
{a}(*tmp*)
myarray_foreach
  (A, fopr) = let
//
val n =
myarray_size<a>(A)
//
fun loop(i: int): void =
  if i < n then (fopr(A[i]); loop(i+1))
//
in
  loop(0)
end // end of [myarray_foreach]

implement
{a}(*tmp*)
myarray_iforeach
  (A, fopr) = let
//
val n =
myarray_size<a>(A)
//
fun loop(i: int): void =
  if i < n then (fopr(i, A[i]); loop(i+1))
//
in
  loop(0)
end // end of [myarray_iforeach]

(* ****** ****** *)

implement
{a}(*tmp*)
myarray_rforeach
  (A, fopr) = let
//
val n =
myarray_size<a>(A)
//
fun loop(i: int): void =
  if i > 0 then let
    val i1 = i - 1
    val () = fopr(A[i1])
  in
    loop(i1)
  end // end of [loop]
//
in
  loop(n)
end // end of [myarray_rforeach]

(* ****** ****** *)
//
extern
fun{a:t@ype}
myarray2list(A: myarray(a)): list0(a)
//
(* ****** ****** *)

implement
{a}(*tmp*)
myarray2list
  (A) = let
//
val r0 = ref<list0(a)>(nil0())
val () = myarray_rforeach(A, lam(x) => !r0 := cons0(x, !r0))
//
in
  myarray_foldright<a,list0(a)>(A, lam(x, xs) => cons0(x, xs), nil0())
end // end of [myarray2list]

(* ****** ****** *)
//
typedef
myarrayr
  (a:t@ype) = (int) -<cloref1> a
typedef
myarrayw
  (a:t@ype) = (int, a) -<cloref1> void
//
(* ****** ****** *)
//
extern
fun
{a:t@ype}
myarrayr_make_array
  (myarray(a)): myarrayr(a)
//
implement
{a}(*tmp*)
myarrayr_make_array(A) = lam(i) => A[i]
//
(* ****** ****** *)
//
extern
fun
{a:t@ype}
{b:t@ype}
myarrayr_foldleft
(
  A: myarrayr(a)
, asz: int, init: b, fopr: (b, a) -<cloref1> b
) : (b) // end-of-function
//
implement
{a}{b}(*tmp*)
myarrayr_foldleft
  (A, asz, init, fopr) = let
//
fun aux(i: int, res: b): b =
  if i < asz
    then aux(i+1, fopr(res, A(i))) else res
  // end of [if]
//
in
  aux(0, init)
end // end of [myarrayr_foldleft]
//
(* ****** ****** *)
//
extern
fun
{a:t@ype}
{b:t@ype}
myarrayr_ifoldleft
(
  A: myarrayr(a)
, asz: int, init: b, fopr: (int, b, a) -<cloref1> b
) : (b) // end-of-function
//
implement
{a}{b}(*tmp*)
myarrayr_ifoldleft
  (A, asz, init, fopr) = let
//
fun aux(i: int, res: b): b =
  if i < asz
    then aux(i+1, fopr(i, res, A(i))) else res
  // end of [if]
//
in
  aux(0, init)
end // end of [myarrayr_ifoldleft]
//
(* ****** ****** *)
//
extern
fun
{a:t@ype}
myarrayw_make_array
  (myarray(a)): myarrayw(a)
//
implement
{a}(*tmp*)
myarrayw_make_array(A) = lam(i, x) => A[i] := x
//
(* ****** ****** *)

(* end of [myarray.dats] *)
