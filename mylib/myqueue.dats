#include
"share/atspre_staload.hats"
#include
"share/HATS/atspre_staload_libats_ML.hats"

(* ****** ****** *)
//
datatype
myqueue(a:t@ype) =
MYQUEUE of
  (ref(list0(a)), ref(list0(a)))
//
(* ****** ****** *)

extern
fun
{a:t@ype}
myqueue_make_nil(): myqueue(a)

(* ****** ****** *)
//
extern
fun
{a:t@ype}
myqueue_enqueue
  (que: myqueue(a), x0: a): void
//
extern
fun
{a:t@ype}
myqueue_enqueue_list
  (que: myqueue(a), xs: list0(a)): void
//
(* ****** ****** *)

exception MYQUEUE_EMPTY_EXN of ()

extern
fun
{a:t@ype}
myqueue_dequeue_exn(que: myqueue(a)): a
extern
fun
{a:t@ype}
myqueue_dequeue_opt(que: myqueue(a)): Option(a)

(* ****** ****** *)

implement
{a}
myqueue_make_nil
  () = let
//
val left = ref<list0(a)>(nil0)
val right = ref<list0(a)>(nil0)
//
in
  MYQUEUE(left, right)
end // end of [myqueue_make_nil]

(* ****** ****** *)

implement
{a}(*tmp*)
myqueue_enqueue
  (que, x0) = let
  val MYQUEUE(left, right) = que
in
  !left := list0_cons(x0, !left)
end // end of [myqueue_enqueue]

(* ****** ****** *)

implement
{a}(*tmp*)
myqueue_enqueue_list
  (que, xs) = let
  val MYQUEUE(left, right) = que
in
  !left := list0_reverse_append(xs, !left)
end // end of [myqueue_enqueue_list]

(* ****** ****** *)

implement
{a}(*tmp*)
myqueue_dequeue_exn
  (que) = let
  val MYQUEUE(left, right) = que
in
  case+ !right of
  | list0_nil() => let
      val ys = !left
      val () = !left := list0_nil()
      val ys = list0_reverse(ys)
    in
      case+ ys of
      | list0_nil() =>
          $raise MYQUEUE_EMPTY_EXN()
      | list0_cons(y, ys) => (!right := ys; y)
    end
  | list0_cons(y, ys) => (!right := ys; y)
end // end of [myqueue_dequeue_exn]

(* ****** ****** *)

implement
{a}(*tmp*)
myqueue_dequeue_opt
  (que) =
try 
  Some(myqueue_dequeue_exn(que))
with ~MYQUEUE_EMPTY_EXN() => None()

(* ****** ****** *)

(* end of [myqueue.dats] *)
