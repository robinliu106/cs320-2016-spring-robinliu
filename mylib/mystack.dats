#include
"share/atspre_staload.hats"
#include
"share/HATS/atspre_staload_libats_ML.hats"

(* ****** ****** *)

typedef
mystack(a:t@ype) = ref(list0(a))

(* ****** ****** *)

extern
fun{a:t@ype}
mystack_make_nil(): mystack(a)

implement
{a}(*tmp*)
mystack_make_nil() = ref(list0_nil())

(* ****** ****** *)

extern
fun{a:t@ype}
mystack_top_exn(stk: mystack(a)): a

(* ****** ****** *)

extern
fun{a:t@ype}
mystack_pop_exn(stk: mystack(a)): a
extern
fun{a:t@ype}
mystack_pop_opt(stk: mystack(a)): Option(a)

(* ****** ****** *)

extern
fun{a:t@ype}
mystack_push(stk: mystack(a), x0: a): void

(* ****** ****** *)

extern
fun{a:t@ype}
mystack_push_list
  (stk: mystack(a), xs: list0(a)): void

(* ****** ****** *)

exception MYSTACK_EMPTY_EXN of ()

(* ****** ****** *)
//
implement
{a}(*tmp*)
mystack_pop_exn(stk) =
  case !stk of
  | list0_cons
      (x, xs) => (!stk := xs; x)
    // list0_cons
  | list0_nil() => $raise MYSTACK_EMPTY_EXN()
//
(* ****** ****** *)
//
implement
{a}(*tmp*)
mystack_pop_opt(stk) =
  case !stk of
  | list0_nil() => None()
  | list0_cons(x, xs) => (!stk := xs; Some(x))
//
(* ****** ****** *)

implement
{a}(*tmp*)
mystack_push(stk, x0) =
  !stk := list0_cons(x0, !stk)

(* ****** ****** *)

implement
{a}(*tmp*)
mystack_push_list(stk, xs) =
  !stk := list0_append(xs, !stk)

(* ****** ****** *)

(* end of [mystack.dats] *)
