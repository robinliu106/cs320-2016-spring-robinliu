(* ****** ****** *)

staload "./mytree.dats"

(* ****** ****** *)

extern
fun{a:t@ype}
mytree0_is_braun(xs: tree0(a)): bool

(* ****** ****** *)

(*

implement
{a}
mytree0_is_braun
  (xs) = let
//
fun
aux(xs: tree0(a)): bool =
(
case+ xs of
| tnil0() => true
| tcons0(xs_l, _, xs_r) => let
    val sz_l = mytree_size(xs_l)
    val sz_r = mytree_size(xs_r)
  in
    aux(xs_l) && aux(xs_r) && (sz_r <= sz_l) && (sz_l <= sz_r+1)
  end // end of [tcons0]
)
//
in
  aux(xs)
end // end of [mytree0_is_braun]

*)

(* ****** ****** *)

implement
{a}(*tmp*)
mytree0_is_braun
  (xs) = let
//
exception NotBraunExn
//
fun
aux(xs: tree0(a)): int =
(
case+ xs of
| tnil0() => 0
| tcons0(xs_l, _, xs_r) => let
    val sz_l = aux(xs_l)
    val sz_r = aux(xs_r)
  in
    if (sz_r <= sz_l) && (sz_l <= sz_r+1)
      then sz_l+1+sz_r else $raise NotBraunExn()
  end // end of [tcons0]
)
//
in
  try let val _ = aux(xs) in true end with ~NotBraunExn() => false
end // end of [mytree0_is_braun]

(* ****** ****** *)

(* end of [mytree_braun.dats] *)
