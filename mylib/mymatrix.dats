(* ****** ****** *)

staload
"libats/ML/SATS/basis.sats"
staload
"libats/ML/SATS/matrix0.sats"

(* ****** ****** *)
//
staload
UN = "prelude/SATS/unsafe.sats"  
//
(* ****** ****** *)
//
// t@ype: flat, unboxed, native
//
typedef mymatrix(a:t@ype) = matrix0(a)

(* ****** ****** *)

staload "./myarray.dats"

(* ****** ****** *)

////

(*
	interfaces
*)

abst@ype mymatrix (a:t@ype)

extern fun {a:t@ype} mymatrix_init (nrows: int, ncols: int, init: a): mymatrix a
extern fun {a:t@ype} mymatrix_nrows (mymatrix a): int
extern fun {a:t@ype} mymatrix_ncols (mymatrix a): int 
extern fun {a:t@ype} mymatrix_swap_elt (mymatrix a, int, int): void
extern fun {a:t@ype} mymatrix_swap_row (mymatrix a, int, int): void
extern fun {a:t@ype} mymatrix_swap_col (mymatrix a, int, int): void
extern fun {a:t@ype} mymatrix_foreach_elt (mymatrix a, a -<cloref1> void): void
extern fun {a:t@ype} mymatrix_iforeach_elt (mymatrix a, (int, int, a) -<cloref1> void): void
extern fun {a:t@ype} mymatrix_get_elt (mymatrix a, int, int): a 
extern fun {a:t@ype} mymatrix_set_elt (mymatrix a, int, int, a): void

extern fun {} mymatrix_print_rowsep (): void
extern fun {} mymatrix_print_colsep (): void
extern fun {a:t@ype} mymatrix_print_elt (a): void

extern fun {a:t@ype} mymatrix_print (mymatrix a): void 

symintr .nrows
symintr .ncols
symintr .get 
symintr .set 

overload .nrows with mymatrix_nrows
overload .ncols with mymatrix_ncols
overload .get with mymatrix_get_elt
overload .set with mymatrix_set_elt

overload print with mymatrix_print

(*
	implementations
*)

#include "share/atspre_staload.hats"
#include "share/HATS/atspre_staload_libats_ML.hats"
staload "./myarray.dats"

assume mymatrix (a:t@ype) = @(int, int, myarray a)

implement {a} mymatrix_init (nrows, ncols, init) = let 
	val _ = assertloc ((nrows > 0) && (ncols > 0))
in 
	@(nrows, ncols, myarray_make_elt<a> (nrows * ncols, init))
end

implement {a} mymatrix_nrows (mat) = 
	mat.0 

implement {a} mymatrix_ncols (mat) = 
	mat.1

implement {a} mymatrix_get_elt (mat, row, col) = let 
	val _ = assertloc ((row >= 0) && (row < mat.0))
	val _ = assertloc ((col >= 0) && (col < mat.1))
	val ret = mat.2
in 
	ret[row * mat.ncols() + col] 
end 

implement {a} mymatrix_set_elt (mat, row, col, v) = let 
	val _ = assertloc ((row >= 0) && (row < mat.0))
	val _ = assertloc ((col >= 0) && (col < mat.1))
	val ret = mat.2
in 
	ret[row * mat.ncols() + col] := v
end 

implement {a} mymatrix_foreach_elt (mat, f) = 
	myarray_foreach (mat.2, lam (e) => f (e))

implement {a} mymatrix_iforeach_elt (mat, f) = 
	myarray_iforeach (mat.2, lam (i, e) => f (i / mat.ncols(), i % mat.ncols(), e))

implement {a} mymatrix_print (mat) = 
	mymatrix_iforeach_elt (mat, lam (r, c, e) => let 
		val _ = mymatrix_print_elt<a> e
		val _ = 
			if c = mat.ncols() - 1
			then mymatrix_print_rowsep<> ()
			else mymatrix_print_colsep<> ()
		in 
		end)

(*
	tests
*)

local 

	implement mymatrix_print_elt<int> (e) = print_int e
	implement mymatrix_print_rowsep<> (): void = print_char ('\n')
	implement mymatrix_print_colsep<> (): void = print_char (' ')

in 
	implement main0 () = let 
		val mat = mymatrix_init<int> (4, 5, 0)
		val _ = mat.set(2,3,100)
		val _ = print<int> mat 
		val _ = print '\n'
	in 
		()
	end 
	
end 

