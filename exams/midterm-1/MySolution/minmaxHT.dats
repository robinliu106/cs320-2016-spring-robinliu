(* ****** ****** *)

#include "./../midterm-1.dats"

(* ****** ****** *)

(*
//
// HX: 10 points
// Please implement mytree_minmaxHT
//
*)

implement
{a}
mytree_minmaxHT(xs) = 
mytree_fold<a,int>
    (xs, lam(xs_l, _, xs_r) => 1+max(xs_l, xs_r), 1+min(xs_l, xs_r)  ) 



(* ****** ****** *)

implement
main0() = () where
{
//
// Please design your own test for this one 
//
} (* end of [main0] *)

(* ****** ****** *)

(* end of [minmaxHT.dats] *)
