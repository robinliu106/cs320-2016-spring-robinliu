(* ****** ****** *)

#include "./../midterm-1.dats"

(* ****** ****** *)

(*
//
// HX: 10 points
// Please implement mylist_altmerge
//
*)

implement
{a}
mylist_altmerge(xs, ys) =
    case+ (xs,ys) of
    | (nil0(), _ ) => ys 
    | ( _ ,nil0()) => xs
    | (cons0(x,xs),cons0(y,ys)) => cons0(x,cons0(y,mylist_altmerge(ys,xs)))
        


(* ****** ****** *)

implement
main0() =
{
//
val xs = g0ofg1($list{int}(1, 2, 3))
val ys = g0ofg1($list{int}(4, 5, 6, 7, 8))
//
val () = println! ("altmerge(xs, ys) = ", mylist_altmerge<int>(xs, ys))
val () = println! ("altmerge(ys, xs) = ", mylist_altmerge<int>(ys, xs))
//
} (* end of [main0] *)

(* ****** ****** *)

(* end of [submaxord.dats] *)