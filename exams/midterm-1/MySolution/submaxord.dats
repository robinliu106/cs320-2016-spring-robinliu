(* ****** ****** *)

#include "./../midterm-1.dats"

(* ****** ****** *)

(*
//
// HX: 10 points
// Please implement mylist_submaxord
//
*)

(* ****** ****** *)

implement
main0() =
{
//
val xs = g0ofg1($list{int}(1, 3, 2, 4))
val ys = mylist_submaxord(xs)
val () = println! ("ys = ", ys)
val xs = g0ofg1($list{int}(4, 1, 2, 3, 8, 9, 5, 6, 7))
val ys = mylist_submaxord(xs)
val () = println! ("ys = ", ys)
//
} (* end of [main0] *)

(* ****** ****** *)

(* end of [submaxord.dats] *)