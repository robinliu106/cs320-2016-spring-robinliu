(* ****** ****** *)

#include "./../midterm-1.dats"

(* ****** ****** *)

(*
//
// HX: 10 points
// Please implement mylist_pairing
//
*)

implement
{a}
mylist_pairing(xs) = let
val (xs,ys) = mylist_split(xs)
val ys = mylist_reverse(ys)
in
    mylist_zip(xs,ys)
end

(* ****** ****** *)

implement
main0() = () where
{
//
val xs =
g0ofg1 (
  $list{int}(1, 2, 3, 4, 5, 6)
) (* val *)
//
val xys = mylist_pairing<int>(xs)
//
val () = println! ("xys = ", xys)
//
} (* end of [main0] *)

(* ****** ****** *)

(* end of [pairing.dats] *)
