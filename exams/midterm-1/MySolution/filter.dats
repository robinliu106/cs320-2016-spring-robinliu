(* ****** ****** *)

#include "./../midterm-1.dats"
 
(* ****** ****** *)

(*
//
// HX: 10 points
// Please implement mytree_filter
//
*)


implement
{a}
mytree_filter(xs,p) = let
fun aux(xs: tree0(a)): list0(a) = 
(
case+ xs of
| tnil0() => tnil0()
| tcons0(xs_l,_,xs_r) =>
    if p(xs_l) then tcons0(xs_l,aux(xs),xs_r) else aux(xs)
)
//
in 
  aux(xs) 
end 





(* ****** ****** *)

implement
main0() = () where
{
//
// Please design your own test for this one 
//
} (* end of [main0] *)

(* ****** ****** *)

(* end of [filter.dats] *)