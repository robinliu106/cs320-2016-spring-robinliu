(* ****** ****** *)

#include "./../midterm-1.dats"

(* ****** ****** *)

(*
//
// HX: 10 points
// Please implement A_draw
//
*)
implement A_draw(n) = let

    fun drawChar(c: char, n: int): void =
        if n <= 0 
        then ()
        else let
            val _ = print c
        in
            drawChar(c,n-1)
        end

    fun drawTip(level: int): void = let
        val _ = drawChar(' ',2*n)
        val _ = drawChar('*',1)
        val _ = println!() 
    in 
    end
    
    fun drawTop(level: int,innerSpace: int): void = 
        if level = n
        then () else let
      val _ = drawChar(' ', 2*n-level)
      val _ = drawChar('*', 1)
      val _ = drawChar(' ', innerSpace)
      val _ = drawChar('*', 1)
      val _ = println!()
      
      in
        drawTop(level + 1,innerSpace+2)
      end

       
    fun drawLine(n: int): void = let
        val _ = drawChar(' ',n)
        val _ = drawChar('*',n*2+1)
    in 
    end
    
    fun drawBottom(level: int): void = 
        if level > n
        then () else
        let
            val _ = drawChar(' ',n-level)
            val _ = drawChar('*',1)
            val _ = drawChar(' ',2*(level+n)-1)
            val _ = drawChar('*',1)
            val _ = println!()
        in 
            drawBottom(level+1)
        end
    
    val _ = drawTip(n) //draw tip of the a
    val _ = drawTop(1,1) //draw top portion of the letter
    val _ = drawLine(n)//draw horizontal line
    val _ = println!()
    val _ = drawBottom(1) //draw bottom portion
    val _ = println!()

in
end


(* ****** ****** *)

implement
main0() = () where
{
//
val () = A_draw(5)
//
val () = A_draw(2)
//
} (* end of [main0] *)

(* ****** ****** *)

(* end of [A_draw.dats] *)