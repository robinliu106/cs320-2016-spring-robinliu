(* ****** ****** *)

#include "./../midterm-1.dats"

(* ****** ****** *)

(*
//
// HX: 10 points
//
// HX: 20 points if done based on combinators
//
// Please implement mylist_average
//
*)

implement
{a}
mylist_average(xs) = let
val sum = 0
val count = 0
in
fun aux(xs: list0(double),sum: int,count: int) = let
  case+ xs of
  | nil0() => nil0()
  | cons0(x,xs) => let
    cons0( sum/count , aux(xs,sum+x,count++))
in
  aux(xs,sum,count)
end



(* ****** ****** *)

implement
main0() = () where
{
//
val xs =
g0ofg1 (
$list{double}
  (1.0, 2.0, 3.0, 4.0, 5.0)
) (* val *)
//
val ys = mylist_average(xs)
//
(*
The following line is expected:
ys = 1.000000, 1.500000, 2.000000, 2.500000, 3.000000
*)
//
val () = println! ("ys = ", ys)
//
} (* end of [main0] *)

(* ****** ****** *)

(* end of [average.dats] *)