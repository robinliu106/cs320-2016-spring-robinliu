(* ****** ****** *)

#include "./../midterm-1.dats"

(* ****** ****** *)

(*
//
// HX: 10 points
// Please implement mytree_is_AVLtree
//
*)


implement
{a}
mytree_is_AVLtree(xs) = let
fun aux(xs: tree0(a)): bool =
(
case+ xs of
| tnil0() => true
| tcons0(xs_l, _, xs_r) => let
    val sz_l = mytree_size(xs_l)
    val sz_r = mytree_size(xs_r)
  in
    aux(xs_l) && aux(xs_r) && (sz_r <= sz_l) && (sz_l <= sz_r+1) && 
    (mytree_height(xs_l) <= mytree_height(xs_r) + 1)
  end 
)
in
  aux(xs)
end 

(* ****** ****** *)

implement
main0() = () where
{
//
// Please design your own test for this one 
//
} (* end of [main0] *)

(* ****** ****** *)

(* end of [is_AVLtree.dats] *)
