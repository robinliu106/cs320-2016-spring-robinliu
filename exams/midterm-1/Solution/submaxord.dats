(* ****** ****** *)

#include "./../midterm-1.dats"

(* ****** ****** *)

(*
//
// HX: 10 points
// Please implement mylist_submaxord
//
*)

(* ****** ****** *)

implement
mylist_submaxord(xs) = let
//
fun
aux0
(
  xs: list0(int)
) : list0(int) =
(
case+ xs of
| list0_nil() => list0_nil()
| list0_cons(x, xs) => let
    val ys0 = aux0(xs)
    val ys1 = aux1(x, xs)
  in
    if length(ys0) >= length(ys1)+2 then ys0 else cons0(x, ys1)
  end // end of [list0_cons]
)
//
and
aux1
(
  x0: int, xs: list0(int)
) : list0(int) =
(
case+ xs of
| list0_nil() => list0_nil()
| list0_cons(x1, xs) =>
  (
    if x1 < x0
      then aux1(x0, xs)
      else let
        val ys0 = aux1(x0, xs)
        val ys1 = aux1(x1, xs)
      in
        if length(ys0) >= length(ys1)+2 then ys0 else cons0(x1, ys1)
      end // end of [else]
  ) (* end of [list0_cons] *)
)
//
in
  aux0(xs)
end // end of [submaxord]

(* ****** ****** *)

implement
main0() =
{
//
val xs = g0ofg1($list{int}(1, 3, 2, 4))
val ys = mylist_submaxord(xs)
val () = println! ("ys = ", ys)
val xs = g0ofg1($list{int}(4, 1, 2, 3, 8, 9, 5, 6, 7))
val ys = mylist_submaxord(xs)
val () = println! ("ys = ", ys)
//
} (* end of [main0] *)

(* ****** ****** *)

(* end of [submaxord.dats] *)
