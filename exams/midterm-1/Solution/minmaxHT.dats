(* ****** ****** *)

#include "./../midterm-1.dats"

(* ****** ****** *)

(*
//
// HX: 10 points
// Please implement mytree_minmaxHT
//
*)

(* ****** ****** *)
//
implement
{a}(*tmp*)
mytree_minmaxHT
  (t0) =
mytree_fold<a, (int,int)>
(
  t0
, lam(xs_l, _, xs_r) => (1+max(xs_l.0, xs_r.0), 1+min(xs_l.1,xs_r.1))
, (0, 0)
) (* end of [mytree_fold] *)
//
(* ****** ****** *)

implement
main0() = () where
{
//
// Please design your own test for this one 
//
} (* end of [main0] *)

(* ****** ****** *)

(* end of [minmaxHT.dats] *)
