(* ****** ****** *)

#include "./../midterm-1.dats"

(* ****** ****** *)

(*
//
// HX: 10 points
// Please implement mytree_is_AVLtree
//
*)

(* ****** ****** *)
  
implement
{a}(*tmp*)
mytree_is_AVLtree
  (xs) = let
//
exception NotAVLExn of ()
//
fun
aux
(
  xs: tree0(a)
) : int =
  case+ xs of
  | tnil0() => 0
  | tcons0(xs_l, _, xs_r) => let
      val hl = aux(xs_l)
      and hr = aux(xs_r)
      val () =
        if abs(hl-hr) >= 2 then $raise NotAVLExn()
      // end of [val]
    in
      1 + max(hl, hr)
    end
// end of [aux]
//
in
  try let val _ = aux(xs) in true end with ~NotAVLExn() => false
end // end of [mytree_is_AVLtree]  
  
(* ****** ****** *)

implement
main0() = () where
{
//
// Please design your own test for this one 
//
} (* end of [main0] *)

(* ****** ****** *)

(* end of [is_AVLtree.dats] *)
