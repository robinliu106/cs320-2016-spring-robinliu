(* ****** ****** *)

#include "./../midterm-1.dats"

(* ****** ****** *)

(*
//
// HX: 10 points
//
// HX: 20 points if done based on combinators
//
// Please implement mylist_average
//
*)

(* ****** ****** *)

implement
mylist_average
  (xs) = let
//
val ys =
mylist_foldleft<double,list0(double)>
( xs
, list0_nil
, lam(ys, x) =>
  if iseqz(ys)
    then list0_sing(x) else list0_cons(x+mylist_head<double>(ys), ys)
  // end of [if]
) (* end of [val] *)
//
(*
//
// Take a look at [ys]:
//
val () =
  println! ("ys = ", ys)
*)
//
val nys = mylist_length(ys)
//
in
  mylist_ifoldleft<double,list0(double)>(ys, list0_nil, lam(zs, i, y) => list0_cons(y/(nys-i), zs))
end // end of [mylist_average]

(* ****** ****** *)

implement
main0() = () where
{
//
val xs =
g0ofg1 (
$list{double}
  (1.0, 2.0, 3.0, 4.0, 5.0)
) (* val *)
//
val ys = mylist_average(xs)
//
(*
The following line is expected:
ys = 1.000000, 1.500000, 2.000000, 2.500000, 3.000000
*)
//
val () = println! ("ys = ", ys)
//
} (* end of [main0] *)

(* ****** ****** *)

(* end of [average.dats] *)