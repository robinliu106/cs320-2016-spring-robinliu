(* ****** ****** *)

#include "./../midterm-1.dats"

(* ****** ****** *)

(*
//
// HX: 10 points
// Please implement A_draw
//
*)

(* ****** ****** *)

implement
main0() = () where
{
//
val () = A_draw(1)
//
val () = A_draw(2)
//
} (* end of [main0] *)

(* ****** ****** *)

(* end of [A_draw.dats] *)