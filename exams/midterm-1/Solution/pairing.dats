(* ****** ****** *)

#include "./../midterm-1.dats"

(* ****** ****** *)

(*
//
// HX: 10 points
// Please implement mylist_pairing
//
*)

(* ****** ****** *)

implement
{a}(*tmp*)
mylist_pairing
  (xs) = let
//
val n2 =
list0_length(xs) / 2 
//
fun
aux
(
  xs: list0(a), ys: list0(a), i: int
) : list0(@(a, a)) = (
//
if
(i < n2)
then let
  val-list0_cons(x, xs) = xs
  val-list0_cons(y, ys) = ys
in
  list0_cons((x, y), aux(xs, ys, i+1))
end // end of [then]
else list0_nil() // end of [else]
//
) (* end of [aux] *)
//
in
  aux(xs, list0_reverse(xs), 0)
end // end of [mylist_pairing]

(* ****** ****** *)

implement
main0() = () where
{
//
val xs =
g0ofg1 (
  $list{int}(1, 2, 3, 4, 5, 6)
) (* val *)
//
val xys = mylist_pairing<int>(xs)
//
val () = println! ("xys = ", xys)
//
} (* end of [main0] *)

(* ****** ****** *)

(* end of [pairing.dats] *)
