(* ****** ****** *)

#include "./../midterm-1.dats"

(* ****** ****** *)

(*
//
// HX: 10 points
// Please implement mytree_filter
//
*)

(* ****** ****** *)
//
implement
{a}(*tmp*)
mytree_filter
  (xs, p) =
mytree_fold<a,list0(a)>
( 
  xs
, lam(xs_l, x, xs_r) => 
    if p(x) 
    then list0_append(xs_l, list0_cons(x, xs_r))
    else list0_append(xs_l, xs_r)
  // end of [lam]
, list0_nil()
) (* end of [mytree_filter] *)
//
(* ****** ****** *)

implement
main0() = () where
{
//
// Please design your own test for this one 
//
} (* end of [main0] *)

(* ****** ****** *)

(* end of [filter.dats] *)