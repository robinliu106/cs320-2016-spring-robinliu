(* ****** ****** *)

#include "./../midterm-1.dats"

(* ****** ****** *)

(*
//
// HX: 10 points
// Please implement mylist_altmerge
//
*)

(* ****** ****** *)

implement
{a}(*tmp*)
mylist_altmerge
  (xs, ys) = let
//
fun
aux
(
  xs: list0(a), ys: list0(a)
) : list0(a) =
(
case+
(xs, ys) of
| (_, list0_nil()) => xs
| (list0_nil(), _) => ys
| (list0_cons(x, xs), list0_cons(y, ys)) =>
    list0_cons(x, list0_cons(y, aux(xs, ys)))
)
//
in
  aux(xs, ys)
end // end of [mylist_altmerge]

(* ****** ****** *)

implement
main0() =
{
//
val xs = g0ofg1($list{int}(1, 2, 3))
val ys = g0ofg1($list{int}(4, 5, 6, 7, 8))
//
val () = println! ("altmerge(xs, ys) = ", mylist_altmerge<int>(xs, ys))
val () = println! ("altmerge(ys, xs) = ", mylist_altmerge<int>(ys, xs))
//
} (* end of [main0] *)

(* ****** ****** *)

(* end of [submaxord.dats] *)