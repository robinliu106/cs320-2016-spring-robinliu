//
// Title:
// Concepts of Programming Languages
// Number: CAS CS 320
// Semester: Spring 2016
// Class Time: TR 12:30-2:00pm
// Instructor: Hongwei Xi (hwxiATbuDOTedu)
// Teaching Fellow: Hanwen (Steinway) Wu (hwwuATbuDOTedu)
//
(* ****** ****** *)
//
// HX-2016-04-19:
//
(* ****** ****** *)

staload "libats/ML/SATS/basis.sats"

(* ****** ****** *)
//
// Midterm-2
//
(* ****** ****** *)
//
datatype
tree0 (a:t@ype) =
  | tree0_nil of ()
  | tree0_cons of (tree0 a, a, tree0 a)
//
(* ****** ****** *)
(*
**
** Q1: 10 points
**
*)
//
// A list is a sublist of another list if the former can be obtained from
// removing some elements in the latter. For instance, [1,3,5] is a sublist
// of [1,2,3,4,5] but [1,1] is not a sublist of [1,2,3]. Also, [2,1] is not
// a sublist of [1,2,3].
//
// Given two lists [xs] and [ys], the function sublist_test(xs, ys) returns
// true if and only if [ys] is a sublist of [xs]:
//
extern
fun
sublist_test
  (xs: list0(int), ys: list0(int)): bool
//
(* ****** ****** *)
(*
//
// Q2: 10 points
//
// [mylist_duprem_l] keeps the leftmost occurrence
// of each element and removes all of its duplicates
//
// [mylist_duprem_r] keeps the rightmost occurrence
// of each element and removes all of its duplicates
//
// For instance,
// mylist_duprem_l([1,2,3,4,3,2,1]) returns [1,2,3,4]
// mylist_duprem_r([1,2,3,4,3,2,1]) returns [4,3,2,1]
//
*)
//
extern
fun
mylist_duprem_l(xs: list0(int)): list0(int)
//
extern
fun
mylist_duprem_r(xs: list0(int)): list0(int)
//
(* ****** ****** *)

(*
//
// Q3: 10 points
// [mystream_duprem] keeps the leftmost occurrence
// of each element and removes all of its duplicates
//
*)
extern
fun
mystream_duprem(xs: stream(int)): stream(int)
  
(* ****** ****** *)
//
(*
//
// Q4: 10 points
//
Please implement a function which checks whether a binary tree is a
max-heap.  A binary tree is a max-heap if it is empty or its root is larger
than or equal to all the other nodes in it and its left and right sub-trees
are also max-heaps.
//
*)
extern
fun
tree0_is_heap (t0: tree0 (int)): bool
//
(* ****** ****** *)
(*
//
// Q5: 10 points
//
//
// A binary tree t0 is a braun tree if
// (1) t0 is nil, or
// (2) both left and right children of t0 are
//     braun trees and
//     size-of-right-child <= size-of-left-child <= size-of-right-child+1
//
// The function [mylist_to_brauntree] turns a list into a Braun tree
// such that each element in the list appears in the Braun tree and vice versa.
//
*)
//
extern
fun
{a:t@ype}
mylist2brauntree(list0(a)): tree0(a)
//
(* ****** ****** *)
(*
**
** Q6: 10 points
**
*)
(*
You can find Wallis product here:
http://en.wikipedia.org/wiki/Wallis_product
Please implement a function that generates a stream
consisting of all the partial products in Wallis product:
*)
//
extern fun Wallis((*void*)): stream (double)
//
(* ****** ****** *)
//
(*
//
// Q7: 10 points
//
// Given a list of positive numbers xs and an integer n,
// [subseq_sum_test] returns true if and only if there is
// a subsequence of xs such that the sum of all the integers
// in this subsequence equals n. For instance, we have
// 
// subseq_sum_test([1,2,3,4], 9) returns true
// subseq_sum_test([1,2,3,4], 11) returns false
//
*)
//
extern
fun
subseq_sum_test (xs: list0(int), n: int): bool
//
(* ****** ****** *)

(*
//
// Q8: 10 points
//
// Given a non-empty list of integers, mylist_find_freq
// returns the one that appears most frequently in this list.
// If there are several integers satisfying the criterion,
// please return the largest one among them.
// For instance, we have
//
// mylist_find_freq([1,3,1,1,2,2,4,2,3]) returns 2
//
*)
extern fun mylist_find_freq(xs: list0(int)): int

(* ****** ****** *)

(*
//
// Q9: 10 points
//
// Given a list xs of integers containing at least 3 element,
// mylist_triangle_test(xs) returns true if there exists three
// elements in xs that can form the three sides of a triangle;
// otherwise, mylist_triangle_test(xs) returns false.
//
*)
extern fun mylist_triangle_test(xs: list0(int)): bool

(* ****** ****** *)

(*
**
** Q10: 10 points
**
*)
(*
Given a stream xss of streams xs_0, xs_1, xs_2, ...,
the function [stream_transpose] produces another stream
yss of streams ys_0, ys_1, ys_2, ..., where we have
xs_i(j) equals ys_j(i).

You may assume every stream xs_n is infinite for n >= 0.

For instance,
suppose we have:

xs_0 = 0, 1, 2, 3, ...
xs_1 = 0, 1, 2, 3, ...
xs_2 = 0, 1, 2, 3, ...
xs_3 = 0, 1, 2, 3, ...
...

Then we should have

ys_0 = 0, 0, 0, 0, ...
ys_1 = 1, 1, 1, 1, ...
ys_2 = 2, 2, 2, 2, ...
ys_3 = 3, 3, 3, 3, ...

*)
//
extern
fun
{a:t@ype}
stream_transpose(xss: stream(stream(a))): stream(stream(a))
//
(* ****** ****** *)

(*
** Q11: 20 points
*)
//
abstype mymat = ptr // a 2D matrix of doubles
//
extern
fun mymat_get_at (mymat, i: int, j: int): double
and mymat_remove_row (M: mymat, nrow: int): mymat
and mymat_remove_col (M: mymat, ncol: int): mymat
//
// Please implement the following function based on the
// above ones (which are already implemented for you):
//
extern
fun mymat_eval_det (M: mymat, n: int): double
//
// which evaluates the determinant of a given square matrix M
// of dimension n by n.
// See http://en.wikipedia.org/wiki/Determinant for the definition
// of the determinant of a square matrix and the Laplace's formula
// for computing its determinant.

(* ****** ****** *)
//
(*
//
// Q12: 20 points
//
// The permutations of 0,1,2 can be ordered
// according to the lexicographic ordering as follows:
//
// (0,1,2) < (0,2,1) < (1,0,2) < (1,2,0) < (2,0,1) < (2,1,0)
//
// This ordering can be readily generalized to the permuations
// of n numbers, which n is positive. Given a permutation xs of
// the first n natural numbers, perm_succ(xs) returns the next
// permutation following xs or it raises an exception LastPermExn().
// Please implement [perm_succ].
//
*)
//
exception LastPermExn of ()
//
extern fun perm_succ(xs: list0(int)): list0(int)
//
(* ****** ****** *)

(* end of [midterm-2.dats] *)
