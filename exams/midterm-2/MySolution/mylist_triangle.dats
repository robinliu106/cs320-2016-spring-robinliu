//
// Title:
// Concepts of Programming Languages
// Number: CAS CS 320
// Semester: Spring 2016
// Class Time: TR 12:30-2:00pm
// Instructor: Hongwei Xi (hwxiATbuDOTedu)
// Teaching Fellow: Hanwen (Steinway) Wu (hwwuATbuDOTedu)
//
(* ****** ****** *)
//
// HX-2016-04-19:
//
(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
#include
"share/HATS/atspre_staload_libats_ML.hats"
//
(* ****** ****** *)

staload "./../midterm-2.dats"

(* ****** ****** *)

implement mylist_triangle_test(xs) = 
if (xs[0] + xs[1] <= xs[1]) || (xs[1] + xs[2] <= xs[0]) || (xs[2] + xs[0] <= xs[1]) then false 
else true

implement
main0() = () where
{
    
val xs = g0ofg1($list{int}(5,1,3))
val () = println!(mylist_triangle_test(xs))
//
} (* end of [main0] *)

(* ****** ****** *)

(* end of [mylist0_triangle.dats] *)
