//
// Title:
// Concepts of Programming Languages
// Number: CAS CS 320
// Semester: Spring 2016
// Class Time: TR 12:30-2:00pm
// Instructor: Hongwei Xi (hwxiATbuDOTedu)
// Teaching Fellow: Hanwen (Steinway) Wu (hwwuATbuDOTedu)
//
(* ****** ****** *)
//
// HX-2016-04-19:
//
(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
#include
"share/HATS/atspre_staload_libats_ML.hats"
//
(* ****** ****** *)

staload "./../midterm-2.dats"

(* ****** ****** *)

extern
fun{a:t@ype}
helper(ls: list0 (int), b: int): list0(int)


implement
mylist_duprem_r(xs) = let
fun aux(x: int, ys: list0 (int)) : bool =
(
  case+ ys of
  | list0_cons
      (y, ys) => if x = y then true else aux (x, ys)
  | list0_nil () => false
)
in
case+ xs of
| list0_cons
    (x, xs) => let
    val ys = mylist_duprem_r(xs)
  in
    if aux (x, ys) then ys else list0_cons (x, ys)
  end // end of [list0_cons]
| list0_nil () => list0_nil ()
end // end of [duprem_r]



implement
{a}
helper(xs, a) = 
(
case+ xs of
|list0_nil()=> nil0()
|list0_cons (x,xs1) => if x = a then helper(xs1,a) else list0_cons(x,helper(xs1,a))
)


implement
mylist_duprem_l (xs) =
(
case+ xs of
|list0_nil() => xs
|list0_cons(x,xs1) => list0_cons(x,mylist_duprem_l(helper(xs1,x)))
)




implement
main0() = () where
{
//
val xs = g0ofg1($list{int}(1,2,1,1,2,2,3,4,5,1,2,3))
val a = g0ofg1($list{int}(1,2,3,4,3,2,1))
val () = println! ("duprem_l(xs) = ", mylist_duprem_l(a))
val () = println! ("duprem_r(xs) = ", mylist_duprem_r(a))
//
} (* end of [main0] *)

(* ****** ****** *)

(* end of [duprem_lr.dats] *)
