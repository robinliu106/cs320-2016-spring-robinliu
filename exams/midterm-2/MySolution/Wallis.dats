//
// Title:
// Concepts of Programming Languages
// Number: CAS CS 320
// Semester: Spring 2016
// Class Time: TR 12:30-2:00pm
// Instructor: Hongwei Xi (hwxiATbuDOTedu)
// Teaching Fellow: Hanwen (Steinway) Wu (hwwuATbuDOTedu)
//
(* ****** ****** *)
//
// HX-2016-04-19:
//
(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
#include
"share/HATS/atspre_staload_libats_ML.hats"
//
(* ****** ****** *)

staload "./../midterm-2.dats"


#include
"share/HATS/atspre_staload_libats_ML.hats"
#define  :: stream_cons
//
extern
fun
from : (double) -> stream(double)
implement
from(n) = $delay(stream_cons(n, from(n+1)))

fun
partialsum(xs0: stream(double)) : stream(double) = let
//
fun
aux
(sum: double, xs0: stream(double)) : stream(double) =
$delay(
  case+ !xs0 of
  | stream_nil() => stream_nil()
  | stream_cons(x, xs) => stream_cons(sum, aux(sum*x, xs)) //multiply the product 
) (* $delay *)
//
in
  aux(1.0, xs0) //start at 1 instead of 0 b/c of multiplication
end // end of [partialsum] modified from previous hw partialsum

fun Wallis(): stream(double) = let
    val xs0 = from(1.0) // 1.0, 2.0, 3.0, 4.0 ...
    val xs1 = stream_map_cloref<double><double>(xs0, lam(x) => $effmask_all(  ((2.0*x)/(2.0*x-1.0)) * ((2.0*x)/(2.0*x+1.0))  )) 
in
    partialsum(xs1)
end // end of [Wallis]


/////////////////////////////////////


(* ****** ****** *)

implement
main0() = () where
{
//
val a = stream_nth_exn(Wallis() , 100000)
val () = println!("Answer is = 1.57079632679")
val () = println!("Wallis Product = ", a )
} (* end of [main0] *)

(* ****** ****** *)

(* end of [Wallis.dats] *)
