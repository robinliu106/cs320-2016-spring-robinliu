//
// Title:
// Concepts of Programming Languages
// Number: CAS CS 320
// Semester: Spring 2016
// Class Time: TR 12:30-2:00pm
// Instructor: Hongwei Xi (hwxiATbuDOTedu)
// Teaching Fellow: Hanwen (Steinway) Wu (hwwuATbuDOTedu)
//
(* ****** ****** *)
//
// HX-2016-04-19:
//
(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
#include
"share/HATS/atspre_staload_libats_ML.hats"
//
(* ****** ****** *)

staload "./../midterm-2.dats"

(* ****** ****** *)

extern
fun
helper(t0: tree0 (int), b: int): bool

implement
tree0_is_heap(t0) = (
    case t0 of
    |tree0_nil() => true
    |tree0_cons(l,x,r) => helper(l,x) && helper(r,x)
) 

implement helper(t0, t1) = (
    case t0 of
    |tree0_nil() => true
    |tree0_cons(l,x,r) => 
        if x <= t1 
        then helper(l,x) && helper(r,x) 
        else false
)

implement
main0() = () where
{
//val () = println!("Go!")
} (* end of [main0] *)

(* ****** ****** *)

(* end of [tree_is_heap.dats] *)