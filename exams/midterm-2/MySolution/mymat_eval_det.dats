//
// Title:
// Concepts of Programming Languages
// Number: CAS CS 320
// Semester: Spring 2016
// Class Time: TR 12:30-2:00pm
// Instructor: Hongwei Xi (hwxiATbuDOTedu)
// Teaching Fellow: Hanwen (Steinway) Wu (hwwuATbuDOTedu)
//
(* ****** ****** *)
//
// HX-2016-04-19:
//
(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
#include
"share/HATS/atspre_staload_libats_ML.hats"
//
(* ****** ****** *)

staload "./../midterm-2.dats"

(* ****** ****** *)
//
local
//
assume mymat =
  (int, int) -<cloref1> double
//
in
//
implement
mymat_get_at(M, i, j) = M(i, j)
//
implement
mymat_remove_row(M, i0) =
  lam(i, j) => M(if i < i0 then i else i+1, j)
implement
mymat_remove_col(M, j0) =
  lam(i, j) => M(i, if j < j0 then j else j+1)
//
end // end of [local]
//
(* ****** ****** *)

implement
main0() = () where
{
//
val () =
println!
  ("Please design your own test!")
//
} (* end of [main0] *)

(* ****** ****** *)

(* end of [mymat_eval_det.dats] *)
