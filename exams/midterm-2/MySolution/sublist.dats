//
// Title:
// Concepts of Programming Languages
// Number: CAS CS 320
// Semester: Spring 2016
// Class Time: TR 12:30-2:00pm
// Instructor: Hongwei Xi (hwxiATbuDOTedu)
// Teaching Fellow: Hanwen (Steinway) Wu (hwwuATbuDOTedu)
//
(* ****** ****** *)
//
// HX-2016-04-19:
//
(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
#include
"share/HATS/atspre_staload_libats_ML.hats"
//
(* ****** ****** *)

staload "./../midterm-2.dats"

(* ****** ****** *)

fun sublist_test(xs, ys) = let
fun aux(xs: list0(int), ys: list0(int), zs: list0(int)) : bool = (
case+ xs of
|list0_nil() => false
|list0_cons(x,x1) => (
case+ ys of
|list0_nil() => true
|list0_cons(y,y1) => (
case+ x1 of
    |list0_nil() => 
        if x = y 
        then aux(x1, y1, list0_nil()) 
        else aux(x1, ys, list0_cons(x,nil0()))
    |list0_cons(i,g)=> 
        if x = y 
        then aux(list0_append(x1,zs),y1, list0_nil()) 
        else aux(x1,ys, list0_cons(x,zs))
)))
in
    aux(xs,ys,list0_nil())
end


implement
main0() = () where
{
//
val xs1 = g0ofg1($list{int}(1,2))
val xs2 = g0ofg1($list{int}(1,2,1,1,5))
//
val () = println!(xs1," is a sublist of ", xs2)
val () = assertloc(sublist_test(xs2, xs1))
val () = println!(sublist_test(xs2,xs1))
//val () = println!("Robin is awesome!")
//
} (* end of [main0] *)

(* ****** ****** *)

(* end of [sublist.dats] *)
