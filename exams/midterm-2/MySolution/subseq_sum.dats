//
// Title:
// Concepts of Programming Languages
// Number: CAS CS 320
// Semester: Spring 2016
// Class Time: TR 12:30-2:00pm
// Instructor: Hongwei Xi (hwxiATbuDOTedu)
// Teaching Fellow: Hanwen (Steinway) Wu (hwwuATbuDOTedu)
//
(* ****** ****** *)
//
// HX-2016-04-19:
//
(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
#include
"share/HATS/atspre_staload_libats_ML.hats"
//
(* ****** ****** *)

staload "./../midterm-2.dats"

(* ****** ****** *)

fun subseq_sum_test(xs, n) = let
fun aux(xs: list0(int), a: int, n: int): bool = (
case+ xs of
|list0_nil() => a = n
|list0_cons(x,xss) => aux(xss, a, n) || aux(xss, a+x, n)
)
in
    aux(xs, 0, n)
end





implement
main0() = () where
{
//
val a = list0_make_intrange (1,5)
val () = println!(a)
val () = println!("should be true, subseq_sum_test(a,9) = ",subseq_sum_test(a,9))
val () = println!("should be false, subseq_sum_test(a,11) = ",subseq_sum_test(a,11))
//val () = println!("Robin is awesome!")
//
} (* end of [main0] *)

(* ****** ****** *)

(* end of [subseq_sum.dats] *)
