//
// Title:
// Concepts of Programming Languages
// Number: CAS CS 320
// Semester: Spring 2016
// Class Time: TR 12:30-2:00pm
// Instructor: Hongwei Xi (hwxiATbuDOTedu)
// Teaching Fellow: Hanwen (Steinway) Wu (hwwuATbuDOTedu)
//
(* ****** ****** *)
//
// HX-2016-04-19:
//
(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
#include
"share/HATS/atspre_staload_libats_ML.hats"
//
(* ****** ****** *)

staload "./../midterm-2.dats"

(* ****** ****** *)

#include
"share/HATS/atspre_staload_libats_ML.hats"
#define  :: stream_cons
//
extern
fun
from : (int) -> stream(int)
implement
from(n) = $delay(stream_cons(n, from(n+1)))

fun
partialsum(xs0: stream(double)) : stream(double) = let
//
fun
aux
(sum: double, xs0: stream(double)) : stream(double) =
$delay(
  case+ !xs0 of
  | stream_nil() => stream_nil()
  | stream_cons(x, xs) => stream_cons(sum, aux(sum*x, xs)) //multiply the product 
) (* $delay *)
//
in
  aux(1.0, xs0) //start at 1 instead of 0 b/c of multiplication
end // end of [partialsum] modified from previous hw partialsum


extern
fun
{a:t@ype}
stream_transpose(xss: stream(stream(a))): stream(stream(a))

(*
implement
stream_transpose(xss) = let
  val a = from(1)
in
stream_transpose(xss)
*)

implement
main0() = () where
{
//
val xs = from(0)
//val () = stream_transpose(xs)
val () = println!("Hi!")
//
} (* end of [main0] *)

(* ****** ****** *)

(* end of [stream_transpose.dats] *)
