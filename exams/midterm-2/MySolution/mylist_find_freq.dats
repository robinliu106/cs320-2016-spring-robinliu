//
// Title:
// Concepts of Programming Languages
// Number: CAS CS 320
// Semester: Spring 2016
// Class Time: TR 12:30-2:00pm
// Instructor: Hongwei Xi (hwxiATbuDOTedu)
// Teaching Fellow: Hanwen (Steinway) Wu (hwwuATbuDOTedu)
//
(* ****** ****** *)
//
// HX-2016-04-19:
//
(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
#include
"share/HATS/atspre_staload_libats_ML.hats"
//
(* ****** ****** *)

staload "./../midterm-2.dats"

(* ****** ****** *)



extern fun helper(xs: list0(int), a: int, b: int): int

implement 
helper(xs, a, b) =
(
case+ xs of
|list0_nil()=> b
|list0_cons(x,xss) => if x = a 
    then helper(xss,a,b+1) 
    else helper(xss,a,b)
)

fun mylist_find_freq(xs) = let
fun aux(xs: list0(int), a: int, b: int):int = 
(
case+ xs of
|list0_nil() => a
|list0_cons(x,xs1) => 
    if helper(xs,x,0) > b 
    then aux(xs1,x,helper(xs1,x,helper(xs,x,0)))
    else if helper(xs, x, 0) = b && a < x 
    then aux(xs1,x,b)
    else aux(xs1,a,b)
)
in
    aux(xs,0,0)
end

implement
main0() = () where
{
//
//val () = println!("Robin is awesome!")
val a = list0_make_intrange(1,8)
val () = println!(mylist_find_freq(a))
//
} 

(* ****** ****** *)

(* end of [mylist0_find_freq.dats] *)