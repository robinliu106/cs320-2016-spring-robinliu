(* ****** ****** *)

#include "./../assign4.dats"

(* ****** ****** *)

(*
** HX-2016-02-12:
** please implement mylist_zip3 here
*)
implement
{a,b,c}
mylist_zip3(xs, ys, zs) = let
//
fun
aux
( xs: list0(a), ys: list0(b), zs: list0(c) ) : list0(@(a, b, c)) =
(
case+ (xs, ys, zs) of
| (list0_nil(), _, _) => list0_nil()
| (_, list0_nil(), _) => list0_nil()
| (_, _, list0_nil()) => list0_nil()
| (list0_cons(x, xs),list0_cons(y, ys),list0_cons(z,zs)) => list0_cons((x, y, z), aux(xs, ys, zs))
)
//val () = println!(xs)//aux(xs, ys,zs)
//val () = println!(a)
in
  aux(xs, ys, zs)
end 
(* ****** ****** *)

implement
show_poles
(
  R, H, xs1, xs2, xs3
) = let
//
fun
show_disk3
(
  xyz: (int, int, int)
) : void =
{
  val () = show_disk(R, xyz.0)
  val () = show_disk(R, xyz.1)
  val () = show_disk(R, xyz.2)
  val () = print_newline()
}
//
val xs1 = pole_padding(H, xs1)
val xs2 = pole_padding(H, xs2)
val xs3 = pole_padding(H, xs3)
//
val xyzs = mylist_zip3<int,int,int>(xs1, xs2, xs3)
val () = println!(xyzs)
//
in
  mylist_foreach(xyzs, lam(xyz) => show_disk3(xyz))
end // end of [show_poles]

(* ****** ****** *)

#ifdef
MAIN_NONE
#then
#else
implement
main0 (argc, argv) =
{
//
// HX: here is some testing code:
//
val width = 10
val height = 10
//
val P1 = cons0(1, cons0(3, nil0()))
val P2 = cons0(2, cons0(4, cons0(5, nil0())))
val P3 = cons0(6, cons0(7, cons0(8, nil0())))

// TEST
val a = mylist_zip3(P1, P2, P3)
// TEST


val () = show_poles(width, height, P1, P2, P3)
//
val () = println! ("Good news: Your code has passed initial testing!")
//
} (* end of [main0] *)
#endif // #ifdef

(* ****** ****** *)

(* end of [assign4_sol.dats] *)
