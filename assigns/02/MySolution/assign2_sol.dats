(* ****** ****** *)

#include "./../assign2.dats"

(* ****** ****** *)
//
// HX:
// Please replace
// this dummy implementation
//


extern fun repeat (string, int): void 
implement repeat(s,n) = 
if n = 0 
     then ()
     else 
        let 
            val _ = print s 
        in 
            repeat (s, n-1)
end 

extern fun drawTriangle(n:int,l:int,level:int,space:int):void         
//collaborated with Biken on show_triangle
implement

show_triangle(level) = 
let
    val _ = level
in 
    drawTriangle(level,1,level,1) 
end

implement drawTriangle(x,y,level,space) =
   if (level > 0) then
        if (x > 0) then let 
            val _ = print!(" ")
        in
            drawTriangle(x-1,y,level,space)
        end
    else if (y > 0)  then let 
            val _= print!("*")
            val length = y
        in
            drawTriangle(x,y-1,level,space)
        end
    else
        let
            val _ = print_newline()
            val space = space + 2
        in
            drawTriangle(level-1,space,level-1,space)
end






//
(* ****** ****** *)

#ifdef
MAIN_NONE
#then
#else
implement
main0 (argc, argv) =
{
//
// HX: here is some testing code:
//
val () = show_triangle(7)
//val () = show_triangle(5)
//
val () = println! ("Good news: Your code has passed initial testing!")
//
} (* end of [main0] *)
#endif // #ifdef

(* ****** ****** *)

(* end of [assign2_sol.dats] *)