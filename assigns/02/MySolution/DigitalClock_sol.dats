(* ****** ****** *)

#include "./../DigitalClock.dats"

(* ****** ****** *)

(*
//
// HX: 20 points
// Please replace
// the following dummy implementations:
//
*)

(* ****** ****** *)

implement
draw_0(X, Y, W, H) = {
    val () = draw_hline(X,Y,W-2)
    val () = draw_vline(X,Y,W)
    val () = draw_vline(X+6,Y,W)
    val () = draw_hline(X,Y+8,W-2)
    
}

implement
draw_1(X, Y, W, H) = {
    val () = draw_vline(X+6,Y,W)
}

implement
draw_2(X, Y, W, H) = {
    val () = draw_hline(X,Y,W-2)
    val () = draw_vline(X+6,Y-5,W)
    val () = draw_hline(X,Y+4,W-2)
    val () = draw_vline(X,Y+5,W)
    val () = draw_hline(X,Y+8,W-2)
}

implement
draw_3(X, Y, W, H) = {
    val () = draw_hline(X,Y,W-2)
    val () = draw_vline(X+6,Y-5,W)
    val () = draw_hline(X,Y+4,W-2)
    val () = draw_vline(X+6,Y+5,W)
    val () = draw_hline(X,Y+8,W-2)
}

implement
draw_4(X, Y, W, H) = {
    val () = draw_vline(X+6,Y,W)
    val () = draw_vline(X,Y-5,W)
    val () = draw_hline(X,Y+4,W-2)
}


implement
draw_5(X, Y, W, H) = {
    val () = draw_hline(X,Y,W-2)
    val () = draw_vline(X,Y-5,W)
    val () = draw_hline(X,Y+4,W-2)
    val () = draw_vline(X+6,Y+5,W)
    val () = draw_hline(X,Y+8,W-2)
}

implement
draw_6(X, Y, W, H) = {
    val () = draw_hline(X,Y,W-2)
    val () = draw_vline(X,Y,W)
    val () = draw_vline(X+6,Y+5,W)
    val () = draw_hline(X,Y+4,W-2)
    val () = draw_hline(X,Y+8,W-2) 
}

implement
draw_7(X, Y, W, H) = {
    val () = draw_hline(X,Y,W-2)
    val () = draw_vline(X+6,Y,W)
}

implement
draw_8(X, Y, W, H) = {
    val () = draw_hline(X,Y,W-2)
    val () = draw_vline(X,Y,W)
    val () = draw_hline(X,Y+4,W-2)
    val () = draw_vline(X+6,Y,W)
    val () = draw_hline(X,Y+8,W-2)  
}

implement
draw_9(X, Y, W, H) = {
    val () = draw_hline(X,Y,W-2)
    val () = draw_vline(X+6,Y,W)
    val () = draw_vline(X,Y-5,W)
    val () = draw_hline(X,Y+4,W-2)
    val () = draw_hline(X,Y+8,W-2) 
}

(* ****** ****** *)

(* end of [DigitalClock_sol.dats] *)
