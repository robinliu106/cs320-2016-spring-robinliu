//
// Title:
// Concepts of Programming Languages
// Number: CAS CS 320
// Semester: Spring 2016
// Class Time: TR 12:30-2:00
// Instructor: Hongwei Xi (hwxiATbuDOTedu)
// Teaching Fellow: Hanwen Wu (hwwuATbuDOTedu)
//
(*
//
// Assignment #8
// Due Wednesday, the 30th of March, 2016 at 11:59pm
//
*)
(* ****** ****** *)

(*
#
# Assign8: 20 points
#
Please use the code in game-of-24.dats to
give a running implementation of Game-of-24
#
There is little code for you to write.
#
The assignment primarily expects that you read
the code in lecture-03-24 and then use it to
to give a running implementation of Game-of-24.
#
*)

(* ****** ****** *)
//
#include
"./../game-of-24.dats"
//
(* ****** ****** *)

extern
fun
process_node(node): void

(* ****** ****** *)

implement
process_node
  (x0) = let
//
val cs = x0
//
in
//
case+ cs of
| list0_sing(c0) => let
    val v0 = card_eval(c0)
  in
    if abs(v0 - 24) < EPSILON then println! ("Solution: ", c0)
  end // end of [list0_sing]
| _(* non-sing *) => ()
//
end // end of [process_node]

(* ****** ****** *)
//
extern
fun
dfirst_search(node): void
//
(* ****** ****** *)

staload
"./../../../mylib/mystack.dats"

implement
dfirst_search(nx) = let
//
val
stk = mystack_make_nil()
//
val () = mystack_push(stk, nx)
//
fun
loop
(
// argless
) : void = let
//
val
opt = mystack_pop_opt(stk)
//
in
  case+ opt of
  | None() => ()
  | Some(nx) => let
      val () = process_node(nx)
      val nxs = node_get_children(nx)
      val () = mystack_push_list(stk, nxs)
    in
      loop()
    end // end of [Some]
end // end of [loop]
//
in
  loop()
end // end of [dfirst_search]

(* ****** ****** *)
//
// HX: 20 points
//
(*
implement
play_game_of_24(n1, n2, n3, n4) = ...
*)
//
(* ****** ****** *)

implement
play_game_of_24
  (n1, n2, n3, n4) = let
//
val c1 = CARDval(g0i2f(n1))
val c2 = CARDval(g0i2f(n2))
val c3 = CARDval(g0i2f(n3))
val c4 = CARDval(g0i2f(n4))
//
val node0 = g0ofg1($list{card}(c1, c2, c3, c4))
//
in
  dfirst_search(node0)
end // end of [play_game_of_24]

(* ****** ****** *)

staload "libc/SATS/stdlib.sats"

(* ****** ****** *)

implement
main0(argc, argv) = let
//
val n1 = (if argc >= 2 then atoi(argv[1]) else 0): int
val n2 = (if argc >= 3 then atoi(argv[2]) else 0): int
val n3 = (if argc >= 4 then atoi(argv[3]) else 0): int
val n4 = (if argc >= 5 then atoi(argv[4]) else 0): int
//
in
  play_game_of_24(n1, n2, n3, n4)
end // end of [main0]

(* ****** ****** *)

(* end of [assign8_sol.dats] *)
