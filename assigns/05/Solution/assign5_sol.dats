(* ****** ****** *)

#include "./../assign5.dats"

(* ****** ****** *)

(*
**
** HX-2016-02-18: 20 points
** please implement mylist_fold_split here
*)

implement
{a,b}
mylist_fold_split
(
  xs, fopr, sink0, fsink1
) = let
//
fun
aux
(
  xs: list0(a), n: int
) : b = let
//
(*
val
out = stdout_ref
val () =
fprintln!(out, "aux: n = ", n)
*)
//
in
//
if
(n >= 2)
then let
  val n2 = n / 2
  val xs1 = mylist_take(xs, n2)
  val xs2 = mylist_drop(xs, n2)
in
  fopr(aux(xs1, n2), aux(xs2, n-n2))
end // end of [then]
else (
  case+ xs of
  | list0_nil() => sink0
  | list0_cons(x, _) => fsink1(x)
) (* end of [else] *)
//
end (* end of [aux] *)
//
in
  aux(xs, length(xs))
end // end of [mylist_fold_split]

(* ****** ****** *)

(* ****** ****** *)

(*
// 5 points
please implement
mylist_reverse based on mylist_fold_split
//
*)

implement
{a}(*tmp*)
mylist_reverse
  (xs) = (
//
mylist_fold_split<a,list0(a)>
( xs
, lam(res1, res2) =>
  mylist_append<a>(res2, res1)
, list0_nil(), lam(x) => list0_sing(x)
) (* mylist_fold_split *)
//
) (* end of [mylist_reverse] *)

(* ****** ****** *)

(*
//
// 5 points
// please implement
// mylist_filter based on mylist_fold_split
//
*)

implement
{a}(*tmp*)
mylist_filter
  (xs, p) = (
//
mylist_fold_split<a,list0(a)>
( xs
, lam(res1, res2) =>
  mylist_append<a>(res1, res2)
, list0_nil(), lam(x) => if p(x) then list0_sing(x) else list0_nil()
) (* mylist_fold_split *)
//
) (* end of [mylist_filter] *)

(* ****** ****** *)

#ifdef
MAIN_NONE
#then
#else
//
implement
main0(argc, argv) =
{
//
val xs =
mylist_make_intrange(0, 10)
//
val
out = stdout_ref
//
val () = fprintln! (out, "xs = ", xs)
//
val ys = mylist_reverse<int>(xs)
val () = fprintln! (out, "ys = ", ys)
//
val zs = mylist_filter<int>(xs, lam(x) => x % 2 = 0)
val () = fprintln! (out, "zs = ", zs)
//
val () = println! ("Good news: Your code has passed initial testing!")
//
} (* end of [main0] *)
//
#endif // #ifdef

(* ****** ****** *)

(* end of [assign5_sol.dats] *)
