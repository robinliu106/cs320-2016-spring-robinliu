//
// Title:
// Concepts of Programming Languages
// Number: CAS CS 320
// Semester: Spring 2016
// Class Time: TR 12:30-2:00
// Instructor: Hongwei Xi (hwxiATbuDOTedu)
// Teaching Fellow: Hanwen Wu (hwwuATbuDOTedu)
//
(*
//
// Assignment #5
// Due Wednesday, February 24, 2016 at 11:59pm
//
*)
(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
#include
"share/HATS/atspre_staload_libats_ML.hats"
//
(* ****** ****** *)

staload "./../../mylib/mylist.dats"

(* ****** ****** *)
//
// HX: 20 points
// Please implement
// [mylist_fold_split]
//
extern
fun
{a,b:t@ype}
mylist_fold_split
( xs: list0(a) , fopr: (b, b) -<cloref1> b, sink0: b, fsink1: (a) -<cloref1> b) : b // end of [mylist_fold_split]
//
(* ****** ****** *)
//
// HX:
// Here is an example of using mylist_fold_split
//
(*
implement
{a}(*tmp*)
mylist_length(xs) = mylist_fold_split<a,int>
  (xs, lam(x, y) => x+y, 0, lam _ => 1)
*)
//
(* ****** ****** *)
//
// HX: 5 points
// Please implement
// [mylist_reverse] based on [mylist_fold_split]
//
(* ****** ****** *)
(*
implement
{a}(*tmp*)
mylist_reverse(xs) = ...
*)
(* ****** ****** *)
//
// HX: 5 points
// Please implement
// [mylist_filter] based on [mylist_fold_split]
//
(* ****** ****** *)
(*
implement
{a}(*tmp*)
mylist_filter(xs, p) = ...
*)
(* ****** ****** *)

(* end of [assign5.dats] *)
