(* ****** ****** *)

#include "./../assign5.dats"

(* ****** ****** *)

(*
** HX-2016-02-18: 20 points
** please implement mylist_fold_split here
*)
//discussed with Biken, Peter, Carlos
implement    
{a,b} 
mylist_fold_split(xs,fopr,sink0,fsink1) = let 
fun 
aux 
(xs: list0(a), y:(b), z:(b)): (b,b) =
case+ xs of
    | list0_nil() => (y,z) //if list is null, return lists
    | list0_cons(x,xs) => 
      case+ xs of
      | list0_nil() => (fopr(fsink1(x),y),z) //single element, apply sink and fopr
      | list0_cons(n,nn) => let
            val y  = fopr(fsink1(x),y) 
            val z = fopr(fsink1(n),z)
           in 
              aux(nn,y,z)
           end
val (xs,ys) = aux(xs,sink0,sink0)
in
   fopr(xs,ys)
end


(*
implement 
{a,b}
mylist_fold_split( xs, fopr, sink0, fsink1) = let
val (left,right) = mylist_split(xs)
in
case+ xs of
    | list0_nil => sink0 //
    | list0_cons(x,nil0) => fsink1(x)
    |list0_cons(x,xs) => fopr(mylist_fold_split(left,fopr,sink0,fsink1), mylist_fold_split(right,fopr,sink0,fsink1))
end        
*)


(* ****** ****** *)

(*
// 5 points
please implement
mylist_reverse based on mylist_fold_split
//
*)

(* ****** ****** *)

(*
//
// 5 points
// please implement
// mylist_filter based on mylist_fold_split
//
*)

(* ****** ****** *)

#ifdef
MAIN_NONE
#then
#else
//
implement
main0(argc, argv) =
{
//
val xs =
mylist_make_intrange(0, 10)
//
val
out = stdout_ref
//
val () = fprintln! (out, "xs = ", xs)
//
val ys = mylist_reverse<int>(xs)
val () = fprintln! (out, "ys = ", ys)
//
val zs = mylist_filter<int>(xs, lam(x) => x % 2 = 0)
val () = fprintln! (out, "zs = ", zs)
//
val () = println! ("Good news: Your code has passed initial testing!")
//
} (* end of [main0] *)
//
#endif // #ifdef

(* ****** ****** *)

(* end of [assign5_sol.dats] *)
