//
// Title:
// Concepts of Programming Languages
// Number: CAS CS 320
// Semester: Spring 2016
// Class Time: TR 12:30-2:00
// Instructor: Hongwei Xi (hwxiATbuDOTedu)
// Teaching Fellow: Hanwen Wu (hwwuATbuDOTedu)
//
(*
//
// Assignment #8-2
// Due Wednesday, the 6th of April, 2016 at 11:59pm
//
*)
(* ****** ****** *)
//
#include "./../doublet.dats"
//
(* ****** ****** *)

(*
**
** HX: 30 points
**
** Please read doublet.dats and
** then try to implement doublet2
**
*)

(* ****** ****** *)

local
//
#include
"./../../../mylib/myqueue.dats"
//
typedef node2 = list0(node)
//
extern
fun{}
process2(node2): void
extern
fun{}
bfirst_search2(node2): void
//
(* ****** ****** *)
//
extern
fun
node2_mark : node2 -> void
implement
node2_mark(n2x) =
  let val-cons0(nx, _) = n2x in node_mark(nx) end
//
extern
fun
node2_marked : node2 -> bool
implement
node2_marked(n2x) =
  let val-cons0(nx, _) = n2x in node_marked(nx) end
//
extern
fun
node2_get_word : node2 -> word
implement
node2_get_word(n2x) =
  let val-cons0(nx, _) = n2x in node_get_word(nx) end
//
(* ****** ****** *)
//
extern
fun
node2_get_children : node2 -> list0(node2)
//
implement
node2_get_children
  (n2x) = let
//
val-cons0(nx, _) = n2x
val nxs = node_get_children(nx)
//
in
  list0_map(nxs, lam(nx) => cons0(nx, n2x))
end // end of [node2_get_children]
//
(* ****** ****** *)

implement
{}(*tmp*)
bfirst_search2(n2x) = let
//
val
stk = myqueue_make_nil()
//
val () = myqueue_enqueue(stk, n2x)
//
fun loop() = let
  val opt = myqueue_dequeue_opt(stk)
in
  case+ opt of
  | None() => ()
  | Some(n2x) => let
      val () = process2(n2x)
      val n2xs =
        node2_get_children(n2x)
      val n2xs =
        list0_filter
          (n2xs, lam(n2x) => ~node2_marked(n2x))
        // list0_filter
      val ((*void*)) =
        list0_foreach(n2xs, lam(n2x) => node2_mark(n2x))
      val ((*void*)) =
        myqueue_enqueue_list(stk, n2xs)
    in
      loop()
    end
end // end of [loop]
//
in
  loop()
end // end of [bfirst_search2]

in (* in-of-local *)

implement
doublet2
  (w1, w2) = let
//
exception Found of (node2)
//
exception NONWORDEXN of ()
//
implement
process2<>(n2x) = let
(*
  val () = node2_mark(n2x)
*)
in
//
if node2_get_word(n2x)=w2
  then $raise(Found(n2x)) else ()
//
end (* end of [process] *)
//
val () = dict_unmarking()
//
val opt = word_get_node(w1)
//
val n2x1 =
(
  case+ opt of
  | Some(nx) => cons0(nx, nil0())
  | None((*void*)) => $raise NONWORDEXN()
) : node2 // end-of-val
in
//
try
let
val () =
  bfirst_search2(n2x1) in None()
end
with ~Found(n2x) => Some(n2x)
//
end // end of [doublet2]

end // end of [local]

(* ****** ****** *)

staload "libc/SATS/stdlib.sats"

(* ****** ****** *)

implement
main0(argc, argv) = let
//
val w1 =
(
  if argc >= 2 then argv[1] else ""
) : string // end-of-val
val w2 =
(
  if argc >= 3 then argv[2] else ""
) : string // end-of-val
//
val opt = doublet2(w1, w2)
//
in
//
case+ opt of
| None() =>
  println!(w1, '/', w2, " are not a doublet!")
| Some(nxs) =>
  println!(w1, '/', w2, " are a doublet: ", nxs)
//
end // end of [main0]

(* ****** ****** *)

(* end of [assign8-2_sol.dats] *)
