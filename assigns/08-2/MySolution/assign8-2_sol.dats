//
// Title:
// Concepts of Programming Languages
// Number: CAS CS 320
// Semester: Spring 2016
// Class Time: TR 12:30-2:00
// Instructor: Hongwei Xi (hwxiATbuDOTedu)
// Teaching Fellow: Hanwen Wu (hwwuATbuDOTedu)
//
(*
//
// Assignment #8-2
// Due Wednesday, the 6th of April, 2016 at 11:59pm
//
*)
(* ****** ****** *)
#include "./../doublet.dats"
staload "libc/SATS/stdlib.sats"
(* ****** ****** *)

(*
**
** HX: 30 points
**
** Please read doublet.dats and
** then try to implement doublet2
**
*)
//discussed with Biken and Peter

typedef node2 = list0(node)

extern
fun
node2_mark(node2): void
extern
fun
node2_marked(node2): bool

implement
node2_mark(n2x) = node_mark(n2x.head())
implement
node2_marked(n2x) = node_marked(n2x.head())

extern
fun
node2_get_word(node2): word

implement
node2_get_word(n2x) = node_get_word(n2x.head())

extern
fun
node2_get_children(node2): list0(node2)

implement
node2_get_children(n2x) = let
  val nxs = node_get_children(n2x.head())
in
  list0_map(nxs, lam(nx) => cons0(nx, n2x))
end

local

#include
"./../../../mylib/myqueue.dats"

extern
fun{}
process2(node2): void

extern
fun{}
bfirst_search2(node2): void

implement
{}(*tmp*)
bfirst_search2(element) = let
val
stack = myqueue_make_nil()
val () = myqueue_enqueue(stack, element)

fun loop() = let val opt = myqueue_dequeue_opt(stack)
in
  case+ opt of
  | None() => ()
  | Some(element) => let
      val () = process2(element)
      val next =
        node2_get_children(element)
      val next = list0_filter(next, lam(element) => ~node2_marked(element))
      val ((*void*)) = myqueue_enqueue_list(stack, next)
    in 
      loop()
    end
end

in
  loop()
end 

in (* in-of-local *)

implement
doublet2(word1, word2) = let
exception Found of (node2)
exception NONWORDEXN of ()
implement
process2<>(a) = let
  val () = node2_mark(a)
in
if node2_get_word(a) = word2
  then $raise(Found(a)) else ()
//
end (* end of [process] *)
val () = dict_unmarking()
val opt = word_get_node(word1)
val b =
(
  case+ opt of
  | Some(nx) => cons0(nx, nil0())
  | None((*void*)) => $raise NONWORDEXN()
) : node2 // end-of-val
in
try
let
val () =
  bfirst_search2(b) in None()
end
with ~Found(n2x) => Some(n2x)
end
end 


(* ****** ****** *)

implement
main0(argc, argv) = let
//
val w1 =
(
  if argc >= 2 then argv[1] else ""
) : string // end-of-val
val w2 =
(
  if argc >= 3 then argv[2] else ""
) : string // end-of-val
//
val opt = doublet2(w1, w2)
//
in
//
case+ opt of
| None() =>
  println!(w1, '/', w2, " are not a doublet!")
| Some(nxs) =>
  println!(w1, '/', w2, " are a doublet: ", nxs)
//

end // end of [main0]

(* ****** ****** *)

(* end of [assign8-2_sol.dats] *)
