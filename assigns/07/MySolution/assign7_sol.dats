(* ****** ****** *)

#include "./../assign7.dats"
//discussed with Peter, Carlos
extern
fun
{a,b:t@ype}
myarray_foldleft
(
  A: myarray(a), init: b, fopr: (b, a) -<cloref1> b
) : b // end-of-myarray_foldleft
//
extern
fun
{a,b:t@ype}
myarray_foldright
(
  A: myarray(a), fopr: (a, b) -<cloref1> b, sink: b
) : b // end-of-myarray_foldright


implement
{a,b}
myarray_foldleft(A, init, fopr) = let
val x = myarray_size<a>(A)
    
fun 
aux(i: int, init: b): b = 
    if i < x then fopr(aux(i+1,init),A[i])
    else init
in
    aux(0, init)
end


implement
{a,b}
myarray_foldright(A,fopr,sink) = let
val x = myarray_size<a>(A)

fun
aux(i: int, sink: b): b = 
    if i < x then fopr(A[i], aux(i+1,sink))
    else sink
in 
    aux(0,sink)
end

(* ****** ****** *)

#ifdef
MAIN_NONE
#then
#else
//
implement
main0(argc, argv) =
{
//
val A = array0_tabulate<int>(i2sz(100), lam(i) => sz2i(i))
//
val A_sum_l = myarray_foldleft<int,int>(A, 0, lam(init, x) => init + x)
//
val A_sum_r = myarray_foldright<int,int>(A, lam(x, sink) => x + sink, 0)
//
val () = assertloc (A_sum_l = A_sum_r)
//
val () = println! ("Good news: Your code has passed initial testing!")
//
} (* end of [main0] *)
//
#endif // #ifdef

(* ****** ****** *)

(* end of [assign7_sol.dats] *)