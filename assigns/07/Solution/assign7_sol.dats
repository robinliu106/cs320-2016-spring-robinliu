(* ****** ****** *)

#include "./../assign7.dats"

(* ****** ****** *)

implement
{a,b}
myarray_foldleft
  (A, init, fopr) = let
//
fun
aux
(
  i: int, n: int, res: b
) : b =
(
if i < n
  then aux(i+1, n, fopr(res, A[i])) else res
)
//
in
  aux(0, myarray_size<a>(A), init)
end // end of [aux]

(* ****** ****** *)

implement
{a,b}
myarray_foldright
  (A, fopr, sink) = let
//
fun
aux
(i: int, res: b): b =
(
if i > 0
  then aux(i-1, fopr(A[i-1], res)) else res
)
//
in
  aux(myarray_size<a>(A), sink)
end // end of [aux]

(* ****** ****** *)

#ifdef
MAIN_NONE
#then
#else
//
implement
main0(argc, argv) =
{
//
val A =
array0_tabulate<int>(i2sz(100), lam(i) => sz2i(i))
//
val A_sum_l =
myarray_foldleft<int,int>(A, 0, lam(init, x) => init + x)
//
val A_sum_r =
myarray_foldright<int,int>(A, lam(x, sink) => x + sink, 0)
//
val () = assertloc (A_sum_l = A_sum_r)
//
val () = println! ("Good news: Your code has passed initial testing!")
//
} (* end of [main0] *)
//
#endif // #ifdef

(* ****** ****** *)

(* end of [assign7_sol.dats] *)
