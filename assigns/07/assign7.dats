//
// Title:
// Concepts of Programming Languages
// Number: CAS CS 320
// Semester: Spring 2016
// Class Time: TR 12:30-2:00
// Instructor: Hongwei Xi (hwxiATbuDOTedu)
// Teaching Fellow: Hanwen Wu (hwwuATbuDOTedu)
//
(*
//
// Assignment #7
// Due Wednesday, the 23rd of March, 2016 at 11:59pm
//
*)
(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
#include
"share/HATS/atspre_staload_libats_ML.hats"
//
(* ****** ****** *)
//
staload "./../../mylib/mylist.dats"
staload "./../../mylib/myarray.dats"
//
(* ****** ****** *)

(*
//
// HX: 10 points
//
Please implement [myarray_foldleft]
//
*)

(* ****** ****** *)

(*
//
// HX: 10 points
//
Please implement [myarray_foldright]
//
*)

(* ****** ****** *)

(* end of [assign7.dats] *)
