//
// Title:
// Concepts of Programming Languages
// Number: CAS CS 320
// Semester: Spring 2016
// Class Time: TR 12:30-2:00
// Instructor: Hongwei Xi (hwxiATbuDOTedu)
// Teaching Fellow: Hanwen Wu (hwwuATbuDOTedu)
//
(*
//
// Assignment #9
// Due Wednesday, the 13th of April, 2016 at 11:59pm
//
*)
(* ****** ****** *)


(*
#
# Assign9:
#
*)

(* ****** ****** *)
//
#include
"./../assign9.dats"
#include
"share/atspre_define.hats"
#include
"share/atspre_staload.hats"
#include
"share/HATS/atspre_staload_libats_ML.hats"
#define  :: stream_cons
//
extern
fun
from : (int) -> stream(int)
implement
from(n) = $delay(stream_cons(n, from(n+1)))

(* ****** ****** *)

fun
alterneg
(
  xs0: stream(int)
) : stream(int) = let
//
fun
aux
(
  sgn: int
, xs0: stream(int)
) : stream(int) =
$delay(
  case+ !xs0 of
  | stream_nil() => stream_nil()
  | stream_cons(x, xs) => stream_cons(sgn*x, aux(~sgn, xs))
) (* $delay *)
//
in
  aux(1, xs0)
end // end of [alterneg]

fun
partialsum(xs0: stream(double)) : stream(double) = let
//
fun
aux
(sum: double, xs0: stream(double)) : stream(double) =
$delay(
  case+ !xs0 of
  | stream_nil() => stream_nil()
  | stream_cons(x, xs) => stream_cons(sum, aux(sum+x, xs))
) (* $delay *)
//
in
  aux(0.0, xs0)
end // end of [partialsum]

fun stream_ln2(): stream(double) = let
    val xs0 = from(1) // 1, 2, 3, 4 ...
    val xs1 = alterneg(xs0) // 1, -2, 3, -4 ...
    val xs2 = stream_map_cloref<int><double>(xs1, lam(x) => $effmask_all(1.0/x)) // 1/1, -1/2, 1/3, -1/4 ...
in
    partialsum(xs2)
end // end of [stream_ln2]

/////////////////////////////


/////////////////////////////

fun euler_trans_con(xs0:stream(double)):stream_con(double) = let 
  val- x0 :: xs1 = !xs0
  val- x1 :: xs2 = !xs1
  val- x2 :: xs3 = !xs2
  val x01 = x0 - x1 and x21 = x2 - x1
in
   (x2-x21 * x21 / (x21 + x01)) :: EulerTrans(xs1)
end

implement EulerTrans(xs0) = $delay (euler_trans_con(xs0))

//////////////////////////////

implement
main0 () =
{
val () = println!("Hello from [assign9_sol]!") 
//
val ln2_1M = stream_nth_exn (stream_ln2(), 1000000)
val () = println! ("ln2_1M = ", ln2_1M)
//
//val xys = intpair_enumerate()
//val xys_10 = stream_take_exn(xys, 10)
//val xys_10 = list0_of_list_vt(xys_10)
//
//val () = println! ("xys_10 = ", xys_10)
//
val ln2 = stream_ln2()
val ln2 = EulerTrans(ln2)
val ln2 = EulerTrans(ln2)
val ln2 = EulerTrans(ln2)
val ln2 = EulerTrans(ln2)
val () = println! ("ln2_4_0 = ", stream_nth_exn (ln2, 0))

} (* end of [main0] *)

(* ****** ****** *)

(* end of [assign9_sol.dats] *)
