/*
**
** The JavaScript code is generated by atscc2js
** The starting compilation time is: 2016-4-30: 22h: 2m
**
*/

/* ATSextcode_beg() */
//
var
theKeyDowns =
  $(document).asEventStream("keydown")
//
/*
var
theKeyDowns =
theKeyDowns.doAction(".preventDefault")
*/
//
/* ATSextcode_end() */

var Wormlike__statmp13

var Wormlike__statmp14

var Wormlike__statmp22

var Wormlike__statmp27

function
Wormlike__patsfun_7__closurerize(env0, env1)
{
  return [function(cenv, arg0, arg1) { return Wormlike__patsfun_7(cenv[1], cenv[2], arg0, arg1); }, env0, env1];
}


function
Wormlike__patsfun_10__closurerize(env0)
{
  return [function(cenv, arg0, arg1) { return Wormlike__patsfun_10(cenv[1], arg0, arg1); }, env0];
}


function
Wormlike__patsfun_13__closurerize(env0)
{
  return [function(cenv, arg0) { return Wormlike__patsfun_13(cenv[1], arg0); }, env0];
}


function
Wormlike__patsfun_14__closurerize(env0, env1, env2)
{
  return [function(cenv, arg0) { return Wormlike__patsfun_14(cenv[1], cenv[2], cenv[3], arg0); }, env0, env1, env2];
}


function
Wormlike__patsfun_15__closurerize(env0, env1)
{
  return [function(cenv, arg0) { return Wormlike__patsfun_15(cenv[1], cenv[2], arg0); }, env0, env1];
}


function
Wormlike__patsfun_16__closurerize(env0)
{
  return [function(cenv, arg0) { return Wormlike__patsfun_16(cenv[1], arg0); }, env0];
}


function
_057_home_057_ubuntu_057_workspace_057_assigns_057_10_057_MySolution_057_Wormlike_057_Wormlike_056_sats__succ_row(arg0)
{
//
// knd = 0
  var tmpret0
  var tmp1
  var tmp2
  var tmplab, tmplab_js
//
  // __patsflab_succ_row
  tmp1 = ats2jspre_add_int0_int0(arg0, 1);
  tmp2 = ats2jspre_lt_int0_int0(tmp1, 32);
  if(tmp2) {
    tmpret0 = tmp1;
  } else {
    tmpret0 = 0;
  } // endif
  return tmpret0;
} // end-of-function


function
_057_home_057_ubuntu_057_workspace_057_assigns_057_10_057_MySolution_057_Wormlike_057_Wormlike_056_sats__pred_row(arg0)
{
//
// knd = 0
  var tmpret3
  var tmp4
  var tmplab, tmplab_js
//
  // __patsflab_pred_row
  tmp4 = ats2jspre_gt_int0_int0(arg0, 0);
  if(tmp4) {
    tmpret3 = ats2jspre_sub_int0_int0(arg0, 1);
  } else {
    tmpret3 = ats2jspre_sub_int1_int1(32, 1);
  } // endif
  return tmpret3;
} // end-of-function


function
_057_home_057_ubuntu_057_workspace_057_assigns_057_10_057_MySolution_057_Wormlike_057_Wormlike_056_sats__succ_col(arg0)
{
//
// knd = 0
  var tmpret5
  var tmp6
  var tmp7
  var tmplab, tmplab_js
//
  // __patsflab_succ_col
  tmp6 = ats2jspre_add_int0_int0(arg0, 1);
  tmp7 = ats2jspre_lt_int0_int0(tmp6, 64);
  if(tmp7) {
    tmpret5 = tmp6;
  } else {
    tmpret5 = 0;
  } // endif
  return tmpret5;
} // end-of-function


function
_057_home_057_ubuntu_057_workspace_057_assigns_057_10_057_MySolution_057_Wormlike_057_Wormlike_056_sats__pred_col(arg0)
{
//
// knd = 0
  var tmpret8
  var tmp9
  var tmplab, tmplab_js
//
  // __patsflab_pred_col
  tmp9 = ats2jspre_gt_int0_int0(arg0, 0);
  if(tmp9) {
    tmpret8 = ats2jspre_sub_int0_int0(arg0, 1);
  } else {
    tmpret8 = ats2jspre_sub_int1_int1(64, 1);
  } // endif
  return tmpret8;
} // end-of-function


function
_057_home_057_ubuntu_057_workspace_057_assigns_057_10_057_MySolution_057_Wormlike_057_Wormlike_main_056_dats__xnode2string(arg0)
{
//
// knd = 0
  var tmpret10
  var tmp11
  var tmp12
  var tmplab, tmplab_js
//
  // __patsflab_xnode2string
  // ATScaseofseq_beg
  tmplab_js = 1;
  while(true) {
    tmplab = tmplab_js; tmplab_js = 0;
    switch(tmplab) {
      // ATSbranchseq_beg
      case 1: // __atstmplab0
      if(ATSCKptriscons(arg0)) { tmplab_js = 4; break; }
      case 2: // __atstmplab1
      tmpret10 = " ";
      break;
      // ATSbranchseq_end
      // ATSbranchseq_beg
      case 3: // __atstmplab2
      case 4: // __atstmplab3
      tmp11 = arg0[0];
      // ATScaseofseq_beg
      tmplab_js = 1;
      while(true) {
        tmplab = tmplab_js; tmplab_js = 0;
        switch(tmplab) {
          // ATSbranchseq_beg
          case 1: // __atstmplab4
          if(!ATSCKpat_int(tmp11, 0)) { tmplab_js = 3; break; }
          case 2: // __atstmplab5
          tmpret10 = "O";
          break;
          // ATSbranchseq_end
          // ATSbranchseq_beg
          case 3: // __atstmplab6
          if(!ATSCKpat_int(tmp11, 1)) { tmplab_js = 5; break; }
          case 4: // __atstmplab7
          tmpret10 = "$";
          break;
          // ATSbranchseq_end
          // ATSbranchseq_beg
          case 5: // __atstmplab8
          tmp12 = ats2jspre_lt_int0_int0(tmp11, 0);
          if(!ATSCKpat_bool(tmp12, true)) { tmplab_js = 6; break; }
          tmpret10 = "X";
          break;
          // ATSbranchseq_end
          // ATSbranchseq_beg
          case 6: // __atstmplab9
          tmpret10 = ats2jspre_String(tmp11);
          break;
          // ATSbranchseq_end
        } // end-of-switch
        if (tmplab_js === 0) break;
      } // endwhile
      // ATScaseofseq_end
      break;
      // ATSbranchseq_end
    } // end-of-switch
    if (tmplab_js === 0) break;
  } // endwhile
  // ATScaseofseq_end
  return tmpret10;
} // end-of-function


function
theGamebd_get()
{
//
// knd = 0
  var tmpret15
  var tmplab, tmplab_js
//
  // __patsflab_theGamebd_get
  tmpret15 = Wormlike__statmp13;
  return tmpret15;
} // end-of-function


function
theGamebd_display()
{
//
// knd = 0
  var tmp17
  var tmp18
  var tmplab, tmplab_js
//
  // __patsflab_theGamebd_display
  tmp17 = theGamebd_get();
  tmp18 = thePrintbd_get();
  ats2jspre_mtrxszref_foreach_cloref(tmp17, Wormlike__patsfun_7__closurerize(tmp17, tmp18));
  return/*_void*/;
} // end-of-function


function
Wormlike__patsfun_7(env0, env1, arg0, arg1)
{
//
// knd = 0
  var tmp20
  var tmp21
  var tmplab, tmplab_js
//
  // __patsflab_Wormlike__patsfun_7
  tmp21 = ats2jspre_mtrxszref_get_at(env0, arg0, arg1);
  tmp20 = _057_home_057_ubuntu_057_workspace_057_assigns_057_10_057_MySolution_057_Wormlike_057_Wormlike_main_056_dats__xnode2string(tmp21);
  ats2jspre_mtrxszref_set_at(env1, arg0, arg1, tmp20);
  return/*_void*/;
} // end-of-function


function
theVisitbd_get()
{
//
// knd = 0
  var tmpret23
  var tmplab, tmplab_js
//
  // __patsflab_theVisitbd_get
  tmpret23 = Wormlike__statmp22;
  return tmpret23;
} // end-of-function


function
theVisitbd_reset()
{
//
// knd = 0
  var tmp25
  var tmplab, tmplab_js
//
  // __patsflab_theVisitbd_reset
  tmp25 = theVisitbd_get();
  ats2jspre_mtrxszref_foreach_cloref(tmp25, Wormlike__patsfun_10__closurerize(tmp25));
  return/*_void*/;
} // end-of-function


function
Wormlike__patsfun_10(env0, arg0, arg1)
{
//
// knd = 0
  var tmplab, tmplab_js
//
  // __patsflab_Wormlike__patsfun_10
  ats2jspre_mtrxszref_set_at(env0, arg0, arg1, 0);
  return/*_void*/;
} // end-of-function


function
thePrintbd_get()
{
//
// knd = 0
  var tmpret28
  var tmplab, tmplab_js
//
  // __patsflab_thePrintbd_get
  tmpret28 = Wormlike__statmp27;
  return tmpret28;
} // end-of-function


function
Printbd_display(arg0)
{
//
// knd = 0
  var tmp30
  var tmp31
  var tmp34
  var tmp39
  var tmp49
  var tmplab, tmplab_js
//
  // __patsflab_Printbd_display
  tmp30 = ats2jspre_mtrxszref_get_nrow(arg0);
  tmp31 = ats2jspre_mtrxszref_get_ncol(arg0);
  ats2jspre_print_string("+");
  tmp34 = ats2jspre_int_foreach_method(tmp31);
  tmp34[0](tmp34, Wormlike__patsfun_13__closurerize("+"));
  ats2jspre_print_string("+");
  ats2jspre_print_newline();
  tmp39 = ats2jspre_int_foreach_method(tmp30);
  tmp39[0](tmp39, Wormlike__patsfun_14__closurerize(arg0, "+", tmp31));
  ats2jspre_print_string("+");
  tmp49 = ats2jspre_int_foreach_method(tmp31);
  tmp49[0](tmp49, Wormlike__patsfun_16__closurerize("+"));
  ats2jspre_print_string("+");
  ats2jspre_print_newline();
  return/*_void*/;
} // end-of-function


function
Wormlike__patsfun_13(env0, arg0)
{
//
// knd = 0
  var tmplab, tmplab_js
//
  // __patsflab_Wormlike__patsfun_13
  ats2jspre_print_string(env0);
  return/*_void*/;
} // end-of-function


function
Wormlike__patsfun_14(env0, env1, env2, arg0)
{
//
// knd = 0
  var tmp43
  var tmplab, tmplab_js
//
  // __patsflab_Wormlike__patsfun_14
  ats2jspre_print_string(env1);
  tmp43 = ats2jspre_int_foreach_method(env2);
  tmp43[0](tmp43, Wormlike__patsfun_15__closurerize(env0, arg0));
  ats2jspre_print_string(env1);
  ats2jspre_print_newline();
  return/*_void*/;
} // end-of-function


function
Wormlike__patsfun_15(env0, env1, arg0)
{
//
// knd = 0
  var tmp45
  var tmplab, tmplab_js
//
  // __patsflab_Wormlike__patsfun_15
  tmp45 = ats2jspre_mtrxszref_get_at(env0, env1, arg0);
  ats2jspre_print_string(tmp45);
  return/*_void*/;
} // end-of-function


function
Wormlike__patsfun_16(env0, arg0)
{
//
// knd = 0
  var tmplab, tmplab_js
//
  // __patsflab_Wormlike__patsfun_16
  ats2jspre_print_string(env0);
  return/*_void*/;
} // end-of-function

// dynloadflag_minit
var _057_home_057_ubuntu_057_workspace_057_assigns_057_10_057_MySolution_057_Wormlike_057_Wormlike_main_056_dats__dynloadflag = 0;

function
_057_home_057_ubuntu_057_workspace_057_assigns_057_10_057_MySolution_057_Wormlike_057_Wormlike_main_056_dats__dynload()
{
//
// knd = 0
  var tmplab, tmplab_js
//
  // ATSdynload()
  // ATSdynloadflag_sta(_057_home_057_ubuntu_057_workspace_057_assigns_057_10_057_MySolution_057_Wormlike_057_Wormlike_main_056_dats__dynloadflag(140))
  if(ATSCKiseqz(_057_home_057_ubuntu_057_workspace_057_assigns_057_10_057_MySolution_057_Wormlike_057_Wormlike_main_056_dats__dynloadflag)) {
    _057_home_057_ubuntu_057_workspace_057_assigns_057_10_057_MySolution_057_Wormlike_057_Wormlike_main_056_dats__dynloadflag = 1 ; // flag is set
    Wormlike__statmp14 = null;
    Wormlike__statmp13 = ats2jspre_mtrxszref_make_elt(32, 64, Wormlike__statmp14);
    Wormlike__statmp22 = ats2jspre_mtrxszref_make_elt(32, 64, 0);
    Wormlike__statmp27 = ats2jspre_mtrxszref_make_elt(32, 64, "");
    theWormlike_scene2();
    theWormlike_bonus_rand(100);
    Wormlike_worm_initize();
    Wormlike_keyboard_initize();
  } // endif
  return/*_void*/;
} // end-of-function


function
Wormlike__dynload()
{
//
// knd = 0
  var tmplab, tmplab_js
//
  _057_home_057_ubuntu_057_workspace_057_assigns_057_10_057_MySolution_057_Wormlike_057_Wormlike_main_056_dats__dynload();
  return/*_void*/;
} // end-of-function


/* ATSextcode_beg() */
//
function
thePrintbd_display()
{
//
theGamebd_display();
//
ats2jspre_the_print_store_clear();
//
Printbd_display(thePrintbd_get(/*void*/));
//
document.getElementById("theWormlike_printbd").innerHTML = ats2jspre_the_print_store_join();
//
} /* end of [thePrintbd_display] */
//
/* ATSextcode_end() */

/* ATSextcode_beg() */
//
function
Wormlike_initize()
{
//
thePrintbd_display();
//
return; // from [Wormlike_initize]
//
} // end of [Wormlike_initize]
/* ATSextcode_end() */

/* ATSextcode_beg() */
//
jQuery(document).ready(function(){Wormlike__dynload(); Wormlike_initize();});
//
/* ATSextcode_end() */

/* ****** ****** */

/* end-of-compilation-unit */
