(* ****** ****** *)
//
// Title:
// Concepts of Programming Languages
// Number: CAS CS 320
// Semester: Spring 2016
// Class Time: TR 12:30-2:00
// Instructor: Hongwei Xi (hwxiATbuDOTedu)
// Teaching Fellow: Hanwen Wu (hwwuATbuDOTedu)
//
(* ****** ****** *)

#include "./Wormlike_worm.dats"

local 

staload"./myqueue.dats"

typedef wnode2 = list0(wnode)
extern fun wnode_get_children: wnode -> list0(wnode)
extern fun{} process2(wnode2): bool
extern fun{} bfirst_search2(wnode2): Option(wnode2)
extern fun wnode2_mark : wnode2 -> void
extern fun wnode2_marked: wnode2 -> bool
extern fun wnode2_get_children : wnode2 -> list0(wnode2)

(* ****** ****** *)
fun 
wnode_mark(wx: wnode): void = let
  val+WN(i, j) = wx
  val V0 = theVisitbd_get() in V0[i, j] := 1
  end
  
fun
wnode_marked(wx: wnode): bool = let
  val+WN(i, j) = wx
  val V0 = theVisitbd_get() in V0[i, j] >= 1
  end
  

implement
wnode_get_children(wx) = xs where
  {
      val WN(i0, j0) = wx
      val G0 = theGamebd_get()
      
      fun
      test(xn: xnode): bool =
        case+ xn of
        | XN0() => true | XN1(knd) => knd >= 1
      
      val xs = list0_nil()
      val i1 = pred_row(i0) and j1 = j0
      val xs = (
        if test(G0[i1, j1])
        then list0_cons(WN(i1, j1), xs) else xs
      ): list0(wnode)
      
      val i1 = i0 and j1 = succ_col(j0)
      
      val xs = 
      (
        if test(G0[i1, j1])
        then list0_cons(WN(i1, j1), xs) else xs
      ): list0(wnode)
      
  }



implement 
wnode2_mark(w2x) = let
  val-cons0(wx, _) = w2x in wnode_mark(wx) 
end
  
implement
wnode2_marked(w2x) = 
  let val-cons0(wx, _) = w2x 
in 
  wnode_marked(wx) 
end
  
implement
wnode2_get_children(x) = let
  val-cons0(wx, _) = x
  val xs = wnode_get_children(wx)
in 
  list0_map_cloref(xs, lam(wx) => cons0(wx, x))
end
  
implement
{}
process2(xs) = let
  val-list0_cons(x,_) = xs
  val+WN(i, j) = x
  val G0 = theGamebd_get()
in
  case+ G0[i, j] of XN0() => false | XN1(knd) => knd >=1
end
  
implement
{}
bfirst_search2(x) = let
  val stack = myqueue_make_nil()
  val () = myqueue_enqueue(stack, x)
   
  fun aux(): Option(wnode2) = let
    val opt = myqueue_dequeue_opt(stack)
  in
    case+ opt of
    | None() => None()
    | Some(x) => let
      val ans = process2(x)
    in 
      if ans then Some(x)
    else let
      val xs = wnode2_get_children(x)
      val xs = list0_filter_cloref(xs, lam(x) => ~wnode2_marked(x))
      val () = list0_foreach_cloref(xs, lam(x) => wnode2_mark(x))
      val () = list0_foreach_cloref(xs, lam(x) => myqueue_enqueue(stack,x))
    in
      aux()
    end
  end
  end
in
  aux()
end
in
    
implement
{}
theWorm_move_search() = let
  val () = theVisitbd_reset()
  val xs = theWorm_get()
  val opt = (
    case+ xs of
    | list0_nil() => Some(WN(0, 0))
    | list0_cons(x, _) => let
        val opt = bfirst_search2(cons0(x, nil0()))
      in
        case+ opt of 
        | None() => None() where {}
        | Some(xs) => opt where
          {
              val n = list0_length(xs)
              val opt = list0_get_at_opt(xs, n-2)
          }
        end
  ): Option(wnode)
in 
  theWorm_move_with(opt)
end
end
  

(* ****** ****** *)

(* end of [Wormlist_worm2.dats] *)
