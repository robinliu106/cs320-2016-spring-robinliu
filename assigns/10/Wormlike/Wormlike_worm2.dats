(* ****** ****** *)
//
// Title:
// Concepts of Programming Languages
// Number: CAS CS 320
// Semester: Spring 2016
// Class Time: TR 12:30-2:00
// Instructor: Hongwei Xi (hwxiATbuDOTedu)
// Teaching Fellow: Hanwen Wu (hwwuATbuDOTedu)
//
(* ****** ****** *)

#include "./Wormlike_worm.dats"

(* ****** ****** *)

implement
{}(*tmp*)
theWorm_move_search
  ((*void*)) = 0 where
{
//
val () =
alert("theWorm_move_search: unimplemented!")
//
} // end of [theWorm_move_search]

(* ****** ****** *)

(* end of [Wormlist_worm2.dats] *)
