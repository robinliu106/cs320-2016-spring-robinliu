(* ****** ****** *)
//
#include
"share/atspre_define.hats"
//
staload
UN = "prelude/SATS/unsafe.sats"
//
#include
"{$LIBATSCC2JS}/staloadall.hats"
//
(* ****** ****** *)
//
datatype
myqueue(a:t@ype) =
MYQUEUE of
  (ref(List0(a)), ref(List0(a)))
//
(* ****** ****** *)

extern
fun
{a:t@ype}
myqueue_make_nil(): myqueue(a)

(* ****** ****** *)
//
extern
fun
{a:t@ype}
myqueue_enqueue
  (que: myqueue(a), x0: a): void
//
extern
fun
{a:t@ype}
myqueue_enqueue_list
  (que: myqueue(a), xs: List0(a)): void
//
(* ****** ****** *)

extern
fun
{a:t@ype}
myqueue_dequeue_opt(que: myqueue(a)): Option(a)

(* ****** ****** *)

implement
{a}
myqueue_make_nil
  () = let
//
val left = ref{List0(a)}(nil)
val right = ref{List0(a)}(nil)
//
in
  MYQUEUE($UN.cast(left), $UN.cast(right))
end // end of [myqueue_make_nil]

(* ****** ****** *)

implement
{a}(*tmp*)
myqueue_enqueue
  (que, x0) = let
  val MYQUEUE(left, right) = que
in
  left[] := list_cons(x0, left[])
end // end of [myqueue_enqueue]

(* ****** ****** *)

implement
{a}(*tmp*)
myqueue_enqueue_list
  (que, xs) = let
  val MYQUEUE(left, right) = que
in
  left[] := list_reverse_append(xs, left[])
end // end of [myqueue_enqueue_list]

(* ****** ****** *)

implement
{a}(*tmp*)
myqueue_dequeue_opt
  (que) = let
//
val
MYQUEUE(left, right) = que
//
in
//
case+
right[]
of // case+
| list_nil() => let
    val ys = left[]
    val () = left[] := list_nil()
    val ys = list_reverse(ys)
  in
    case+ ys of
    | list_nil() => None()
    | list_cons
        (y, ys) => (right[] := ys; Some(y))
      // list_cons
  end // list_nil
| list_cons(y, ys) => (right[] := ys; Some(y))
//
end // end of [myqueue_dequeue_opt]

(* ****** ****** *)

(* end of [myqueue.dats] *)
