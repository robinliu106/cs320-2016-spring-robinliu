(* ****** ****** *)

#include "./../assign2-2.dats"

(* ****** ****** *)
//
fun
fact(n: int): int =
  if n > 0 then n * fact(n-1) else 0
//
(* ****** ****** *)

implement
mylist_length(xs) =
case xs of
| mylist_nil() => 0 // clause
| mylist_cons(x, xs) => 1 + mylist_length(xs)
    
implement
mylist_fromto(start, finish) =
  if start < finish then
    mylist_cons(start, mylist_fromto(start+1, finish)) 
  else 
    mylist_nil()

implement mylist_mul(xs) =
  case xs of
  |mylist_nil() => 1 
  |mylist_cons(x, xs) => x * mylist_mul(xs)
  

  

#ifdef
MAIN_NONE
#then
#else
implement
main0 (argc, argv) =
{
//
// HX: here is some testing code:
//
val N = 10
//
val xs = mylist_fromto(1,10)
//
val () =
(
  print! "xs = [";
  mylist_print(xs); println! ("]")
)
//
//val () = assertloc(mylist_length(xs) = N)
//

val _ = println!( mylist_length(xs))

val () = assertloc(fact(N) = mylist_mul(xs)*N)

//
val () = println! ("Good news: Your code has passed initial testing!")
//
} (* end of [main0] *)
#endif // #ifdef

(* ****** ****** *)

(* end of [assign2-2_sol.dats] *)
