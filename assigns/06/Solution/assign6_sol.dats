(* ****** ****** *)

#include "./../assign6.dats"

(* ****** ****** *)

(*
** HX-2016-02-25: 10 points
** please implement mylist2_foldright
*)

(* ****** ****** *)

implement
{a,b,c}
mylist2_foldright
  (xs, ys, fopr, sink) = let
//
fun
aux(xs: list0(a), ys: list0(b)): c =
(
case+ (xs, ys) of
| (list0_nil(), _) => sink
| (_, list0_nil()) => sink
| (list0_cons(x, xs),
   list0_cons(y, ys)) => fopr(x, y, aux(xs, ys))
) (* end of [aux] *)
//
in
  aux(xs, ys)
end // end of [mylist2_foldright]

(* ****** ****** *)

(*
//
// HX-2016-02-25: 10 points
// please implement
// mylist_compare based on mylist2_foldleft+exception
//
*)

implement
{a}(*tmp*)
mylist_compare
  (xs, ys) = let
//
exception Done of (int)
//
in
//
try
//
let
  val _ =
  mylist2_foldleft<a,a,int>
  ( xs, ys
  , 0(*init*)
  , lam(_, x, y) =>
    let
      val sgn = gcompare_val_val<a>(x, y)
    in if sgn = 0 then 0 else $raise Done(sgn) end
  ) (* mylist2_foldleft *)
in
  compare(mylist_length(xs), mylist_length(ys))
end // end of [let]
//
with ~Done(sgn) => sgn
//
end // end of [mylist_compare]

(* ****** ****** *)

#ifdef
MAIN_NONE
#then
#else
//
implement
main0(argc, argv) =
{
//
val xs =
mylist_make_intrange(0, 10)
//
val
out = stdout_ref
//
val () = fprintln! (out, "xs = ", xs)
//
val ys = mylist_reverse<int>(xs)
val () = fprintln! (out, "ys = ", ys)
//
val zs = mylist_filter<int>(xs, lam(x) => x % 2 = 0)
val () = fprintln! (out, "zs = ", zs)
//
val sgn = mylist_compare<int>(xs, xs)
val () = println! ("compare(xs, xs) = ", sgn)
//
val sgn = mylist_compare<int>(xs, ys)
val () = println! ("compare(xs, ys) = ", sgn)
//
val sgn = mylist_compare<int>(xs, zs)
val () = println! ("compare(xs, zs) = ", sgn)
//
val () = println! ("Good news: Your code has passed initial testing!")
//
} (* end of [main0] *)
//
#endif // #ifdef

(* ****** ****** *)

(* end of [assign6_sol.dats] *)
