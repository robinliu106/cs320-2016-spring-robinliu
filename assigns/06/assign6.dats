//
// Title:
// Concepts of Programming Languages
// Number: CAS CS 320
// Semester: Spring 2016
// Class Time: TR 12:30-2:00
// Instructor: Hongwei Xi (hwxiATbuDOTedu)
// Teaching Fellow: Hanwen Wu (hwwuATbuDOTedu)
//
(*
//
// Assignment #6
// Due Wednesday, the 2nd of March, 2016 at 11:59pm
//
*)
(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
#include
"share/HATS/atspre_staload_libats_ML.hats"
//
(* ****** ****** *)
//
staload "./../../mylib/mylist.dats"
//
(* ****** ****** *)
//
// HX: 10 points
//
extern
fun
{a,b,c:t@ype}
mylist2_foldright
(
  xs: list0(a), ys: list0(b)
, fopr: (a, b, c) -<cloref1> c, sink: c
) : (c) // mylist2_foldright
//
(* ****** ****** *)
//
// HX: 10 points
// Please implement
// mylist_compare based based on mylist2_foldleft
//
// mylist_compare implements the lexicographic ordering on lists
//
extern
fun{a:t@ype}
mylist_compare
  (xs: list0(a), ys: list0(a)): int
//
(* ****** ****** *)

(* end of [assign6.dats] *)
