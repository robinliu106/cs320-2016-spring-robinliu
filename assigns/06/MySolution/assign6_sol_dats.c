/*
**
** The C code is generated by ATS/Postiats
** The starting compilation time is: 2016-3-3:  3h:28m
**
*/

/*
** include runtime header files
*/
#ifndef _ATS_CCOMP_HEADER_NONE_
#include "pats_ccomp_config.h"
#include "pats_ccomp_basics.h"
#include "pats_ccomp_typedefs.h"
#include "pats_ccomp_instrset.h"
#include "pats_ccomp_memalloc.h"
#ifndef _ATS_CCOMP_EXCEPTION_NONE_
#include "pats_ccomp_memalloca.h"
#include "pats_ccomp_exception.h"
#endif // end of [_ATS_CCOMP_EXCEPTION_NONE_]
#endif /* _ATS_CCOMP_HEADER_NONE_ */


/*
** include prelude cats files
*/
#ifndef _ATS_CCOMP_PRELUDE_NONE_
//
#include "prelude/CATS/basics.cats"
#include "prelude/CATS/integer.cats"
#include "prelude/CATS/pointer.cats"
#include "prelude/CATS/bool.cats"
#include "prelude/CATS/char.cats"
#include "prelude/CATS/float.cats"
#include "prelude/CATS/integer_ptr.cats"
#include "prelude/CATS/integer_fixed.cats"
#include "prelude/CATS/memory.cats"
#include "prelude/CATS/string.cats"
#include "prelude/CATS/strptr.cats"
//
#include "prelude/CATS/fprintf.cats"
//
#include "prelude/CATS/filebas.cats"
//
#include "prelude/CATS/list.cats"
#include "prelude/CATS/option.cats"
#include "prelude/CATS/array.cats"
#include "prelude/CATS/arrayptr.cats"
#include "prelude/CATS/arrayref.cats"
#include "prelude/CATS/matrix.cats"
#include "prelude/CATS/matrixptr.cats"
//
#endif /* _ATS_CCOMP_PRELUDE_NONE_ */
/*
** for user-supplied prelude
*/
#ifdef _ATS_CCOMP_PRELUDE_USER_
//
#include _ATS_CCOMP_PRELUDE_USER_
//
#endif /* _ATS_CCOMP_PRELUDE_USER_ */
/*
** for user2-supplied prelude
*/
#ifdef _ATS_CCOMP_PRELUDE_USER2_
//
#include _ATS_CCOMP_PRELUDE_USER2_
//
#endif /* _ATS_CCOMP_PRELUDE_USER2_ */

/*
staload-prologues(beg)
*/
/*
/home/ubuntu/ATS2/prelude/DATS/basics.dats: 1534(line=44, offs=1) -- 1573(line=45, offs=32)
*/
/*
/home/ubuntu/ATS2/prelude/DATS/pointer.dats: 1533(line=44, offs=1) -- 1572(line=44, offs=40)
*/
/*
/home/ubuntu/ATS2/prelude/DATS/integer.dats: 1636(line=51, offs=1) -- 1675(line=51, offs=40)
*/
/*
/home/ubuntu/ATS2/prelude/DATS/integer_ptr.dats: 1639(line=51, offs=1) -- 1678(line=51, offs=40)
*/
/*
/home/ubuntu/ATS2/prelude/DATS/integer_fixed.dats: 1641(line=51, offs=1) -- 1680(line=51, offs=40)
*/
/*
/home/ubuntu/ATS2/prelude/DATS/char.dats: 1610(line=48, offs=1) -- 1649(line=48, offs=40)
*/
/*
/home/ubuntu/ATS2/prelude/DATS/float.dats: 1636(line=50, offs=1) -- 1675(line=50, offs=40)
*/
/*
/home/ubuntu/ATS2/prelude/DATS/memory.dats: 1410(line=38, offs=1) -- 1449(line=39, offs=32)
*/
/*
/home/ubuntu/ATS2/prelude/DATS/string.dats: 1609(line=48, offs=1) -- 1648(line=48, offs=40)
*/
/*
/home/ubuntu/ATS2/prelude/DATS/strptr.dats: 1609(line=48, offs=1) -- 1648(line=48, offs=40)
*/
/*
/home/ubuntu/ATS2/prelude/DATS/strptr.dats: 1671(line=52, offs=1) -- 1718(line=52, offs=48)
*/
/*
/home/ubuntu/ATS2/prelude/DATS/integer.dats: 1636(line=51, offs=1) -- 1675(line=51, offs=40)
*/
/*
/home/ubuntu/ATS2/prelude/DATS/filebas.dats: 1613(line=48, offs=1) -- 1652(line=48, offs=40)
*/
/*
/home/ubuntu/ATS2/prelude/DATS/filebas.dats: 1675(line=52, offs=1) -- 1722(line=52, offs=48)
*/
/*
/home/ubuntu/ATS2/prelude/DATS/integer.dats: 1636(line=51, offs=1) -- 1675(line=51, offs=40)
*/
/*
/home/ubuntu/ATS2/prelude/DATS/filebas.dats: 1745(line=56, offs=1) -- 1783(line=56, offs=39)
*/
/*
/home/ubuntu/ATS2/libc/SATS/stdio.sats: 1380(line=35, offs=1) -- 1418(line=37, offs=3)
*/

#include "libc/CATS/stdio.cats"
/*
/home/ubuntu/ATS2/libc/SATS/stdio.sats: 1898(line=62, offs=1) -- 1940(line=64, offs=27)
*/
/*
/home/ubuntu/ATS2/libc/sys/SATS/types.sats: 1390(line=36, offs=1) -- 1432(line=38, offs=3)
*/

#include "libc/sys/CATS/types.cats"
/*
/home/ubuntu/ATS2/prelude/DATS/filebas.dats: 1861(line=61, offs=1) -- 1901(line=61, offs=41)
*/
/*
/home/ubuntu/ATS2/libc/sys/SATS/stat.sats: 1390(line=36, offs=1) -- 1431(line=38, offs=3)
*/

#include "libc/sys/CATS/stat.cats"
/*
/home/ubuntu/ATS2/libc/sys/SATS/stat.sats: 1712(line=52, offs=1) -- 1754(line=53, offs=35)
*/
/*
/home/ubuntu/ATS2/libc/sys/SATS/types.sats: 1390(line=36, offs=1) -- 1432(line=38, offs=3)
*/

#include "libc/sys/CATS/types.cats"
/*
/home/ubuntu/ATS2/prelude/DATS/filebas.dats: 15323(line=844, offs=1) -- 15353(line=844, offs=31)
*/
/*
/home/ubuntu/ATS2/libc/SATS/stdio.sats: 1380(line=35, offs=1) -- 1418(line=37, offs=3)
*/

#include "libc/CATS/stdio.cats"
/*
/home/ubuntu/ATS2/libc/SATS/stdio.sats: 1898(line=62, offs=1) -- 1940(line=64, offs=27)
*/
/*
/home/ubuntu/ATS2/libc/sys/SATS/types.sats: 1390(line=36, offs=1) -- 1432(line=38, offs=3)
*/

#include "libc/sys/CATS/types.cats"
/*
/home/ubuntu/ATS2/prelude/DATS/list.dats: 1527(line=44, offs=1) -- 1566(line=44, offs=40)
*/
/*
/home/ubuntu/ATS2/prelude/DATS/list.dats: 1567(line=45, offs=1) -- 1613(line=45, offs=47)
*/
/*
/home/ubuntu/ATS2/prelude/DATS/unsafe.dats: 1532(line=44, offs=1) -- 1566(line=44, offs=35)
*/
/*
/home/ubuntu/ATS2/prelude/DATS/list_vt.dats: 1536(line=44, offs=1) -- 1575(line=44, offs=40)
*/
/*
/home/ubuntu/ATS2/prelude/DATS/array.dats: 1534(line=44, offs=1) -- 1573(line=44, offs=40)
*/
/*
/home/ubuntu/ATS2/prelude/DATS/array.dats: 1574(line=45, offs=1) -- 1616(line=45, offs=43)
*/
/*
/home/ubuntu/ATS2/prelude/DATS/arrayptr.dats: 1532(line=44, offs=1) -- 1571(line=44, offs=40)
*/
/*
/home/ubuntu/ATS2/prelude/DATS/arrayref.dats: 1532(line=44, offs=1) -- 1571(line=44, offs=40)
*/
/*
/home/ubuntu/ATS2/prelude/DATS/matrix.dats: 1535(line=44, offs=1) -- 1574(line=44, offs=40)
*/
/*
/home/ubuntu/ATS2/prelude/DATS/matrixptr.dats: 1538(line=44, offs=1) -- 1577(line=44, offs=40)
*/
/*
/home/ubuntu/ATS2/prelude/DATS/matrixref.dats: 1538(line=44, offs=1) -- 1577(line=44, offs=40)
*/
/*
/home/ubuntu/ATS2/prelude/DATS/stream.dats: 1523(line=44, offs=1) -- 1562(line=44, offs=40)
*/
/*
/home/ubuntu/ATS2/prelude/DATS/stream_vt.dats: 1523(line=44, offs=1) -- 1562(line=44, offs=40)
*/
/*
/home/ubuntu/ATS2/prelude/DATS/tostring.dats: 1528(line=44, offs=1) -- 1567(line=45, offs=32)
*/
/*
/home/ubuntu/ATS2/prelude/DATS/unsafe.dats: 1532(line=44, offs=1) -- 1566(line=44, offs=35)
*/
/*
/home/ubuntu/ATS2/prelude/DATS/checkast.dats: 1531(line=44, offs=1) -- 1570(line=45, offs=32)
*/
/*
/home/ubuntu/ATS2/libats/ML/SATS/list0.sats: 1665(line=47, offs=1) -- 1700(line=47, offs=36)
*/
/*
/home/ubuntu/ATS2/libats/ML/SATS/option0.sats: 1524(line=41, offs=1) -- 1559(line=41, offs=36)
*/
/*
/home/ubuntu/ATS2/libats/ML/SATS/array0.sats: 1525(line=41, offs=1) -- 1569(line=43, offs=3)
*/

#include "libats/ML/CATS/array0.cats"
/*
/home/ubuntu/ATS2/libats/ML/SATS/array0.sats: 1608(line=47, offs=1) -- 1643(line=47, offs=36)
*/
/*
/home/ubuntu/ATS2/libats/ML/SATS/matrix0.sats: 1526(line=41, offs=1) -- 1561(line=41, offs=36)
*/
/*
/home/ubuntu/ATS2/libats/ML/SATS/string.sats: 1525(line=41, offs=1) -- 1560(line=41, offs=36)
*/
/*
/home/ubuntu/ATS2/libats/ML/SATS/strarr.sats: 1529(line=41, offs=1) -- 1573(line=43, offs=3)
*/

#include "libats/ML/CATS/strarr.cats"
/*
/home/ubuntu/ATS2/libats/ML/SATS/strarr.sats: 1612(line=47, offs=1) -- 1647(line=47, offs=36)
*/
/*
/home/ubuntu/ATS2/libats/ML/SATS/gvalue.sats: 1534(line=42, offs=1) -- 1569(line=42, offs=36)
*/
/*
/home/ubuntu/ATS2/libats/ML/SATS/filebas.sats: 1525(line=41, offs=1) -- 1560(line=41, offs=36)
*/
/*
/home/ubuntu/ATS2/libats/ML/SATS/intrange.sats: 1625(line=45, offs=1) -- 1660(line=45, offs=36)
*/
/*
/home/ubuntu/ATS2/libats/ML/SATS/stdlib.sats: 1522(line=41, offs=1) -- 1557(line=41, offs=36)
*/
/*
/home/ubuntu/ATS2/libats/ML/DATS/list0.dats: 1444(line=40, offs=1) -- 1483(line=41, offs=32)
*/
/*
/home/ubuntu/ATS2/libats/ML/DATS/list0.dats: 1506(line=45, offs=1) -- 1541(line=45, offs=36)
*/
/*
/home/ubuntu/ATS2/libats/ML/DATS/list0.dats: 1542(line=46, offs=1) -- 1577(line=46, offs=36)
*/
/*
/home/ubuntu/ATS2/libats/ML/SATS/list0.sats: 1665(line=47, offs=1) -- 1700(line=47, offs=36)
*/
/*
/home/ubuntu/ATS2/libats/ML/DATS/option0.dats: 1392(line=36, offs=1) -- 1431(line=37, offs=32)
*/
/*
/home/ubuntu/ATS2/libats/ML/DATS/option0.dats: 1454(line=41, offs=1) -- 1489(line=41, offs=36)
*/
/*
/home/ubuntu/ATS2/libats/ML/DATS/option0.dats: 1490(line=42, offs=1) -- 1527(line=42, offs=38)
*/
/*
/home/ubuntu/ATS2/libats/ML/SATS/option0.sats: 1524(line=41, offs=1) -- 1559(line=41, offs=36)
*/
/*
/home/ubuntu/ATS2/libats/ML/DATS/array0.dats: 1392(line=36, offs=1) -- 1431(line=36, offs=40)
*/
/*
/home/ubuntu/ATS2/libats/ML/DATS/array0.dats: 1454(line=40, offs=1) -- 1489(line=40, offs=36)
*/
/*
/home/ubuntu/ATS2/libats/ML/DATS/array0.dats: 1490(line=41, offs=1) -- 1525(line=41, offs=36)
*/
/*
/home/ubuntu/ATS2/libats/ML/SATS/list0.sats: 1665(line=47, offs=1) -- 1700(line=47, offs=36)
*/
/*
/home/ubuntu/ATS2/libats/ML/DATS/array0.dats: 1526(line=42, offs=1) -- 1562(line=42, offs=37)
*/
/*
/home/ubuntu/ATS2/libats/ML/SATS/array0.sats: 1525(line=41, offs=1) -- 1569(line=43, offs=3)
*/

#include "libats/ML/CATS/array0.cats"
/*
/home/ubuntu/ATS2/libats/ML/SATS/array0.sats: 1608(line=47, offs=1) -- 1643(line=47, offs=36)
*/
/*
/home/ubuntu/ATS2/libats/ML/DATS/matrix0.dats: 1396(line=36, offs=1) -- 1435(line=37, offs=32)
*/
/*
/home/ubuntu/ATS2/libats/ML/DATS/matrix0.dats: 1458(line=41, offs=1) -- 1493(line=41, offs=36)
*/
/*
/home/ubuntu/ATS2/libats/ML/DATS/matrix0.dats: 1494(line=42, offs=1) -- 1531(line=42, offs=38)
*/
/*
/home/ubuntu/ATS2/libats/ML/SATS/matrix0.sats: 1526(line=41, offs=1) -- 1561(line=41, offs=36)
*/
/*
/home/ubuntu/ATS2/libats/ML/DATS/string.dats: 1609(line=46, offs=1) -- 1648(line=46, offs=40)
*/
/*
/home/ubuntu/ATS2/libats/ML/DATS/string.dats: 2510(line=96, offs=1) -- 2545(line=96, offs=36)
*/
/*
/home/ubuntu/ATS2/libats/ML/DATS/string.dats: 2546(line=97, offs=1) -- 2581(line=97, offs=36)
*/
/*
/home/ubuntu/ATS2/libats/ML/SATS/list0.sats: 1665(line=47, offs=1) -- 1700(line=47, offs=36)
*/
/*
/home/ubuntu/ATS2/libats/ML/DATS/string.dats: 2582(line=98, offs=1) -- 2618(line=98, offs=37)
*/
/*
/home/ubuntu/ATS2/libats/ML/SATS/string.sats: 1525(line=41, offs=1) -- 1560(line=41, offs=36)
*/
/*
/home/ubuntu/ATS2/libats/ML/DATS/strarr.dats: 1613(line=46, offs=1) -- 1652(line=47, offs=32)
*/
/*
/home/ubuntu/ATS2/libats/ML/DATS/strarr.dats: 1675(line=51, offs=1) -- 1710(line=51, offs=36)
*/
/*
/home/ubuntu/ATS2/libats/ML/DATS/strarr.dats: 1711(line=52, offs=1) -- 1747(line=52, offs=37)
*/
/*
/home/ubuntu/ATS2/libats/ML/SATS/array0.sats: 1525(line=41, offs=1) -- 1569(line=43, offs=3)
*/

#include "libats/ML/CATS/array0.cats"
/*
/home/ubuntu/ATS2/libats/ML/SATS/array0.sats: 1608(line=47, offs=1) -- 1643(line=47, offs=36)
*/
/*
/home/ubuntu/ATS2/libats/ML/DATS/strarr.dats: 1748(line=53, offs=1) -- 1796(line=53, offs=49)
*/
/*
/home/ubuntu/ATS2/libats/ML/DATS/array0.dats: 1392(line=36, offs=1) -- 1431(line=36, offs=40)
*/
/*
/home/ubuntu/ATS2/libats/ML/DATS/array0.dats: 1454(line=40, offs=1) -- 1489(line=40, offs=36)
*/
/*
/home/ubuntu/ATS2/libats/ML/DATS/array0.dats: 1490(line=41, offs=1) -- 1525(line=41, offs=36)
*/
/*
/home/ubuntu/ATS2/libats/ML/SATS/list0.sats: 1665(line=47, offs=1) -- 1700(line=47, offs=36)
*/
/*
/home/ubuntu/ATS2/libats/ML/DATS/array0.dats: 1526(line=42, offs=1) -- 1562(line=42, offs=37)
*/
/*
/home/ubuntu/ATS2/libats/ML/SATS/array0.sats: 1525(line=41, offs=1) -- 1569(line=43, offs=3)
*/

#include "libats/ML/CATS/array0.cats"
/*
/home/ubuntu/ATS2/libats/ML/SATS/array0.sats: 1608(line=47, offs=1) -- 1643(line=47, offs=36)
*/
/*
/home/ubuntu/ATS2/libats/ML/DATS/strarr.dats: 1819(line=57, offs=1) -- 1855(line=57, offs=37)
*/
/*
/home/ubuntu/ATS2/libats/ML/SATS/strarr.sats: 1529(line=41, offs=1) -- 1573(line=43, offs=3)
*/

#include "libats/ML/CATS/strarr.cats"
/*
/home/ubuntu/ATS2/libats/ML/SATS/strarr.sats: 1612(line=47, offs=1) -- 1647(line=47, offs=36)
*/
/*
/home/ubuntu/ATS2/libats/ML/DATS/filebas.dats: 1472(line=40, offs=1) -- 1511(line=40, offs=40)
*/
/*
/home/ubuntu/ATS2/libats/ML/DATS/filebas.dats: 1534(line=44, offs=1) -- 1581(line=44, offs=48)
*/
/*
/home/ubuntu/ATS2/prelude/DATS/integer.dats: 1636(line=51, offs=1) -- 1675(line=51, offs=40)
*/
/*
/home/ubuntu/ATS2/libats/ML/DATS/filebas.dats: 1582(line=45, offs=1) -- 1629(line=45, offs=48)
*/
/*
/home/ubuntu/ATS2/prelude/DATS/filebas.dats: 1613(line=48, offs=1) -- 1652(line=48, offs=40)
*/
/*
/home/ubuntu/ATS2/prelude/DATS/filebas.dats: 1675(line=52, offs=1) -- 1722(line=52, offs=48)
*/
/*
/home/ubuntu/ATS2/prelude/DATS/integer.dats: 1636(line=51, offs=1) -- 1675(line=51, offs=40)
*/
/*
/home/ubuntu/ATS2/prelude/DATS/filebas.dats: 1745(line=56, offs=1) -- 1783(line=56, offs=39)
*/
/*
/home/ubuntu/ATS2/libc/SATS/stdio.sats: 1380(line=35, offs=1) -- 1418(line=37, offs=3)
*/

#include "libc/CATS/stdio.cats"
/*
/home/ubuntu/ATS2/libc/SATS/stdio.sats: 1898(line=62, offs=1) -- 1940(line=64, offs=27)
*/
/*
/home/ubuntu/ATS2/libc/sys/SATS/types.sats: 1390(line=36, offs=1) -- 1432(line=38, offs=3)
*/

#include "libc/sys/CATS/types.cats"
/*
/home/ubuntu/ATS2/prelude/DATS/filebas.dats: 1861(line=61, offs=1) -- 1901(line=61, offs=41)
*/
/*
/home/ubuntu/ATS2/libc/sys/SATS/stat.sats: 1390(line=36, offs=1) -- 1431(line=38, offs=3)
*/

#include "libc/sys/CATS/stat.cats"
/*
/home/ubuntu/ATS2/libc/sys/SATS/stat.sats: 1712(line=52, offs=1) -- 1754(line=53, offs=35)
*/
/*
/home/ubuntu/ATS2/libc/sys/SATS/types.sats: 1390(line=36, offs=1) -- 1432(line=38, offs=3)
*/

#include "libc/sys/CATS/types.cats"
/*
/home/ubuntu/ATS2/prelude/DATS/filebas.dats: 15323(line=844, offs=1) -- 15353(line=844, offs=31)
*/
/*
/home/ubuntu/ATS2/libc/SATS/stdio.sats: 1380(line=35, offs=1) -- 1418(line=37, offs=3)
*/

#include "libc/CATS/stdio.cats"
/*
/home/ubuntu/ATS2/libc/SATS/stdio.sats: 1898(line=62, offs=1) -- 1940(line=64, offs=27)
*/
/*
/home/ubuntu/ATS2/libc/sys/SATS/types.sats: 1390(line=36, offs=1) -- 1432(line=38, offs=3)
*/

#include "libc/sys/CATS/types.cats"
/*
/home/ubuntu/ATS2/libats/ML/DATS/filebas.dats: 2011(line=64, offs=1) -- 2046(line=64, offs=36)
*/
/*
/home/ubuntu/ATS2/libats/ML/DATS/filebas.dats: 2047(line=65, offs=1) -- 2082(line=65, offs=36)
*/
/*
/home/ubuntu/ATS2/libats/ML/SATS/list0.sats: 1665(line=47, offs=1) -- 1700(line=47, offs=36)
*/
/*
/home/ubuntu/ATS2/libats/ML/DATS/filebas.dats: 2083(line=66, offs=1) -- 2120(line=66, offs=38)
*/
/*
/home/ubuntu/ATS2/libats/ML/SATS/option0.sats: 1524(line=41, offs=1) -- 1559(line=41, offs=36)
*/
/*
/home/ubuntu/ATS2/libats/ML/DATS/filebas.dats: 2143(line=70, offs=1) -- 2180(line=70, offs=38)
*/
/*
/home/ubuntu/ATS2/libats/ML/SATS/filebas.sats: 1525(line=41, offs=1) -- 1560(line=41, offs=36)
*/
/*
/home/ubuntu/ATS2/libats/ML/DATS/filebas.dats: 2734(line=100, offs=1) -- 2780(line=100, offs=47)
*/
/*
/home/ubuntu/ATS2/prelude/DATS/strptr.dats: 1609(line=48, offs=1) -- 1648(line=48, offs=40)
*/
/*
/home/ubuntu/ATS2/prelude/DATS/strptr.dats: 1671(line=52, offs=1) -- 1718(line=52, offs=48)
*/
/*
/home/ubuntu/ATS2/prelude/DATS/integer.dats: 1636(line=51, offs=1) -- 1675(line=51, offs=40)
*/
/*
/home/ubuntu/ATS2/libats/ML/DATS/intrange.dats: 1397(line=36, offs=1) -- 1432(line=36, offs=36)
*/
/*
/home/ubuntu/ATS2/libats/ML/DATS/intrange.dats: 1455(line=40, offs=1) -- 1490(line=40, offs=36)
*/
/*
/home/ubuntu/ATS2/libats/ML/SATS/list0.sats: 1665(line=47, offs=1) -- 1700(line=47, offs=36)
*/
/*
/home/ubuntu/ATS2/libats/ML/DATS/intrange.dats: 1491(line=41, offs=1) -- 1527(line=41, offs=37)
*/
/*
/home/ubuntu/ATS2/libats/ML/SATS/array0.sats: 1525(line=41, offs=1) -- 1569(line=43, offs=3)
*/

#include "libats/ML/CATS/array0.cats"
/*
/home/ubuntu/ATS2/libats/ML/SATS/array0.sats: 1608(line=47, offs=1) -- 1643(line=47, offs=36)
*/
/*
/home/ubuntu/ATS2/libats/ML/DATS/intrange.dats: 1528(line=42, offs=1) -- 1566(line=42, offs=39)
*/
/*
/home/ubuntu/ATS2/libats/ML/SATS/intrange.sats: 1625(line=45, offs=1) -- 1660(line=45, offs=36)
*/
/*
/home/ubuntu/ATS2/libats/ML/DATS/stdlib.dats: 1392(line=36, offs=1) -- 1432(line=37, offs=33)
*/
/*
/home/ubuntu/ATS2/libc/SATS/stdlib.sats: 1389(line=36, offs=1) -- 1428(line=38, offs=3)
*/

#include "libc/CATS/stdlib.cats"
/*
/home/ubuntu/ATS2/libc/SATS/stdlib.sats: 1760(line=54, offs=1) -- 1800(line=55, offs=33)
*/
/*
/home/ubuntu/ATS2/libats/ML/DATS/stdlib.dats: 1455(line=41, offs=1) -- 1490(line=41, offs=36)
*/
/*
/home/ubuntu/ATS2/libats/ML/DATS/stdlib.dats: 1513(line=45, offs=1) -- 1549(line=45, offs=37)
*/
/*
/home/ubuntu/ATS2/libats/ML/SATS/stdlib.sats: 1522(line=41, offs=1) -- 1557(line=41, offs=36)
*/
/*
/home/ubuntu/ATS2/libats/ML/SATS/funmap.sats: 1524(line=43, offs=1) -- 1559(line=43, offs=36)
*/
/*
/home/ubuntu/ATS2/libats/ML/SATS/funset.sats: 1524(line=41, offs=1) -- 1559(line=41, offs=36)
*/
/*
/home/ubuntu/ATS2/libats/ML/DATS/funmap.dats: 1485(line=41, offs=1) -- 1531(line=42, offs=34)
*/
/*
/home/ubuntu/ATS2/libats/ML/DATS/funmap.dats: 1556(line=46, offs=1) -- 1591(line=46, offs=36)
*/
/*
/home/ubuntu/ATS2/libats/ML/SATS/list0.sats: 1665(line=47, offs=1) -- 1700(line=47, offs=36)
*/
/*
/home/ubuntu/ATS2/libats/ML/DATS/funmap.dats: 1614(line=50, offs=1) -- 1650(line=50, offs=37)
*/
/*
/home/ubuntu/ATS2/libats/ML/SATS/funmap.sats: 1524(line=43, offs=1) -- 1559(line=43, offs=36)
*/
/*
/home/ubuntu/ATS2/libats/ML/DATS/funset.dats: 1484(line=40, offs=1) -- 1523(line=41, offs=32)
*/
/*
/home/ubuntu/ATS2/libats/ML/DATS/funset.dats: 1550(line=45, offs=1) -- 1596(line=47, offs=34)
*/
/*
/home/ubuntu/ATS2/libats/ML/DATS/funset.dats: 1621(line=51, offs=1) -- 1656(line=51, offs=36)
*/
/*
/home/ubuntu/ATS2/libats/ML/DATS/funset.dats: 1679(line=55, offs=1) -- 1714(line=55, offs=36)
*/
/*
/home/ubuntu/ATS2/libats/ML/SATS/list0.sats: 1665(line=47, offs=1) -- 1700(line=47, offs=36)
*/
/*
/home/ubuntu/ATS2/libats/ML/DATS/funset.dats: 1737(line=59, offs=1) -- 1773(line=59, offs=37)
*/
/*
/home/ubuntu/ATS2/libats/ML/SATS/funset.sats: 1524(line=41, offs=1) -- 1559(line=41, offs=36)
*/
/*
/home/ubuntu/ATS2/libats/ML/SATS/dynarray.sats: 1529(line=41, offs=1) -- 1564(line=41, offs=36)
*/
/*
/home/ubuntu/ATS2/libats/DATS/dynarray.dats: 1399(line=36, offs=1) -- 1438(line=37, offs=32)
*/
/*
/home/ubuntu/ATS2/libats/DATS/dynarray.dats: 1461(line=41, offs=1) -- 1496(line=41, offs=36)
*/
/*
/home/ubuntu/ATS2/libats/SATS/dynarray.sats: 1467(line=40, offs=1) -- 1510(line=42, offs=3)
*/

#include "libats/CATS/dynarray.cats"
/*
/home/ubuntu/ATS2/libats/ML/DATS/dynarray.dats: 1396(line=36, offs=1) -- 1435(line=37, offs=32)
*/
/*
/home/ubuntu/ATS2/libats/ML/DATS/dynarray.dats: 1460(line=41, offs=1) -- 1500(line=42, offs=33)
*/
/*
/home/ubuntu/ATS2/libats/SATS/dynarray.sats: 1467(line=40, offs=1) -- 1510(line=42, offs=3)
*/

#include "libats/CATS/dynarray.cats"
/*
/home/ubuntu/ATS2/libats/ML/DATS/dynarray.dats: 1525(line=46, offs=1) -- 1560(line=46, offs=36)
*/
/*
/home/ubuntu/ATS2/libats/ML/DATS/dynarray.dats: 1561(line=47, offs=1) -- 1596(line=47, offs=36)
*/
/*
/home/ubuntu/ATS2/libats/ML/SATS/list0.sats: 1665(line=47, offs=1) -- 1700(line=47, offs=36)
*/
/*
/home/ubuntu/ATS2/libats/ML/DATS/dynarray.dats: 1619(line=51, offs=1) -- 1657(line=51, offs=39)
*/
/*
/home/ubuntu/ATS2/libats/ML/SATS/dynarray.sats: 1529(line=41, offs=1) -- 1564(line=41, offs=36)
*/
/*
/home/ubuntu/ATS2/libats/ML/SATS/hashtblref.sats: 1524(line=43, offs=1) -- 1559(line=43, offs=36)
*/
/*
/home/ubuntu/ATS2/libats/DATS/hashfun.dats: 1354(line=35, offs=1) -- 1393(line=36, offs=32)
*/
/*
/home/ubuntu/ATS2/libats/DATS/hashfun.dats: 1416(line=40, offs=1) -- 1450(line=40, offs=35)
*/
/*
/home/ubuntu/ATS2/libats/SATS/hashfun.sats: 1354(line=35, offs=1) -- 1396(line=37, offs=3)
*/

#include "libats/CATS/hashfun.cats"
/*
/home/ubuntu/ATS2/libats/DATS/linmap_list.dats: 1539(line=41, offs=1) -- 1578(line=42, offs=32)
*/
/*
/home/ubuntu/ATS2/libats/DATS/linmap_list.dats: 1601(line=46, offs=1) -- 1639(line=46, offs=39)
*/
/*
/home/ubuntu/ATS2/libats/DATS/hashtbl_chain.dats: 1604(line=42, offs=1) -- 1643(line=43, offs=32)
*/
/*
/home/ubuntu/ATS2/libats/DATS/hashtbl_chain.dats: 1666(line=47, offs=1) -- 1700(line=47, offs=35)
*/
/*
/home/ubuntu/ATS2/libats/SATS/hashfun.sats: 1354(line=35, offs=1) -- 1396(line=37, offs=3)
*/

#include "libats/CATS/hashfun.cats"
/*
/home/ubuntu/ATS2/libats/DATS/hashtbl_chain.dats: 1723(line=51, offs=1) -- 1763(line=51, offs=41)
*/
/*
/home/ubuntu/ATS2/libats/DATS/hashtbl_chain.dats: 2924(line=127, offs=1) -- 2967(line=128, offs=36)
*/
/*
/home/ubuntu/ATS2/libats/ML/DATS/hashtblref.dats: 1394(line=36, offs=1) -- 1433(line=37, offs=32)
*/
/*
/home/ubuntu/ATS2/libats/ML/DATS/hashtblref.dats: 1458(line=41, offs=1) -- 1503(line=42, offs=33)
*/
/*
/home/ubuntu/ATS2/libats/ML/DATS/hashtblref.dats: 1528(line=46, offs=1) -- 1563(line=46, offs=36)
*/
/*
/home/ubuntu/ATS2/libats/ML/DATS/hashtblref.dats: 1586(line=50, offs=1) -- 1621(line=50, offs=36)
*/
/*
/home/ubuntu/ATS2/libats/ML/SATS/list0.sats: 1665(line=47, offs=1) -- 1700(line=47, offs=36)
*/
/*
/home/ubuntu/ATS2/libats/ML/DATS/hashtblref.dats: 1644(line=54, offs=1) -- 1678(line=54, offs=35)
*/
/*
/home/ubuntu/ATS2/libats/SATS/hashfun.sats: 1354(line=35, offs=1) -- 1396(line=37, offs=3)
*/

#include "libats/CATS/hashfun.cats"
/*
/home/ubuntu/ATS2/libats/ML/DATS/hashtblref.dats: 1679(line=55, offs=1) -- 1719(line=55, offs=41)
*/
/*
/home/ubuntu/ATS2/libats/ML/SATS/hashtblref.sats: 1524(line=43, offs=1) -- 1559(line=43, offs=36)
*/
/*
/home/ubuntu/workspace/mylib/mylist.dats: 56(line=7, offs=1) -- 91(line=7, offs=36)
*/
/*
staload-prologues(end)
*/
/*
typedefs-for-tyrecs-and-tysums(beg)
*/
typedef
ATSstruct {
#if(0)
int contag ;
#endif
atstyvar_type(a) atslab__0 ;
atstype_boxed atslab__1 ;
} postiats_tysum_0 ;
typedef
ATSstruct {
#if(0)
int contag ;
#endif
atstyvar_type(b) atslab__0 ;
atstype_boxed atslab__1 ;
} postiats_tysum_1 ;
/*
typedefs-for-tyrecs-and-tysums(end)
*/
/*
dynconlst-declaration(beg)
*/
/*
dynconlst-declaration(end)
*/
/*
dyncstlst-declaration(beg)
*/
ATSdyncst_mac(atspre_print_string)
ATSdyncst_mac(atspre_print_newline)
/*
dyncstlst-declaration(end)
*/
/*
dynvalist-implementation(beg)
*/
/*
dynvalist-implementation(end)
*/
/*
exnconlst-declaration(beg)
*/
#ifndef _ATS_CCOMP_EXCEPTION_NONE_
ATSextern()
atsvoid_t0ype
the_atsexncon_initize
(
  atstype_exnconptr d2c, atstype_string exnmsg
) ;
#endif // end of [_ATS_CCOMP_EXCEPTION_NONE_]
/*
exnconlst-declaration(end)
*/
/*
assumelst-declaration(beg)
*/
/*
assumelst-declaration(end)
*/
/*
extypelst-declaration(beg)
*/
/*
extypelst-declaration(end)
*/
#if(0)
#if(0)
ATSextern()
atstyvar_type(c)
_057_home_057_ubuntu_057_workspace_057_assigns_057_06_057_MySolution_057_assign6_sol_056_dats__mylist2_foldright__0(atstype_boxed, atstype_boxed, atstype_cloptr, atstyvar_type(c)) ;
#endif // end of [QUALIFIED]
#endif // end of [TEMPLATE]

#if(0)
ATSstatic()
atstyvar_type(c)
aux_1__1(atstype_funptr, atstype_boxed, atstype_boxed, atstyvar_type(c)) ;
#endif // end of [TEMPLATE]

#if(0)
ATSextern()
atsvoid_t0ype
mainats_argc_argv_0(atstkind_t0ype(atstype_int), atsrefarg0_type(atstkind_type(atstype_ptrk))) ;
#endif // end of [QUALIFIED]

#if(0)
/*
/home/ubuntu/workspace/assigns/06/MySolution/assign6_sol.dats: 484(line=29, offs=18) -- 741(line=39, offs=4)
*/
/*
local: 
global: mylist2_foldright$0$0(level=0)
local: 
global: 
*/
ATSextern()
/*
imparg = a(9801), b(9802), c(9803)
tmparg = S2Evar(a(9801)), S2Evar(b(9802)), S2Evar(c(9803))
tmpsub = None()
*/
atstyvar_type(c)
_057_home_057_ubuntu_057_workspace_057_assigns_057_06_057_MySolution_057_assign6_sol_056_dats__mylist2_foldright__0(atstype_boxed arg0, atstype_boxed arg1, atstype_cloptr arg2, atstyvar_type(c) arg3)
{
/* tmpvardeclst(beg) */
ATStmpdec(tmpret0, atstyvar_type(c)) ;
/* tmpvardeclst(end) */
ATSfunbody_beg()
/*
emit_instr: loc0 = /home/ubuntu/workspace/assigns/06/MySolution/assign6_sol.dats: 467(line=29, offs=1) -- 741(line=39, offs=4)
*/
ATSINSflab(__patsflab_mylist2_foldright):
/*
emit_instr: loc0 = /home/ubuntu/workspace/assigns/06/MySolution/assign6_sol.dats: 507(line=29, offs=41) -- 741(line=39, offs=4)
*/
/*
letpush(beg)
*/
/*
letpush(end)
*/

/*
emit_instr: loc0 = /home/ubuntu/workspace/assigns/06/MySolution/assign6_sol.dats: 722(line=38, offs=5) -- 737(line=38, offs=20)
*/
ATSINSmove(tmpret0, ATSfunclo_fun(PMVd2vfunlab(d2v=aux$5608(1), flab=aux_1$0(level=1)), (atstype_boxed, atstype_boxed, atstyvar_type(c)), atstyvar_type(c))(arg0, arg1, arg3)) ;

/*
emit_instr: loc0 = /home/ubuntu/workspace/assigns/06/MySolution/assign6_sol.dats: 507(line=29, offs=41) -- 741(line=39, offs=4)
*/
/*
INSletpop()
*/
ATSfunbody_end()
ATSreturn(tmpret0) ;
} /* end of [_057_home_057_ubuntu_057_workspace_057_assigns_057_06_057_MySolution_057_assign6_sol_056_dats__mylist2_foldright__0] */
#endif // end of [TEMPLATE]

#if(0)
/*
/home/ubuntu/workspace/assigns/06/MySolution/assign6_sol.dats: 519(line=30, offs=9) -- 714(line=36, offs=6)
*/
/*
local: aux_1$0(level=1)
global: aux_1$0(level=1)
local: fopr$5606(1)(HSEfun(HSEtyvar(a(9801)), HSEtyvar(b(9802)), HSEtyvar(c(9803)); HSEtyvar(c(9803))))
global: fopr$5606(1)(HSEfun(HSEtyvar(a(9801)), HSEtyvar(b(9802)), HSEtyvar(c(9803)); HSEtyvar(c(9803))))
*/
ATSstatic()
atstyvar_type(c)
aux_1__1(atstype_funptr env0, atstype_boxed arg0, atstype_boxed arg1, atstyvar_type(c) arg2)
{
/* tmpvardeclst(beg) */
ATStmpdec(tmpret1, atstyvar_type(c)) ;
ATStmpdec(tmp2, atstyvar_type(a)) ;
ATStmpdec(tmp3, atstype_boxed) ;
ATStmpdec(tmp4, atstyvar_type(b)) ;
ATStmpdec(tmp5, atstype_boxed) ;
ATStmpdec(tmp6, atstyvar_type(c)) ;
/* tmpvardeclst(end) */
ATSfunbody_beg()
/*
emit_instr: loc0 = /home/ubuntu/workspace/assigns/06/MySolution/assign6_sol.dats: 519(line=30, offs=9) -- 714(line=36, offs=6)
*/
ATSINSflab(__patsflab_aux_1):
/*
emit_instr: loc0 = /home/ubuntu/workspace/assigns/06/MySolution/assign6_sol.dats: 575(line=32, offs=5) -- 708(line=35, offs=62)
*/
ATScaseof_beg()
/*
** ibranchlst-beg
*/
ATSbranch_beg()
/*
emit_instr: loc0 = /home/ubuntu/workspace/assigns/06/MySolution/assign6_sol.dats: 599(line=33, offs=8) -- 605(line=33, offs=14)
*/
ATSINSlab(__atstmplab0):
/*
emit_instr: loc0 = /home/ubuntu/workspace/assigns/06/MySolution/assign6_sol.dats: 523(line=30, offs=13) -- 525(line=30, offs=15)
*/
ATSifthen(ATSCKptriscons(arg0)) { ATSINSgoto(__atstmplab2) ; } ;
/*
emit_instr: loc0 = /home/ubuntu/workspace/assigns/06/MySolution/assign6_sol.dats: 608(line=33, offs=17) -- 608(line=33, offs=17)
*/
ATSINSlab(__atstmplab1):
/*
emit_instr: loc0 = : 0(line=0, offs=0) -- 0(line=0, offs=0)
*/
/*
ibranch-mbody:
*/
/*
emit_instr: loc0 = /home/ubuntu/workspace/assigns/06/MySolution/assign6_sol.dats: 614(line=33, offs=23) -- 618(line=33, offs=27)
*/
ATSINSmove(tmpret1, arg2) ;
ATSbranch_end()

ATSbranch_beg()
/*
emit_instr: loc0 = /home/ubuntu/workspace/assigns/06/MySolution/assign6_sol.dats: 631(line=34, offs=12) -- 637(line=34, offs=18)
*/
ATSINSlab(__atstmplab2):
/*
emit_instr: loc0 = /home/ubuntu/workspace/assigns/06/MySolution/assign6_sol.dats: 537(line=30, offs=27) -- 539(line=30, offs=29)
*/
ATSifthen(ATSCKptriscons(arg1)) { ATSINSgoto(__atstmplab4) ; } ;
/*
emit_instr: loc0 = /home/ubuntu/workspace/assigns/06/MySolution/assign6_sol.dats: 637(line=34, offs=18) -- 637(line=34, offs=18)
*/
ATSINSlab(__atstmplab3):
/*
emit_instr: loc0 = : 0(line=0, offs=0) -- 0(line=0, offs=0)
*/
/*
ibranch-mbody:
*/
/*
emit_instr: loc0 = /home/ubuntu/workspace/assigns/06/MySolution/assign6_sol.dats: 642(line=34, offs=23) -- 646(line=34, offs=27)
*/
ATSINSmove(tmpret1, arg2) ;
ATSbranch_end()

ATSbranch_beg()
/*
emit_instr: loc0 = /home/ubuntu/workspace/assigns/06/MySolution/assign6_sol.dats: 654(line=35, offs=8) -- 665(line=35, offs=19)
*/
ATSINSlab(__atstmplab4):
/*
emit_instr: loc0 = /home/ubuntu/workspace/assigns/06/MySolution/assign6_sol.dats: 523(line=30, offs=13) -- 525(line=30, offs=15)
*/
#if(0)
ATSifthen(ATSCKptrisnull(arg0)) { ATSINSdeadcode_fail() ; } ;
#endif
/*
emit_instr: loc0 = /home/ubuntu/workspace/assigns/06/MySolution/assign6_sol.dats: 666(line=35, offs=20) -- 677(line=35, offs=31)
*/
ATSINSlab(__atstmplab5):
/*
emit_instr: loc0 = /home/ubuntu/workspace/assigns/06/MySolution/assign6_sol.dats: 537(line=30, offs=27) -- 539(line=30, offs=29)
*/
#if(0)
ATSifthen(ATSCKptrisnull(arg1)) { ATSINSdeadcode_fail() ; } ;
#endif
/*
emit_instr: loc0 = /home/ubuntu/workspace/assigns/06/MySolution/assign6_sol.dats: 677(line=35, offs=31) -- 677(line=35, offs=31)
*/
ATSINSlab(__atstmplab6):
/*
emit_instr: loc0 = /home/ubuntu/workspace/assigns/06/MySolution/assign6_sol.dats: 660(line=35, offs=14) -- 661(line=35, offs=15)
*/
ATSINSmove(tmp2, ATSSELcon(arg0, postiats_tysum_0, atslab__0)) ;
/*
emit_instr: loc0 = /home/ubuntu/workspace/assigns/06/MySolution/assign6_sol.dats: 662(line=35, offs=16) -- 664(line=35, offs=18)
*/
ATSINSmove(tmp3, ATSSELcon(arg0, postiats_tysum_0, atslab__1)) ;
/*
emit_instr: loc0 = /home/ubuntu/workspace/assigns/06/MySolution/assign6_sol.dats: 672(line=35, offs=26) -- 673(line=35, offs=27)
*/
ATSINSmove(tmp4, ATSSELcon(arg1, postiats_tysum_1, atslab__0)) ;
/*
emit_instr: loc0 = /home/ubuntu/workspace/assigns/06/MySolution/assign6_sol.dats: 674(line=35, offs=28) -- 676(line=35, offs=30)
*/
ATSINSmove(tmp5, ATSSELcon(arg1, postiats_tysum_1, atslab__1)) ;
/*
emit_instr: loc0 = : 0(line=0, offs=0) -- 0(line=0, offs=0)
*/
/*
ibranch-mbody:
*/
/*
emit_instr: loc0 = /home/ubuntu/workspace/assigns/06/MySolution/assign6_sol.dats: 692(line=35, offs=46) -- 707(line=35, offs=61)
*/
ATSINSmove(tmp6, ATSfunclo_fun(PMVd2vfunlab(d2v=aux$5608(1), flab=aux_1$0(level=1)), (atstype_boxed, atstype_boxed, atstyvar_type(c)), atstyvar_type(c))(tmp3, tmp5, arg2)) ;

/*
emit_instr: loc0 = /home/ubuntu/workspace/assigns/06/MySolution/assign6_sol.dats: 682(line=35, offs=36) -- 708(line=35, offs=62)
*/
ATSINSmove(tmpret1, ATSfunclo_clo(env0, (atstype_cloptr, atstyvar_type(a), atstyvar_type(b), atstyvar_type(c)), atstyvar_type(c))(env0, tmp2, tmp4, tmp6)) ;

ATSbranch_end()

/*
** ibranchlst-end
*/
ATScaseof_end()

ATSfunbody_end()
ATSreturn(tmpret1) ;
} /* end of [aux_1__1] */
#endif // end of [TEMPLATE]

/*
/home/ubuntu/workspace/assigns/06/MySolution/assign6_sol.dats: 1329(line=82, offs=6) -- 1986(line=110, offs=2)
*/
/*
local: 
global: mainats_argc_argv_0$2$0(level=0)
local: 
global: 
*/
ATSextern()
atsvoid_t0ype
mainats_argc_argv_0(atstkind_t0ype(atstype_int) arg0, atsrefarg0_type(atstkind_type(atstype_ptrk)) arg1)
{
/* tmpvardeclst(beg) */
// ATStmpdec_void(tmpret7) ;
// ATStmpdec_void(tmp8) ;
/* tmpvardeclst(end) */
ATSfunbody_beg()
/*
emit_instr: loc0 = /home/ubuntu/workspace/assigns/06/MySolution/assign6_sol.dats: 1324(line=82, offs=1) -- 1986(line=110, offs=2)
*/
ATSINSflab(__patsflab_main_argc_argv_0):
/*
emit_instr: loc0 = /home/ubuntu/workspace/assigns/06/MySolution/assign6_sol.dats: 1344(line=83, offs=1) -- 1986(line=110, offs=2)
*/
/*
letpush(beg)
*/
/*
letpush(end)
*/

/*
emit_instr: loc0 = /home/ubuntu/workspace/assigns/06/MySolution/assign6_sol.dats: 1920(line=108, offs=10) -- 1981(line=108, offs=71)
*/
ATSINSmove_void(tmp8, atspre_print_string(ATSPMVstring("Good news: Your code has passed initial testing!"))) ;

/*
emit_instr: loc0 = /home/ubuntu/workspace/assigns/06/MySolution/assign6_sol.dats: 1920(line=108, offs=10) -- 1981(line=108, offs=71)
*/
ATSINSmove_void(tmpret7, atspre_print_newline()) ;

/*
emit_instr: loc0 = /home/ubuntu/workspace/assigns/06/MySolution/assign6_sol.dats: 1344(line=83, offs=1) -- 1986(line=110, offs=2)
*/
/*
INSletpop()
*/
ATSfunbody_end()
ATSreturn_void(tmpret7) ;
} /* end of [mainats_argc_argv_0] */

/*
** for initialization(dynloading)
*/
ATSdynloadflag_minit(_057_home_057_ubuntu_057_workspace_057_assigns_057_06_057_MySolution_057_assign6_sol_056_dats__dynloadflag) ;
ATSextern()
atsvoid_t0ype
_057_home_057_ubuntu_057_workspace_057_assigns_057_06_057_MySolution_057_assign6_sol_056_dats__dynload()
{
ATSfunbody_beg()
ATSdynload(/*void*/)
ATSdynloadflag_sta(
_057_home_057_ubuntu_057_workspace_057_assigns_057_06_057_MySolution_057_assign6_sol_056_dats__dynloadflag
) ;
ATSif(
ATSCKiseqz(
_057_home_057_ubuntu_057_workspace_057_assigns_057_06_057_MySolution_057_assign6_sol_056_dats__dynloadflag
)
) ATSthen() {
ATSdynloadset(_057_home_057_ubuntu_057_workspace_057_assigns_057_06_057_MySolution_057_assign6_sol_056_dats__dynloadflag) ;
/*
dynexnlst-initize(beg)
*/
/*
dynexnlst-initize(end)
*/
} /* ATSendif */
ATSfunbody_end()
ATSreturn_void(tmpret_void) ;
} /* end of [*_dynload] */

/*
** the ATS runtime
*/
#ifndef _ATS_CCOMP_RUNTIME_NONE_
#include "pats_ccomp_runtime.c"
#include "pats_ccomp_runtime_memalloc.c"
#ifndef _ATS_CCOMP_EXCEPTION_NONE_
#include "pats_ccomp_runtime2_dats.c"
#ifndef _ATS_CCOMP_RUNTIME_TRYWITH_NONE_
#include "pats_ccomp_runtime_trywith.c"
#endif /* _ATS_CCOMP_RUNTIME_TRYWITH_NONE_ */
#endif // end of [_ATS_CCOMP_EXCEPTION_NONE_]
#endif /* _ATS_CCOMP_RUNTIME_NONE_ */

/*
** the [main] implementation
*/
int
main
(
int argc, char **argv, char **envp
) {
int err = 0 ;
_057_home_057_ubuntu_057_workspace_057_assigns_057_06_057_MySolution_057_assign6_sol_056_dats__dynload() ;
ATSmainats_argc_argv_0(argc, argv, err) ;
return (err) ;
} /* end of [main] */

/* ****** ****** */

/* end-of-compilation-unit */
