(* ****** ****** *)

#include "./../assign6.dats"

(* ****** ****** *)

(*
** HX-2016-02-25: 10 points
** please implement mylist2_foldright
*)


implement
{a,b,c}
mylist2_foldright(xs, ys, fopr, sink) = let
    fun aux(xs: list0(a), ys: list0(b), sink: c): c =
    (
    case+ (xs,ys) of
    | (nil0(), _ ) => sink 
    | ( _ ,nil0()) => sink
    | (cons0(x,xs),cons0(y,ys)) => fopr(x,y, aux(xs,ys,sink))
    )
in
    aux(xs,ys,sink)
end



(* ****** ****** *)

(*
//
// HX-2016-02-25: 10 points
// please implement
// mylist_compare based on mylist2_foldleft+exception
//
*)
(*
implement
{a}
mylist_compare(xs,ys) =

fun aux(xs: list0(a), ys: list0(a)): int = let
exception NotEqualExn of ()
(
case+ (xs,ys) of
| (nil0(),nil0()) => true
| ( cons0(x,xs) , cons0(y,ys) ) => 
	if x < y then $raise NotEqualExn() //return -1?
	elseif x > y then $raise NotEqualExn()  //return 1?
	else aux(xs,ys)
in
	try aux(xs,ys) with ~NotEqualExn() => 0
end

  *)   
    


(* ****** ****** *)

#ifdef
MAIN_NONE
#then
#else
//
implement
main0(argc, argv) =
{
//

//val xs = mylist_make_intrange(0, 10)
//
//val out = stdout_ref
//
//val () = fprintln! (out, "xs = ", xs)
//
//val ys = mylist_reverse<int>(xs)
//val () = fprintln! (out, "ys = ", ys)
//
//val zs = mylist_filter<int>(xs, lam(x) => x % 2 = 0)
//val () = fprintln! (out, "zs = ", zs)
//
//val sgn = mylist_compare<int>(xs, xs)
//val () = println! ("compare(xs, xs) = ", sgn)
//
//val sgn = mylist_compare<int>(xs, ys)
//val () = println! ("compare(xs, ys) = ", sgn)
//
//val sgn = mylist_compare<int>(xs, zs)
//val () = println! ("compare(xs, zs) = ", sgn)
//

val () = println! ("Good news: Your code has passed initial testing!")
//
} (* end of [main0] *)
//
#endif // #ifdef

(* ****** ****** *)

(* end of [assign6_sol.dats] *)
