(* ****** ****** *)

#include "./../assign4-2.dats"

(* ****** ****** *)

extern fun {a:t@ype} tabulateHelper(a: int, f: (int) -<cloref1> a, b: int): list0(a)
extern fun {a:t@ype} snocHelper(xs: list0(a), x0: a): list0(a)
(*
** HX-2016-02-12:
** please implement mylist_last
*)
extern
fun
{a:t@ype}
mylist_reverse : (list0(a)) -> list0(a)
extern
fun
{a:t@ype}
mylist_revappend : (list0(a), list0(a)) -> list0(a)
//
(* ****** ****** *)
//
implement
{a}
mylist_reverse(xs) = 
mylist_revappend<a>(xs, list0_nil())
//
implement
{a}
mylist_revappend(xs, ys) = let
//
fun
revappend
(
  xs: list0(a), ys: list0(a)
) : list0(a) =
  case+ xs of
  | list0_nil() => ys
  | list0_cons(x, xs) =>
      revappend(xs, list0_cons(x, ys))
    // end of [list0_cons]
//
in
  revappend(xs, ys)
end 

implement
{a}
mylist_last(xs) = let
val xs = mylist_reverse(xs)
in
case- xs of
|cons0(x,xs) => x
end

(* ****** ****** *)

(*
** HX-2016-02-12:
** please implement mylist_init
*)

implement
{a}
mylist_init(xs) = let
val xs = mylist_reverse(xs)
in
case- xs of
|cons0(x,xs) => mylist_reverse(xs)
end

(* ****** ****** *)

(*
** HX-2016-02-12:
** please implement mylist_snoc //puts x0 at the end of xs
*)

implement
{a}
mylist_snoc(xs,x0) = let
val a = mylist_reverse(xs);
//val () = println!("snoc: ", mylist_reverse(cons0(x0,a)) );
//val () = println!("init: ", mylist_init(xs));
in
case- xs of
| cons0(_,_) => mylist_reverse(cons0(x0,a))
end

(* ****** ****** *)

(*
** HX-2016-02-12:
** please implement mylist_tabulate
*)

implement
{a}
mylist_tabulate(n,f) = 
    tabulateHelper(n,f,0);
    
//discussed with Peter and Biken
implement 
{a}
tabulateHelper(a,f,c) = 
    if c < a then //while c is less than the max amount, continue
    cons0( f(c) , tabulateHelper(a,f,c+1)) //apply function on c, add to list. pass in c+1 to the next step
    else nil0 //stop creating the list when c is the same as the max amount

(* ****** ****** *)

#ifdef
MAIN_NONE
#then
#else
implement
main0 (argc, argv) =
{
//
// HX: here is some testing code:
//
val N = 10
//
val xs = mylist_tabulate<int>(N, lam(i) => (i+1))
//
val () = println!("xs: ", xs)
val () = assertloc(mylist_last<int>(xs) = N)
val () = assertloc(mylist_last<int>(mylist_init<int>(xs)) = N-1)
val () = assertloc(mylist_last<int>(mylist_snoc<int>(xs, 2*N)) = 2*N)
val () = println!("finished testing!")
//
} (* end of [main0] *)
#endif // #ifdef

(* ****** ****** *)

(* end of [assign4-2_sol.dats] *)