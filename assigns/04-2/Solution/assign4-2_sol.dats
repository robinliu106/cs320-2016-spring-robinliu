(* ****** ****** *)

#include "./../assign4-2.dats"

(* ****** ****** *)

(*
** HX-2016-02-12:
** please implement mylist_last
*)
implement
{a}(*tmp*)
mylist_last
  (xs) = let
//
fun
aux
(
  xs: list0(a), x0: a
) : a =
  case+ xs of
  | list0_nil() => x0
  | list0_cons(x, xs) => aux(xs, x)
//
in
  case- xs of list0_cons(x, xs) => aux(xs, x)
end // end of [mylist_last]

(* ****** ****** *)

(*
** HX-2016-02-12:
** please implement mylist_init
*)
implement
{a}(*tmp*)
mylist_init
  (xs) = let
//
fun
aux
(
  xs: list0(a), x0
) : list0(a) =
  case+ xs of
  | list0_nil() => list0_nil()
  | list0_cons(x, xs) =>
      list0_cons(x0, aux(xs, x))
    // end of [list0_cons]
//
in
  case- xs of list0_cons(x, _) => aux(xs, x)
end // end of [mylist_init]

(* ****** ****** *)

(*
** HX-2016-02-12:
** please implement mylist_snoc
*)

implement
{a}(*tmp*)
mylist_snoc
  (xs, x0) = let
//
fun
aux
(
  xs: list0(a)
) : list0(a) =
  case+ xs of
  | list0_nil() =>
      list0_sing(x0)
    // list0_nil
  | list0_cons (x, xs) =>
      list0_cons(x, aux(xs))
    // list0_cons
//
in
  aux(xs)
end // end of [mylist_snoc]

(* ****** ****** *)

(*
** HX-2016-02-12:
** please implement mylist_tabulate
*)

implement
{a}(*tmp*)
mylist_tabulate
  (n, f) = let
//
fun
aux(i: int): list0(a) =
(
  if i < n
    then list0_cons(f(i), aux(i+1))
    else list0_nil()
  // end of [if]
) (* end of [aux] *)
//
in
  aux(0)
end // end of [mylist_tabulate]

(* ****** ****** *)

#ifdef
MAIN_NONE
#then
#else
implement
main0 (argc, argv) =
{
//
// HX: here is some testing code:
//
val N = 10
//
val xs =
mylist_tabulate<int>(N, lam(i) => (i+1))
//
val () = assertloc(mylist_last<int>(xs) = N)
val () = assertloc(mylist_last<int>(mylist_init<int>(xs)) = N-1)
val () = assertloc(mylist_last<int>(mylist_snoc<int>(xs, 2*N)) = 2*N)
//
} (* end of [main0] *)
#endif // #ifdef

(* ****** ****** *)

(* end of [assign4-2_sol.dats] *)
