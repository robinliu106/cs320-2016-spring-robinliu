(* ****** ****** *)

#include "./../assign3.dats"

(* ****** ****** *)

staload "./../../../mylib/mylist.dats"

(* ****** ****** *)
//
(*
//
// HX: From scratch!
//
fun
padding
(
  H: int, xs: list0(int)
) : list0(int) = let
//
val n = mylist_length(xs)
val () = assertloc (H >= n)
//
fun
aux
(
  k: int, xs: list0(int)
) : list0(int) =
  if k > 0 then aux(k-1, cons0(0, xs)) else xs
//
in
  aux(H - n, xs)
end // end of [padding]
*)
//
// HX: Based on combinators
//
fun
padding
(
  H: int, xs: list0(int)
) : list0(int) = let
  val n = mylist_length(xs)
  val () = assertloc(H >= n)
in
  mylist_append(mylist_make_elt<int>(H-n, 0), xs)
end // end of [padding]
//
(* ****** ****** *)

implement
show_poles
(
  R, H, xs1, xs2, xs3
) = let
//
fun
aux
(
  xs1: list0(int)
, xs2: list0(int)
, xs3: list0(int)
) : void = (
//
case+ xs1 of
| nil0() => ()
| cons0(x1, xs1) => let
(*
    val-cons0(x2, xs2) = xs2
    val-cons0(x3, xs3) = xs3
*)
    val x2 = mylist_head(xs2)
    val x3 = mylist_head(xs3)
//
    val () = show_disk(R, x1)
    val () = show_disk(R, x2)
    val () = show_disk(R, x3)
//
    val () = print_newline((*void*))
//
  in
    aux(xs1, mylist_tail(xs2), mylist_tail(xs3))
  end // end of [cons0]
//
) (* end of [aux] *)
//
in
  aux(padding(H, xs1), padding(H, xs2), padding(H, xs3))
end // end of [show_poles]

(* ****** ****** *)

#ifdef
MAIN_NONE
#then
#else
implement
main0 (argc, argv) =
{
//
// HX: here is some testing code:
//
val width = 10
val height = 10
//
val P1 = cons0(1, cons0(3, nil0()))
val P2 = cons0(2, cons0(4, cons0(5, cons0(7, nil0()))))
val P3 = cons0(6, cons0(8, nil0()))
//
val () = show_poles(width, height, P1, P2, P3)
//
val () = println! ("Good news: Your code has passed initial testing!")
//
} (* end of [main0] *)
#endif // #ifdef

(* ****** ****** *)

(* end of [assign3_sol.dats] *)
