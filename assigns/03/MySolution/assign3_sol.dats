(* ****** ****** *)

#include "./../assign3.dats"

(* ****** ****** *)

//discussed with Biken and Peter
implement
show_poles(width, height, pole1, pole2, pole3) =
  case- (pole1,pole2,pole3) of 
| (nil0(),nil0(),nil0()) => () //return nothing if one of them is nil
| (cons0(x,xs),cons0(y,ys),cons0(z,zs)) => //pattern match three lists
  let
    val () = show_disk(width,x)
    //val _ = println!()
    val () = show_disk(width,y)
    //val _ = println!()
    val () = show_disk(width,z)
    val _ = println!()
  in
    show_poles(width,height,xs,ys,zs) //pass in the tail of each list 
  end


#ifdef
MAIN_NONE
#then
#else
implement
main0 (argc, argv) =
{
//
// HX: here is some testing code:
//
val width = 10
val height = 10
//
val P1 = cons0(0,cons0(1, cons0(3, nil0())))
val P2 = cons0(2, cons0(4, cons0(5, nil0())))
val P3 = cons0(6, cons0(7, cons0(8, nil0())))

// testing code
//val () = mylist_print(P1)

// testing code

val () = show_poles(width, height, P1, P2, P3)
//
val () = println! ("Good news: Your code has passed initial testing!")
//
} (* end of [main0] *)
#endif // #ifdef

(* ****** ****** *)

(* end of [assign3_sol.dats] *)
