(* ****** ****** *)

#include "./../assign1.dats"

(* ****** ****** *)
//
// HX:
// Please replace
// this dummy implementation
//
implement
fib_trec(n) = let
//
fun
loop
( i: int, x: int, y: int) : int =
(
//
if n > 0
  then let
    val i = i - 1
    val temp = x
    val x = y
    val y = temp+y
  in
    loop (i,x,y)
  end // end of [then]
  else y // end of [else]
//
)
in
  loop(n,0,1)
end // end of [fib_trec]

// referenced http://rosettacode.org/wiki/Fibonacci_sequence#Recursive_4 and facts.dats
(* ****** ****** *)
//
// HX:
// Please replace
// this dummy implementation
//
implement
try_fact((*void*)) = 0
//
(* ****** ****** *)
//
// HX:
// Please replace
// this dummy implementation
//
implement intsqrt(n) = 
let 
    val n4 = n/4
    val r0 = 2 * intsqrt(n4)
    val r1 = r0 + 1
in 
if (r1 * r1) <= n then
    r1
else 
    r0
end 
//
(* ****** ****** *)

#ifdef
MAIN_NONE
#then
#else
implement
main0 (argc, argv) =
{
//
// HX: here is some testing code:
//
val r1 = fib_trec(10)
val () = assertloc(fib(10) = r1)
val () = println! ("fib_trec(10) = ", r1)
//
val r2 = fib_trec(20)
val () = println! ("fib_trec(20) = ", r2)
val () = assertloc(fib(20) = r2)
//
val () = println! ("try_fact() = ", try_fact())
//
val () = assertloc(intsqrt(100*100) = 100)
val () = assertloc(intsqrt(100*100-1) = 99)
//
val () = println! ("Good news: Your code has passed initial testing!")
//
} (* end of [main0] *)
#endif // #ifdef

(* ****** ****** *)

(* end of [assign1_sol.dats] *)